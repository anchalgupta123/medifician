<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pharmacy extends CI_Controller {

	public $current_date_time;
	public $login_id;
	public $login_role;

	public function __construct()
	{
		parent::__construct();
		$this->login_id = $this->session->userdata('login_id');
		$this->login_role = $this->session->userdata('login_role');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("Asia/Kolkata");
		}
		if (!$this->login_id) {
			redirect('Login');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function view_prescription()
	{    
        $this->load->model('Model_pharmacy');
		$data['pharmacy'] = $this->Model_pharmacy->get_all_pharmacy_details();
		// echo "<pre>";
		// print_r($data);
		// return;
		$this->load->view('pharmacy/prescription_upload',$data);	
	}

	public function save_prescription_price()
	{  
	    $id= $_POST['id'];
        $update_price = $_POST['update_price'];

        $this->load->model('Model_pharmacy');
        $data = array(
            'pricing'=>$update_price,
            'status' =>'1',
        );
        $data_id = $this->Model_pharmacy->update_pricing($data,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function view_confirm_order_details()
	{    
        $this->load->model('Model_pharmacy');
		$data['order'] = $this->Model_pharmacy->get_all_confirm_order_details();
		$this->load->view('pharmacy/view_confirm_order',$data);	
	}
    /*public function update_deliver_status_modal()
    { 
      $id = $_POST['id'];
      $this->load->model('Model_pharmacy'); 
      $data['deliver'] = $this->Model_pharmacy->get_update_delivered_status_details($id);
      $this->load->view('pharmacy/modal_update_deliver_status',$data);
    }*/
    public function update_deliver_status()
    {  
        $id = $_POST['status_id'];
        $status= $_POST['status'];

        $this->load->model('Model_pharmacy');
        $data_category = array(
            'status'=>$status,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_pharmacy->update_delivered_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }
	public function view_delivered_order_details()
	{    
        $this->load->model('Model_pharmacy');
		$data['order'] = $this->Model_pharmacy->get_all_delivered_order_details();
		$this->load->view('pharmacy/view_delivered_order',$data);	
	}
    public function prescription_status()
    {    
        $id = $_GET['id'];
        $this->load->model('Model_pharmacy'); 
        $data['deliver'] = $this->Model_pharmacy->get_update_delivered_status_details($id);
        $data['payment'] = $this->Model_pharmacy->get_update_payment_status_details($id);
        $this->load->view('pharmacy/update_prescription_status',$data);   
    }
    public function update_payment_status()
    {  
        $id = $_POST['payment_id'];
        $payment_type= $_POST['payment_type'];

        $this->load->model('Model_pharmacy');
        $data_category = array(
            'payment_type'=>$payment_type,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_pharmacy->update_payment_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }
    public function view_canceled_order_details()
    {    
        $this->load->model('Model_pharmacy');
        $data['order'] = $this->Model_pharmacy->get_all_canceled_order_details();
        $this->load->view('pharmacy/view_canceled_order',$data); 
    }
    public function delete_canceled_order()
    {
        $id= $_POST['id'];
        $this->load->model('Model_pharmacy');
        $data_id = $this->Model_pharmacy->delete_cancel_order($id);
        if ($data_id) {
            echo "Valid";
        }
    }  
	public function show_pharmacy_user_detail_modal()
	{
		$id = $_POST['id'];
        $this->load->model('Model_pharmacy');
        $data['user'] = $this->Model_pharmacy->get_pharmacy_user_details($id);
		$this->load->view('pharmacy/modal_pharmacy_user_details',$data);
	}
	public function pharmacy_check_out_details()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_pharmacy'); 
        $data['check_out'] = $this->Model_pharmacy->get_pharmacy_check_out_details($user_id);
        $data['get_check_out'] = $this->Model_pharmacy->get_pharmacy_all_check_out_details($user_id);
        $this->load->view('pharmacy/view_pharmacy_check_out',$data);
    }
    public function add_pharmacy_medicine_price()
    {  
        $id = $_POST['order_id'];
        $pricing = $_POST['price'];
        $gst_amount = $_POST['gst_amount'];
        $total_amount = $_POST['total_amount'];
        $medicine_name =$_POST['medicine_name'];
        $medicine_price =$_POST['medicine_price'];
        $medicine_gst_price =$_POST['medicine_gst_price'];
        $qty =$_POST['qty'];
        
        $this->load->model('Model_pharmacy');
        $data_category = array(
            'pricing'=>$pricing,
            'gst_amount'=>$gst_amount,
            'total_amount'=>$total_amount,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_pharmacy->update_pharmacy_total_price($data_category,$id);
        
        $medicine_name = explode(',',$medicine_name);
        $medicine_price = explode(',',$medicine_price);
        $medicine_gst_price = explode(',',$medicine_gst_price);
        $qty = explode(',',$qty);

        
        if ($data_id) {
            for ($i=0; $i <count($medicine_name) ; $i++) {
                $data_size = array(
                    'prescription_id'=>$data_id,
                    'medicine_name'=>$medicine_name[$i],
                    'medicine_price'=>$medicine_price[$i],
                    'medicine_gst_price'=>$medicine_gst_price[$i],
                    'qty'=>$qty[$i],
                    'created_date_time'=>$this->current_date_time,
                );

                $this->load->model('Model_pharmacy_medicine_data');   
                $this->Model_pharmacy_medicine_data->insert_pharmacy_medicine_data($data_size);
            }
             echo "Valid";
         } 
     
    }
    public function open_pharmacy_invoice()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_pharmacy'); 
        $data['user_details'] = $this->Model_pharmacy->get_pharmacy_invoice_details($user_id);
        $data['invoice_count'] = $this->Model_pharmacy->get_pp_invoice_count();
        $data['medicine_details'] = $this->Model_pharmacy->get_pharmacy_medicine_details($user_id);
        $this->load->view('pharmacy/view_pharmacy_invoice',$data);
    }
    public function print_pharmacy_invoice_pdf()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_pharmacy'); 
        $data['user_details'] = $this->Model_pharmacy->get_pharmacy_invoice_details($user_id);
         $data['invoice_count'] = $this->Model_pharmacy->get_pp_invoice_count();
        $data['medicine_details'] = $this->Model_pharmacy->get_pharmacy_medicine_details($user_id);
        $this->load->view('pharmacy/print_pharmacy_invoice',$data);
    }
    
}
