<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('Login_form');
	}

	public function check_login()
	{
		$email = $_POST['email'];
		$password = md5($_POST['password']);

		$this->load->model('Model_login');
		$login_data = $this->Model_login->login_user($email,$password);
		
		if ($login_data) 
		{
			$this->session->set_userdata('login_id',$login_data->id);
			$this->session->set_userdata('login_email',$login_data->email);
			$this->session->set_userdata('login_role',$login_data->role);
			echo "Valid";
		}
		else
		{
			echo "Invalid";
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('Login','refresh');
	}

}
