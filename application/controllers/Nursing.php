<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Nursing extends CI_Controller {

	public $current_date_time;
	public $login_id;
	public $login_role;

	public function __construct()
	{
		parent::__construct();
		$this->login_id = $this->session->userdata('login_id');
		$this->login_role = $this->session->userdata('login_role');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("Asia/Kolkata");
		}
		if (!$this->login_id) {
			redirect('Login');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}
    public function view_nursing_add_details()
    {    
        $this->load->model('Model_nursing_details');
        $data['price'] = $this->Model_nursing_details->get_all_nursing_details();
        $this->load->view('nursing/nurse_requirement',$data);   
    }
    public function add_nursing_modal()
    { 
      $this->load->view('nursing/modal_add_nursing');
    }
    public function add_nursing_price()
    {
        $time = $_POST['time'];
        $price= $_POST['price'];
        
        $this->load->model('Model_nursing_details');
        $data_category = array(
            'time'=>$time,
            'price'=>$price,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_nursing_details->insert_nursing_price($data_category);
        if ($data_id) {
            echo "Valid";
        }

      }
    public function edit_nursing_details()
    {   
        $id = $_POST['id'];
        $this->load->model('Model_nursing_details');
        $data['nursing_data'] = $this->Model_nursing_details->get_all_nursing_data($id);
        $this->load->view('nursing/modal_edit_nursing',$data);
    }
    public function edit_nursing_price()
    {
        $id = $_POST['nursing_id'];
        $time = $_POST['time'];
        $price= $_POST['price'];
        
        $this->load->model('Model_nursing_details');
        $data_category = array(
            'time'=>$time,
            'price'=>$price,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_nursing_details->update_nursing_data($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }
    public function delete_nursing_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_nursing_details');
        $data_id = $this->Model_nursing_details->delete_nursing_data($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function view_nursing_pending_details()
    {    
        $this->load->model('Model_nursing_order');
        $data['price'] = $this->Model_nursing_order->get_all_nursing_pending_details();
        $this->load->view('nursing/nursing_order',$data);  
    }
    public function view_nursing_order()
    {    
        $this->load->model('Model_nursing_order');
        $data['price'] = $this->Model_nursing_order->get_all_nursing_order();
        $this->load->view('nursing/nursing_order',$data);   
    }
    public function update_nursing_price_modal()
    { 
      $id = $_POST['id'];
      $this->load->model('Model_nursing_order'); 
      $data['nursing'] = $this->Model_nursing_order->get_update_nursing_details($id);
      $this->load->view('nursing/modal_update_nursing_price',$data);
    }
    public function update_nursing_price()
    {  
        $id = $_POST['price_id'];
        $price= $_POST['price'];

        $this->load->model('Model_nursing_order');
        $data_category = array(
            'price'=>$price,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_nursing_order->update_nursing_price($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }
     public function show_nursing_user_detail_modal()
    {
        $id = $_POST['id'];
        $this->load->model('Model_nursing_order');
        $data['user'] = $this->Model_nursing_order->get_nursing_user_details($id);
        $this->load->view('nursing/modal_nursing_user_details',$data);

    }

}
