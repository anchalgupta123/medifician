<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Physiotherapy extends CI_Controller {

	public $current_date_time;
	public $login_id;
	public $login_role;

	public function __construct()
	{
		parent::__construct();
		$this->login_id = $this->session->userdata('login_id');
		$this->login_role = $this->session->userdata('login_role');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("Asia/Kolkata");
		}
		if (!$this->login_id) {
			redirect('Login');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function view_physiotherapy_labs()
	{    
        $this->load->model('Model_physiotherapy_labs');
		$data['physio_lab'] = $this->Model_physiotherapy_labs->get_all_physiotherapy_labs_details();
		$this->load->view('physiotherapy/view_physiotherapy_lab',$data);	
	}
	public function add_physiotherapy_labs_modal()
	{
		$this->load->view('physiotherapy/modal_add_physiotherapy_lab');
	}
    public function add_physiotherapy_labs()
    {
        $lab_name = $_POST['lab_name'];
        $email = $_POST['email'];
        $address = $_POST['address'];
        $p_city = $_POST['p_city'];
        $p_state = $_POST['p_state'];
        $mobile_no = $_POST['mobile_no'];
        $landline_no = $_POST['landline_no'];

        $this->load->model('Model_physiotherapy_labs');
        $data_category = array(
            'lab_name'=>$lab_name,
            'email'=>$email,
            'address'=>$address,
            'p_city'=>$p_city,
            'p_state'=>$p_state,
            'mobile_no'=>$mobile_no,
            'landline_no'=>$landline_no,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_physiotherapy_labs->insert_physiotherapy_labs($data_category);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function edit_physiotherapy_labs_details()
    {   
        $id = $_POST['id'];
        $this->load->model('Model_physiotherapy_labs');
        $data['lab_data'] = $this->Model_physiotherapy_labs->get_all_physiotherapy_lab_data($id);
        $this->load->view('physiotherapy/modal_edit_physiotherapy_lab',$data);
    }
    public function edit_physiotherapy_labs()
    {
        $id = $_POST['lab_id'];
        $lab_name = $_POST['lab_name'];
        $email = $_POST['email'];
        $address = $_POST['address'];
        $p_city = $_POST['p_city'];
        $p_state = $_POST['p_state'];
        $mobile_no = $_POST['mobile_no'];
        $landline_no = $_POST['landline_no'];

        $this->load->model('Model_physiotherapy_labs');
        $data_category = array(
            'lab_name'=>$lab_name,
            'email'=>$email,
            'address'=>$address,
            'p_city'=>$p_city,
            'p_state'=>$p_state,
            'mobile_no'=>$mobile_no,
            'landline_no'=>$landline_no,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_physiotherapy_labs->update_physiotherapy_labs($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_physiotherapy_lab_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_physiotherapy_labs');
        $data_id = $this->Model_physiotherapy_labs->delete_physiotherapy_labs($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_physiotherapy_test_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_physiotherapy_test');
        $data_id = $this->Model_physiotherapy_test->delete_physiotherapy_tests($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function view_physiotherapy_test()
	{    
        $this->load->model('Model_physiotherapy_test');
		$data['test'] = $this->Model_physiotherapy_test->get_all_physiotherapy_test_details();
		$this->load->view('physiotherapy/view_physiotherapy_test',$data);	
	}
	public function add_physiotherapy_test_modal()
	{
        $this->load->model('Model_physiotherapy_labs');
        $data['physio_lab'] = $this->Model_physiotherapy_labs->get_physiotherapy_test();
    	$this->load->view('physiotherapy/modal_add_physiotherapy_test',$data);
	}
	public function add_physiotherapy_test()
    {
        $test_name = $_POST['test_name'];
        $description= $_POST['description'];
        $lab_id = explode(',', $_POST['lab_id']);
        $price = explode(',', $_POST['price']);
        $offer_price = explode(',', $_POST['offer_price']);
        $off = explode(',', $_POST['off']);

        $this->load->model('Model_physiotherapy_test');
        $this->load->model('Model_physiotherapy_test_price');
        $data_category = array(
            'test_name'=>$test_name,
            'description'=>$description,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_physiotherapy_test->insert_physiotherapy_test($data_category);

        if ($data_id) {
            for ($i=0; $i <count($lab_id) ; $i++) { 
                $data_size = array(
                    'test_id'=>$data_id,
                    'lab_id'=>$lab_id[$i],
                    'price'=>$price[$i],
                    'offer_price'=>$offer_price[$i],
                    'off'=>$off[$i],
                    'created_date_time'=>$this->current_date_time,
                );
                $this->Model_physiotherapy_test_price->insert_physiotherapy_test_price($data_size);
            }

            echo "Valid";
        }
    }
     public function edit_physiotherapy_test_data()
    {   
        $id = $_GET['id'];
        $this->load->model('Model_physiotherapy_test');
        $data['test_data'] = $this->Model_physiotherapy_test->get_all_physiotherapy_test_data($id);
        $this->load->model('Model_physiotherapy_labs');
        $data['physiotherapy_lab'] = $this->Model_physiotherapy_labs->get_physiotherapy_test();
        $this->load->model('Model_physiotherapy_test_price');
        $data['physiotherapy_test_data'] = $this->Model_physiotherapy_test_price->get_physiotherapy_test_price($id);
        $this->load->view('physiotherapy/edit_physiotherapy_test',$data);
    }
    public function add_physiotherapy_price_modal()
    {  
        $test_id = $_POST['test_id'];
        $lab_id = $_POST['lab_id'];
        $price = explode(',', $_POST['price']);
        $offer_price = explode(',', $_POST['offer_price']);
        $off = explode(',', $_POST['off']);

        $this->load->model('Model_physiotherapy_test_price');
        
            for ($i=0; $i <count($test_id) ; $i++) { 
                $data_size = array(
                    'test_id'=>$test_id,
                    'lab_id'=>$lab_id[$i],
                    'price'=>$price[$i],
                    'offer_price'=>$offer_price[$i],
                    'off'=>$off[$i],
                    'created_date_time'=>$this->current_date_time,
                );
                $this->Model_physiotherapy_test_price->insert_physiotherapy_test_price_data($data_size);
            }                
                 echo "Valid";   
    }
     public function edit_physiotherapy_test_details()
    {
        $id = $_POST['test_id'];
        $test_name = $_POST['test_name'];
        $description = $_POST['description'];

        $this->load->model('Model_physiotherapy_test');
        $data_category = array(
            'test_name'=>$test_name,
            'description'=>$description,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_physiotherapy_test->update_physiotherapy_test($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_physiotherapy_test_price_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_physiotherapy_test_price');
        $data_id = $this->Model_physiotherapy_test_price->delete_physiotherapy_test_price_data($id);
        if ($data_id) {
            echo "Valid";
        }
    }


    public function view_physiotherapy_cod_details()
    {    
        $this->load->model('Model_physiotherapy_order');
        $data['order'] = $this->Model_physiotherapy_order->get_all_physiotherapy_cod_order_details();
        $this->load->view('physiotherapy/view_physiotherapy_cod_details',$data);   
    }
    public function update_physiotherapy_cod_status_modal()
    { 
      $id = $_POST['id'];
      $this->load->model('Model_physiotherapy_order'); 
      $data['cod'] = $this->Model_physiotherapy_order->get_update_physiotherapy_cod_status_details($id);
      $this->load->view('physiotherapy/modal_update_physiotherapy_cod_status',$data);
    }
    public function update_physiotherapy_cod_status()
    {  
        $id = $_POST['status_id'];
        $status= $_POST['status'];

        $this->load->model('Model_physiotherapy_order');
        $data_category = array(
            'status'=>$status,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_physiotherapy_order->update_physiotherapy_cod_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
     public function view_physiotherapy_paid_details()
    {    
        $this->load->model('Model_physiotherapy_order');
        $data['order'] = $this->Model_physiotherapy_order->get_all_physiotherapy_paid_order_details();
        $this->load->model('Model_physiotherapy_prescription');
        $data['physiotherapy'] = $this->Model_physiotherapy_prescription->get_all_physiotherapy_prescription_confirm_details();
        $this->load->view('physiotherapy/view_physiotherapy_all_confirm_order_details',$data);   
    }
    public function upload_physiotherapy_report()
    {    
        $this->load->view('physiotherapy/upload_physiotherapy_report');   
    }
    public function update_physiotherapy_status_modal()
    {    
        $this->load->view('physiotherapy/modal_update_physiotherapy_status');   
    }
    public function view_physiotherapist_prescription()
    {    
        $this->load->model('Model_physiotherapy_prescription');
        $data['physiotherapy'] = $this->Model_physiotherapy_prescription->get_all_physiotherapy_prescription_details();
        // echo "<pre>";
        // print_r($data);
        // return;
        $this->load->view('physiotherapy/view_physiotherapy_prescription',$data);    
    }
   /* public function view_physiotherapist_confirm_prescription()
    {    
        $this->load->model('Model_physiotherapy_prescription');
        $data['physiotherapy'] = $this->Model_physiotherapy_prescription->get_all_physiotherapy_prescription_confirm_details();
        $this->load->view('physiotherapy/view_physiotherapy_confirm_prescription',$data);    
    }*/
    /*public function view_physiotherapist_delivered_prescription()
    {    
        $this->load->model('Model_physiotherapy_prescription');
        $data['physiotherapy'] = $this->Model_physiotherapy_prescription->get_all_physiotherapy_prescription_delivered_details();
        $this->load->view('physiotherapy/view_physiotherapy_delivered_prescription',$data);    
    }*/
   /* public function view_physiotherapist_cancelled_prescription()
    {    
        $this->load->model('Model_physiotherapy_prescription');
        $data['physiotherapy'] = $this->Model_physiotherapy_prescription->get_all_physiotherapy_prescription_cancelled_details();
        $this->load->view('physiotherapy/view_physiotherapy_cancelled_prescription',$data);    
    }*/
    public function apply_physiotherapy_price()
    {  
        $id= $_POST['id'];
        $update_price = $_POST['update_price'];

        $this->load->model('Model_physiotherapy_prescription');
        $data = array(
            'pricing'=>$update_price,
            'status' =>'1',
        );
        $data_id = $this->Model_physiotherapy_prescription->update_physiotherapy_prescription_pricing($data,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function physiotherapy_prescription_check_out_details()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_physiotherapy_prescription'); 
        $data['check_out'] = $this->Model_physiotherapy_prescription->get_physiotherapy_prescription_check_out_details($user_id);
        $data['get_check_out'] = $this->Model_physiotherapy_prescription->get_physiotherapy_prescription_all_check_out_details($user_id);
        $this->load->view('physiotherapy/view_physiotherapy_prescription_check_out',$data);
    } 
    public function add_physiotherapy_medicine_price()
    {  
        $id = $_POST['order_id'];
        $pricing = $_POST['price'];
        $medicine_name =$_POST['medicine_name'];
        $medicine_price =$_POST['medicine_price'];
        $qty =$_POST['qty'];
        
        $this->load->model('Model_physiotherapy_prescription');
        $data_category = array(
            'pricing'=>$pricing,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_physiotherapy_prescription->update_physiotherapy_total_price($data_category,$id);
        
        $medicine_name = explode(',',$medicine_name);
        $medicine_price = explode(',',$medicine_price);
        $qty = explode(',',$qty);
        
        if ($data_id) {
            for ($i=0; $i <count($medicine_name) ; $i++) {
                $data_size = array(
                    'prescription_id'=>$data_id,
                    'medicine_name'=>$medicine_name[$i],
                    'medicine_price'=>$medicine_price[$i],
                    'qty'=>$qty[$i],
                    'created_date_time'=>$this->current_date_time,
                );

                $this->load->model('Model_physiotherapy_medicine_data');   
                $this->Model_physiotherapy_medicine_data->insert_physiotherapy_medicine_data($data_size);
            }
             echo "Valid";
         } 
     
    }
     public function open_physiotherapy_prescription_invoice()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_physiotherapy_prescription'); 
        $data['user_details'] = $this->Model_physiotherapy_prescription->get_physiotherapy_prescription_invoice_details($user_id);
        $data['medicine_details'] = $this->Model_physiotherapy_prescription->get_physiotherapy_prescription_medicine_details($user_id);
        $this->load->view('physiotherapy/view_physiotherapy_prescription_invoice',$data);
    }
    public function print_physiotherapy_prescription_invoice_pdf()
    {
         $user_id = $_GET['user_id'];
        $this->load->model('Model_physiotherapy_prescription'); 
        $data['user_details'] = $this->Model_physiotherapy_prescription->get_physiotherapy_prescription_invoice_details($user_id);
        $data['medicine_details'] = $this->Model_physiotherapy_prescription->get_physiotherapy_prescription_medicine_details($user_id);
        $this->load->view('physiotherapy/print_physiotherapy_prescription_invoice',$data);
    }
    public function update_physiotherapy_report_modal()
    { 
      $id = $_POST['id'];
      $this->load->model('Model_physiotherapy_order'); 
      $data['pdf'] = $this->Model_physiotherapy_order->get_update_physiotherapy_report_details($id);
      $this->load->view('physiotherapy/modal_update_physiotherapy_report',$data);
    }
    public function update_physiotherapy_report_status()
    {  
        $id = $_POST['report_id'];
        $upload_report='';
        if(isset($_FILES['file']))
        {
            $info=pathinfo($_FILES['file']['name']);            
            if($info!='')
            {
                $time = microtime();
                $ext=$info['extension'];
                $upload_report=$_FILES['file']['name'];
                $upload_report = $time.$upload_report;
                $target='files/upload_radiology_report/'.$upload_report;
                move_uploaded_file($_FILES['file']['tmp_name'],$target);
            }
        }
        
        $this->load->model('Model_physiotherapy_order');
        $data_category = array(
            'upload_report'=>$upload_report,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_physiotherapy_order->update_physiotherapy_report_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function show_physiotherapy_user_detail_modal()
    {
        $id = $_POST['id'];
        $this->load->model('Model_physiotherapy_order');
        $data['user'] = $this->Model_physiotherapy_order->get_physiotherapy_user_details($id);
        $this->load->view('physiotherapy/modal_physiotherapy_user_details',$data);

    }
    public function show_physiotherapy_prescription_user_detail_modal()
    {
        $id = $_POST['id'];
        $this->load->model('Model_physiotherapy_prescription');
        $data['user'] = $this->Model_physiotherapy_prescription->get_physiotherapy_prescription_user_details($id);
        $this->load->view('physiotherapy/modal_physiotherapy_prescription_user_details',$data);

    }
    public function physiotherapy_check_out_details()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_physiotherapy_order'); 
        $data['check_out'] = $this->Model_physiotherapy_order->get_physiotherapy_check_out_details($user_id);
        $data['get_check_out'] = $this->Model_physiotherapy_order->get_physiotherapy_all_check_out_details($user_id);
        $this->load->view('physiotherapy/view_physiotherapy_check_out',$data);
    }
    public function update_physiotherapy_total_amount()
    {  
        $id = $_POST['order_id'];
        $gst_amount = $_POST['gst_amount'];
        $total_amount = $_POST['total_amount'];
        
        $this->load->model('Model_physiotherapy_order');
        $data_category = array(
            'gst_amount'=>$gst_amount,
            'total_amount'=>$total_amount,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_physiotherapy_order->update_physiotherapy_total_price($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function open_physiotherapy_invoice()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_physiotherapy_order'); 
        $data['user_details'] = $this->Model_physiotherapy_order->get_physiotherapy_invoice_details($user_id);
        $this->load->view('physiotherapy/view_physiotherapy_invoice',$data);
    }
    public function print_physiotherapy_invoice_pdf()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_physiotherapy_order'); 
        $data['user_details'] = $this->Model_physiotherapy_order->get_physiotherapy_invoice_details($user_id);
        $this->load->view('physiotherapy/print_physiotherapy_invoice',$data);
    }
    public function view_physiotherapy_delivered_order()
    {
        $this->load->model('Model_physiotherapy_order'); 
        $data['deliver'] = $this->Model_physiotherapy_order->get_physiotherapy_delivered_order();
         $this->load->model('Model_physiotherapy_prescription');
        $data['physiotherapy'] = $this->Model_physiotherapy_prescription->get_all_physiotherapy_prescription_delivered_details();
        $this->load->view('physiotherapy/view_physiotherapy_all_delivered_order_details',$data);
    }
    public function view_physiotherapy_cancelled_order()
    {
        $this->load->model('Model_physiotherapy_order'); 
        $data['deliver'] = $this->Model_physiotherapy_order->get_physiotherapy_cancelled_order();
        $this->load->model('Model_physiotherapy_prescription');
        $data['physiotherapy'] = $this->Model_physiotherapy_prescription->get_all_physiotherapy_prescription_cancelled_details();
        $this->load->view('physiotherapy/view_physiotherapy_all_cancelled_order_details',$data);
    }
    public function physiotherapy_prescription_status()
    {    
        $id = $_GET['id'];
        $this->load->model('Model_physiotherapy_prescription'); 
        $data['deliver'] = $this->Model_physiotherapy_prescription->get_physiotherapy_delivered_status_details($id);
        $data['payment'] = $this->Model_physiotherapy_prescription->get_physiotherapy_payment_status_details($id);
        $this->load->view('physiotherapy/physiotherapy_prescription_status',$data);   
    } 
    public function physiotherapy_prescription_deliver_status()
    {  
        $id = $_POST['status_id'];
        $status= $_POST['status'];

        $this->load->model('Model_physiotherapy_prescription');
        $data_category = array(
            'status'=>$status,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_physiotherapy_prescription->update_physiotherapy_delivered_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }
    public function physiotherapy_prescription_payment_status()
    {  
        $id = $_POST['payment_id'];
        $payment_type= $_POST['payment_type'];

        $this->load->model('Model_physiotherapy_prescription');
        $data_category = array(
            'payment_type'=>$payment_type,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_physiotherapy_prescription->update_physiotherapy_payment_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }
    public function delete_physiotherapy_cancelled_prescription()
    {
        $id= $_POST['id'];
        $this->load->model('Model_physiotherapy_prescription');
        $data_id = $this->Model_physiotherapy_prescription->delete_cancel_physiotherapy_prescription($id);
        if ($data_id) {
            echo "Valid";
        }
    } 
    public function delete_physiotherapy_cancelled_order()
    {
        $id= $_POST['id'];
        $this->load->model('Model_physiotherapy_order');
        $data_id = $this->Model_physiotherapy_order->delete_cancel_physiotherapy_order($id);
        if ($data_id) {
            echo "Valid";
        }
    } 
    public function physiotherapy_order_status()
    {
        $id = $_GET['id'];
        $this->load->model('Model_physiotherapy_order'); 
        $data['deliver'] = $this->Model_physiotherapy_order->get_physiotherapy_order_delivered_status_details($id);
        $data['payment'] = $this->Model_physiotherapy_order->get_physiotherapy_order_payment_status_details($id);
        $this->load->view('physiotherapy/physiotherapy_order_status',$data);
    }
    public function physiotherapy_order_deliver_status()
    {  
        $id = $_POST['status_id'];
        $delivery_status= $_POST['delivery_status'];

        $this->load->model('Model_physiotherapy_order');
        $data_category = array(
            'delivery_status'=>$delivery_status,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_physiotherapy_order->update_physiotherapy_order_delivered_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }
    public function physiotherapy_order_payment_status()
    {  
        $id = $_POST['payment_id'];
        $status= $_POST['payment_status'];

        $this->load->model('Model_physiotherapy_order');
        $data_category = array(
            'status'=>$status,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_physiotherapy_order->update_physiotherapy_order_payment_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
}
