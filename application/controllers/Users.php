<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public $current_date_time;
	public $login_id;
	public $login_role;

	public function __construct()
	{
		parent::__construct();
		$this->login_id = $this->session->userdata('login_id');
		$this->login_role = $this->session->userdata('login_role');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("Asia/Kolkata");
		}
		if (!$this->login_id) {
			redirect('Login');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}
	public function view_users_profile()
	{   
		$this->load->model('Model_user_master');
		$data['users'] = $this->Model_user_master->get_all_users_details();
		$this->load->view('users/view_user',$data);	
	}
    public function view_users_orders()
    {  
        $user_id = $_GET['user_id'];
        $this->load->model('Model_pharmacy');
        $data['pharmacy_order'] = $this->Model_pharmacy->get_all_pharmacy_order_of_perticuler_order($user_id);
        $this->load->model('Model_physiotherapy_order');
        $data['physiotherapy_order'] = $this->Model_physiotherapy_order->get_all_physiotherapy_order_of_perticuler_order($user_id);

        $this->load->model('Model_physiotherapy_prescription');
        $data['physiotherapy_prescription'] = $this->Model_physiotherapy_prescription->get_all_physiotherapy_prescription_of_perticuler_order($user_id);
        
        $this->load->model('Model_radiology_order');
        $data['radiology_order'] = $this->Model_radiology_order->get_all_radiology_order_of_perticuler_order($user_id);

        $this->load->model('Model_radiology_prescription');
        $data['radiology_prescription'] = $this->Model_radiology_prescription->get_all_radiology_prescription_of_perticuler_order($user_id);

        $this->load->model('Model_pathology_order');
        $data['pathology_order'] = $this->Model_pathology_order->get_all_pathology_order_of_perticuler_order($user_id);

        $this->load->model('Model_pathology_prescription');
        $data['pathology_prescription'] = $this->Model_pathology_prescription->get_all_pathology_prescription_of_perticuler_order($user_id);

        $this->load->model('Model_nursing_order');
        $data['nursing_order'] = $this->Model_nursing_order->get_all_nursing_order_of_perticuler_order($user_id);

        $this->load->model('Model_dietician_order_master');
        $data['dietician_order'] = $this->Model_dietician_order_master->get_all_dietician_order_of_perticuler_order($user_id);
        
        $this->load->view('users/view_users_orders',$data); 
    }
	public function get_message_for_notification_modal()
    {
        $this->load->view('notification/modal_user_notifications_details');
    }	

    public function save_bell_notification()
    {
        $notification = $_POST['notification'];
        $user_ids = $_POST['user_ids'];

        $this->load->model('Model_notification_user');
        $data_array = array(
            'notification'=>$notification,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_notification_user->insert_notification_user($data_array);
        
         
        $user_ids = explode(',',$user_ids);
        $response = array();

        if ($data_id) {
            for ($i=0; $i <count($user_ids) ; $i++) {
                $data_size = array(
                    'user_id'=>$user_ids[$i],
                    'message_id'=>$data_id,
                    'created_date_time'=>$this->current_date_time,
                );
                 $this->load->model('Model_notification_message_master');   
                $this->Model_notification_message_master->insert_message_details($data_size);
            }
             echo "Valid";
         }
    }
}