<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {

	public $current_date_time;
	public $login_id;
	public $login_role;

	public function __construct()
	{
		parent::__construct();
		$this->login_id = $this->session->userdata('login_id');
		$this->login_role = $this->session->userdata('login_role');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("Asia/Kolkata");
		}
		if (!$this->login_id) {
			redirect('Login');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}
    public function view_vendor_profile()
    {
        $id = $this->login_id;
        $this->load->model('Model_login');
        $data['profile'] = $this->Model_login->get_all_vendor_profile_details();
        $this->load->view('vendor/view_profile',$data);
    }
	public function view_vendor()
	{    
        $this->load->model('Model_vendor');
		$data['vendor'] = $this->Model_vendor->get_all_vendor_details();
		$this->load->view('vendor/view_vendor',$data);	
	}
	public function add_vendor_modal()
	{
		$this->load->view('vendor/modal_add_vendor');
	}
    public function add_vendor()
    {
        $name = $_POST['name'];
        $company_name = $_POST['company_name'];
        $email = $_POST['email'];
        $mobile_no = $_POST['mobile_no'];
        $address = $_POST['address'];
        $real_password = $_POST['password'];
        $password = md5($_POST['password']);
        $role = $_POST['role'];
       
        $this->load->model('Model_vendor');
        $data_category = array(
            'name'=>$name,
            'company_name'=>$company_name,
            'email'=>$email,
            'mobile_no'=>$mobile_no,
            'address'=>$address,
            'real_password'=>$real_password,
            'password'=>$password,
            'role'=>$role,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_vendor->insert_vendor($data_category);

        if ($data_id) {
            echo "Valid";
        }
    }
    public function update_vendor_profile()
    {   
        $id =$_POST['profile_id'];
        $name = $_POST['name'];
        $company_name = $_POST['company_name'];
        $mobile_no = $_POST['mobile_no'];
        $address = $_POST['address'];
        $real_password = $_POST['password'];
        $password = md5($_POST['password']);
       
        $this->load->model('Model_login');
        $data_category = array(
            'name'=>$name,
            'company_name'=>$company_name,
            'mobile_no'=>$mobile_no,
            'address'=>$address,
            'real_password'=>$real_password,
            'password'=>$password,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_login->update_vendor_profile($data_category,$id);

        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_vendor()
    {
        $id= $_POST['id'];
        $this->load->model('Model_login');
        $data_id = $this->Model_login->delete_vendor_data($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function add_banner_modal()
    {
        $this->load->view('banner/view_banner');
    }
    public function view_banner()
    {   
        $this->load->model('Model_banner_master');
        $data['banner'] = $this->Model_banner_master->get_all_banner();
        $this->load->view('banner/view_banner',$data);
    }
    public function add_banner()
    {
        $banner_image='';
        if(isset($_FILES['file']))
        {
            $info=pathinfo($_FILES['file']['name']);            
            if($info!='')
            {
                $time = microtime();
                $ext=$info['extension'];
                $banner_image=$_FILES['file']['name'];
                $banner_image = $time.$banner_image;
                $target='files/banner_img/'.$banner_image;
                move_uploaded_file($_FILES['file']['tmp_name'],$target);
            }
        }

        $this->load->model('Model_banner_master');
        $data_category = array(
            'banner_image'=>$banner_image,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_banner_master->insert_banner($data_category);
        if ($data_id) {
            
            echo "Valid";
        }
    }
    public function delete_banner()
    {
        $id = $_POST['id'];
        $this->load->model('Model_banner_master');
        $data_id = $this->Model_banner_master->delete_banner($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function onscreen_user_notification()
    {
        $this->load->model('Model_notification_onscreen_user');
        $data['notify'] = $this->Model_notification_onscreen_user->get_all_notification_onscreen();
        $this->load->view('notification/user_onscreen_notification',$data);
    }  
     public function save_user_onscreen_notification()
    {
        $start_date = change_date_format_ymd($_POST['start_date']);
        $end_date = change_date_format_ymd($_POST['end_date']);
        $notification = $_POST['notification'];

        $data_array = array(
            'start_date'=>$start_date,
            'end_date'=>$end_date,
            'notification'=>$notification,
            'created_date_time'=>$this->current_date_time,
        );

        $this->load->model('Model_notification_onscreen_user');

        $notification_id = $this->Model_notification_onscreen_user->insert_notifications_onscreen($data_array);

        if ($notification_id) {
            echo "Valid";
        }
    }
    public function delete_onscreen_notification()
    {
        $id= $_POST['id'];
        $this->load->model('Model_notification_onscreen_user');
        $data_id = $this->Model_notification_onscreen_user->delete_onscreen_notification($id);
        if ($data_id) {
            echo "Valid";
        }
    }  
    public function bell_user_notification()
    {
        $this->load->model('Model_user_master');
        $data['users'] = $this->Model_user_master->get_all_notification_users_details();
        $this->load->view('notification/user_notification',$data);
    }
   
}
