<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

	public $current_date_time;

	public function __construct()
	{
		parent::__construct();
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("Asia/Kolkata");
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

    public function login(){

    	$data = json_decode(file_get_contents('php://input'), true);

    	if ($data['mode'] == 0) {
    		$mobile = $data['mobile'];
    		$this->load->model('Model_user_otp');
			$check_mobile = $this->Model_user_otp->check_mobile_otp($mobile);
			if ($check_mobile) {
				$otp = rand(100000,999999);
				$data_otp = array(
					'otp'=>$otp,
					'created_date_time'=>$this->current_date_time,
				);

				$otp_id = $this->Model_user_otp->update_otp($data_otp,$mobile);

				if ($mobile) {
					$json["info"] = array("message" => "OTP Send!", "status" => 200);
					$json["data"] = array("mobile" => $mobile);
					json_output($json['info']['status'], $json);
				}
				else
				{
					$json["info"] = array("message" => "OTP not Send!", "status" => 201);
					$json["data"] = array("mobile" => $mobile);
					json_output($json['info']['status'], $json);
				}
			}
			else
			{
				$otp = rand(100000,999999);
				$data_complete = array(
					'mobile_no'=>$mobile,
					'otp'=>$otp,
					'status'=>'0',
					'created_date_time'=>$this->current_date_time,
				);

				$otp_id = $this->Model_user_otp->insert_otp($data_complete);

				if ($otp_id) {
					$message = $otp . " is your One Time Password(OTP) to verify your phone number with Medificians mobile application.";
					send_otp_sms1($mobile, $message);
					$json["info"] = array("message" => "OTP Send!", "status" => 200);
					$json["data"] = array("mobile" => $mobile);
					json_output($json['info']['status'], $json);
				}
				else
				{
					$json["info"] = array("message" => "OTP not Send!", "status" => 201);
					$json["data"] = array("mobile" => $mobile);
					json_output($json['info']['status'], $json);
				}
			}		
		}

		if ($data['mode'] == 1) {
			$mobile = $data['mobile'];
			$this->load->model('Model_user_otp');
			$check_mobile = $this->Model_user_otp->check_mobile_otp($mobile);

			if ($check_mobile) {
				$message = $check_mobile->otp . " is your One Time Password(OTP) to verify your phone number with Medificians mobile application.";
				send_otp_sms1($mobile, $message);
				$json["info"] = array("message" => "OTP has been sent to your mobile number!", "status" => 200);
				$json["data"] = array("mobile" => $mobile);
				json_output($json['info']['status'], $json);
			} else {
				$json["info"] = array("message" => "Mobile No Is incorrect!", "status" => 201);
				$json["data"] = array("mobile" => $mobile);
				json_output($json['info']['status'], $json);
			}
		}
		//varified otp
		if ($data['mode'] ==2 ) {
			$mobile = $data['mobile'];
			$otp = $data['otp'];

			$this->load->model('Model_user_otp');
			$check_mobile = $this->Model_user_otp->verify_user_otp_details($mobile,$otp);
			if ($check_mobile) {
				$this->load->model('Model_user_master');
				$user_details = $this->Model_user_master->check_mobile_details($mobile);

				if ($user_details) {
					$json["info"] = array("message" => "Success", "status" => 200);
					$json["data"] = $user_details;
					json_output($json['info']['status'], $json);
				}
				else
				{
					$data_user = array(
						'mobile_no'=>$mobile,
						'refer_code'=>random_num(6),
						'created_date_time'=>$this->current_date_time,
					);

					$data_id = $this->Model_user_master->insert_customer($data_user);
					$user_details = $this->Model_user_master->get_customer_by_id($data_id);
					$json["info"] = array("message" => "Success", "status" => 200);
					$json["data"] = $user_details;
					json_output($json['info']['status'], $json);
				}
			}
			else{
				$json["info"] = array("message" => "OTP Is incorrect!", "status" => 201);
				$json["data"] = array("mobile" => $mobile);
				json_output($json['info']['status'], $json);
			}
		}
		if ($data['mode'] == 3) {
			$user_id = $data['user_id'];
			$name = $data['name'];
			$date_of_birth = $data['date_of_birth'];
			$email = $data['email'];
			$address = $data['address'];
			$city = $data['city'];
			$state = $data['state'];
			$pincode = $data['pincode'];
			$referer_code =$data['referer_code'];

			$data_user = array(
				"name"=>$name,
				"date_of_birth"=>$date_of_birth,
				"email"=>$email,
				"address"=>$address,
				"city"=>$city,
				"state"=>$state,
				"pincode"=>$pincode,
				"referer_code"=>$referer_code,
				"created_date_time"=>$this->current_date_time,
			);

			$this->load->model('Model_user_master');
			$data_id = $this->Model_user_master->update_user_profile($data_user,$user_id);

			if ($data_id) {
				$json["info"] = array("message" => "Profile Updated Successfully!", "status" => 200);
				json_output($json['info']['status'], $json);
			}
			else
			{
				$json["info"] = array("message" => "Profile Updated Failed!", "status" => 201);
				json_output($json['info']['status'], $json);
			}
		}
    }

    public function home()
    {
    	$data = json_decode(file_get_contents('php://input'), true);
		
		if ($data['mode']==0) {
			$user_id = $data['user_id'];
			$this->load->model('Model_user_master');
			$result_data['user_details'] = $this->Model_user_master->get_customer_by_id($user_id);
			$this->load->model('Model_banner_master');
			$result_data['banner'] = $this->Model_banner_master->get_all_banner();

			foreach ($result_data['banner'] as $key ) {
				$key->banner_image=base_url().'files/banner_img/'.$key->banner_image;
			}
			if ($result_data) {
				$json["info"] = array("message" => "Success!", "status" => 200);
				$json["data"] = $result_data;
	            json_output($json['info']['status'], $json);
			}
			else
			{
				$json["info"] = array("message" => "Failed!", "status" => 201);
				json_output($json['info']['status'], $json);
			}	
        }
    }


}
