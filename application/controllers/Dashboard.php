<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public $current_date_time;
	public $login_id;
	public $login_role;

	public function __construct()
	{
		parent::__construct();
		$this->login_id = $this->session->userdata('login_id');
		$this->login_role = $this->session->userdata('login_role');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("Asia/Kolkata");
		}
		if (!$this->login_id) {
			redirect('Login');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function index()
	{ 
		$this->load->model('Model_user_master');
		$data['user_count'] = $this->Model_user_master->get_count_total_users();
		$this->load->model('Model_pharmacy');
		/*$data['total_sell_count'] = $this->Model_pharmacy->get_count_total_sell_users();
*/
		
		$data['pharmacy_count'] = $this->Model_pharmacy->get_count_total_pharmacy_orders();
		$data['pharmacy_pending_count'] = $this->Model_pharmacy->count_total_pharmacy_pending_orders();
		$data['pharmacy_confirm_count'] = $this->Model_pharmacy->count_total_pharmacy_confirm_orders();
		$data['pharmacy_delivered_count'] = $this->Model_pharmacy->count_total_pharmacy_delivered_orders();

		$date_today = date('Y-m-d');
		$data['pharmacy_today_sale'] = $this->Model_pharmacy->count_pharmacy_today_sell($date_today,$date_today);

		$month_date = date('Y-m-d', strtotime('-30 days'));
		$data['pharmacy_monthly_sale'] = $this->Model_pharmacy->count_pharmacy_today_sell($month_date,$date_today);

		$year_date = date('Y-m-d', strtotime('-365 days'));
		$data['pharmacy_yearly_sale'] = $this->Model_pharmacy->count_pharmacy_today_sell($year_date,$date_today);

        $this->load->model('Model_pathology_order');
		$data['pathology_count'] = $this->Model_pathology_order->get_count_total_pathology_orders();
		$data['pathology_pending_count'] = $this->Model_pathology_order->count_total_pathology_pending_orders();
		$data['pathology_confirm_count'] = $this->Model_pathology_order->count_total_pathology_confirm_orders();
		$this->load->model('Model_pathology_prescription');
		$data['pathology_prescription_count'] = $this->Model_pathology_prescription->count_total_pathology_prescription_orders();

		$date_today = date('Y-m-d');
		$data['pathology_today_sale'] = $this->Model_pathology_order->count_pathology_today_sell($date_today,$date_today);

		$date_today = date('Y-m-d');
		$data['pathology_prescription_today_sale'] = $this->Model_pathology_prescription->count_pathology_prescription_today_sell($date_today,$date_today);

		$month_date = date('Y-m-d', strtotime('-30 days'));
		$data['pathology_monthly_sale'] = $this->Model_pathology_order->count_pathology_today_sell($month_date,$date_today);

		$month_date = date('Y-m-d', strtotime('-30 days'));
		$data['pathology_prescription_monthly_sale'] = $this->Model_pathology_prescription->count_pathology_prescription_today_sell($month_date,$date_today);

		$year_date = date('Y-m-d', strtotime('-365 days'));
		$data['pathology_yearly_sale'] = $this->Model_pathology_order->count_pathology_today_sell($year_date,$date_today);

		$year_date = date('Y-m-d', strtotime('-365 days'));
		$data['pathology_prescription_yearly_sale'] = $this->Model_pathology_prescription->count_pathology_prescription_today_sell($year_date,$date_today);

		$this->load->model('Model_radiology_order');
		$data['radiology_count'] = $this->Model_radiology_order->get_count_total_radiology_orders();
		$data['radiology_pending_count'] = $this->Model_radiology_order->count_total_radiology_pending_orders();
		$data['radiology_confirm_count'] = $this->Model_radiology_order->count_total_radiology_confirm_orders();

		$this->load->model('Model_radiology_prescription');
		$data['radiology_prescription_count'] = $this->Model_radiology_prescription->count_total_radiology_prescription_orders();
        
        $date_today = date('Y-m-d');
		$data['radiology_today_sale'] = $this->Model_radiology_order->count_radiology_today_sell($date_today,$date_today);

		$date_today = date('Y-m-d');
		$data['radiology_prescription_today_sale'] = $this->Model_radiology_prescription->count_radiology_prescription_today_sell($date_today,$date_today);

		$month_date = date('Y-m-d', strtotime('-30 days'));
		$data['radiology_monthly_sale'] = $this->Model_radiology_order->count_radiology_today_sell($month_date,$date_today);

		$month_date = date('Y-m-d', strtotime('-30 days'));
		$data['radiology_prescription_monthly_sale'] = $this->Model_radiology_prescription->count_radiology_prescription_today_sell($month_date,$date_today);

		$year_date = date('Y-m-d', strtotime('-365 days'));
		$data['radiology_yearly_sale'] = $this->Model_radiology_order->count_radiology_today_sell($year_date,$date_today);

		$year_date = date('Y-m-d', strtotime('-365 days'));
		$data['radiology_prescription_yearly_sale'] = $this->Model_radiology_prescription->count_radiology_prescription_today_sell($year_date,$date_today);
         
        $this->load->model('Model_physiotherapy_order');
		$data['physiotherapy_count'] = $this->Model_physiotherapy_order->get_count_total_physiotherapy_orders();
		$data['physiotherapy_pending_count'] = $this->Model_physiotherapy_order->count_total_physiotherapy_pending_orders();
		$data['physiotherapy_confirm_count'] = $this->Model_physiotherapy_order->count_total_physiotherapy_confirm_orders();
		$this->load->model('Model_physiotherapy_prescription');
		$data['physiotherapy_prescription_count'] = $this->Model_physiotherapy_prescription->count_total_physiotherapy_prescription_orders();
        
        $date_today = date('Y-m-d');
		$data['physiotherapy_today_sale'] = $this->Model_physiotherapy_order->count_physiotherapy_today_sell($date_today,$date_today);

		$date_today = date('Y-m-d');
		$data['physiotherapy_prescription_today_sale'] = $this->Model_physiotherapy_prescription->count_physiotherapy_prescription_today_sell($date_today,$date_today);

		$month_date = date('Y-m-d', strtotime('-30 days'));
		$data['physiotherapy_monthly_sale'] = $this->Model_physiotherapy_order->count_physiotherapy_today_sell($month_date,$date_today);

		$month_date = date('Y-m-d', strtotime('-30 days'));
		$data['physiotherapy_prescription_monthly_sale'] = $this->Model_physiotherapy_prescription->count_physiotherapy_prescription_today_sell($month_date,$date_today);

		$year_date = date('Y-m-d', strtotime('-365 days'));
		$data['physiotherapy_yearly_sale'] = $this->Model_physiotherapy_order->count_physiotherapy_today_sell($year_date,$date_today); 

		$year_date = date('Y-m-d', strtotime('-365 days'));
		$data['physiotherapy_prescription_yearly_sale'] = $this->Model_physiotherapy_prescription->count_physiotherapy_prescription_today_sell($year_date,$date_today); 

		$this->load->model('Model_dietician_order_master');
		$data['dietician_count'] = $this->Model_dietician_order_master->get_count_total_dietician_orders();
        
        $date_today = date('Y-m-d');
		$data['dietician_today_sale'] = $this->Model_dietician_order_master->count_dietician_today_sell($date_today,$date_today);

		$month_date = date('Y-m-d', strtotime('-30 days'));
		$data['dietician_monthly_sale'] = $this->Model_dietician_order_master->count_dietician_today_sell($month_date,$date_today);

		$year_date = date('Y-m-d', strtotime('-365 days'));
		$data['dietician_yearly_sale'] = $this->Model_dietician_order_master->count_dietician_today_sell($year_date,$date_today); 

		$this->load->model('Model_nursing_order');
		$data['nursing_count'] = $this->Model_nursing_order->get_count_total_nursing_orders();
		$data['pending_count'] = $this->Model_nursing_order->get_count_total_nursing_pending_orders();
		$data['confirm_count'] = $this->Model_nursing_order->get_count_total_nursing_confirm_orders();
        
        $date_today = date('Y-m-d');
		$data['nursing_today_sale'] = $this->Model_nursing_order->count_nursing_today_sell($date_today,$date_today);

		$month_date = date('Y-m-d', strtotime('-30 days'));
		$data['nursing_monthly_sale'] = $this->Model_nursing_order->count_nursing_today_sell($month_date,$date_today);

		$year_date = date('Y-m-d', strtotime('-365 days'));
		$data['nursing_yearly_sale'] = $this->Model_nursing_order->count_nursing_today_sell($year_date,$date_today); 

		$this->load->view('dashboard',$data);
	
	}
	public function ditesian_dashboard()
	{
		$this->load->view('ditasian_dashboard');
	}
	public function nursing_dashboard()
	{
		$this->load->view('nursing_dashboard');
	}
	public function physiotherapy_dashboard()
	{
		$this->load->view('physiotherapy_dashboard');
	}
	public function pharmacy_dashboard()
	{
		$this->load->model('Model_pharmacy');
		$data['pharmacy_count'] = $this->Model_pharmacy->get_count_total_pharmacy_orders();
		$data['pharmacy_pending_count'] = $this->Model_pharmacy->count_total_pharmacy_pending_orders();
		$data['pharmacy_confirm_count'] = $this->Model_pharmacy->count_total_pharmacy_confirm_orders();
		$data['pharmacy_delivered_count'] = $this->Model_pharmacy->count_total_pharmacy_delivered_orders();

		$date_today = date('Y-m-d');
		$data['pharmacy_today_sale'] = $this->Model_pharmacy->count_pharmacy_today_sell($date_today,$date_today);

		$month_date = date('Y-m-d', strtotime('-30 days'));
		$data['pharmacy_monthly_sale'] = $this->Model_pharmacy->count_pharmacy_today_sell($month_date,$date_today);

		$year_date = date('Y-m-d', strtotime('-365 days'));
		$data['pharmacy_yearly_sale'] = $this->Model_pharmacy->count_pharmacy_today_sell($year_date,$date_today);
		$this->load->view('pharmacy_dashboard',$data);
	}		
}
