<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pathology extends CI_Controller {

	public $current_date_time;
	public $login_id;
	public $login_role;

	public function __construct()
	{
		parent::__construct();
		$this->login_id = $this->session->userdata('login_id');
		$this->login_role = $this->session->userdata('login_role');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("Asia/Kolkata");
		}
		if (!$this->login_id) {
			redirect('Login');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function view_pathology_labs()
	{    
        $this->load->model('Model_pathology_labs');
		$data['lab'] = $this->Model_pathology_labs->get_all_labs_details();
		$this->load->view('pathology/view_labs',$data);	
	}
	public function add_labs_modal()
	{   
		$this->load->view('pathology/modal_add_lab');
	}
    public function add_labs()
    {
        $lab_name = $_POST['lab_name'];
        $email = $_POST['email'];
        $address = $_POST['address'];
        $city = $_POST['city'];
        $c_state = $_POST['state'];
        $mobile_no = $_POST['mobile_no'];
        $landline_no = $_POST['landline_no'];

       
        $this->load->model('Model_pathology_labs');
        $data_category = array(
            'lab_name'=>$lab_name,
            'email'=>$email,
            'address'=>$address,
            'city'=>$city,
            'c_state'=>$c_state,
            'mobile_no'=>$mobile_no,
            'landline_no'=>$landline_no,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_pathology_labs->insert_labs($data_category);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function edit_labs_details()
    {   
        $id = $_POST['id'];
        $this->load->model('Model_pathology_labs');
        $data['lab_data'] = $this->Model_pathology_labs->get_all_lab_data($id);
       /* echo "<pre>";
        print_r($data['lab_data']);
        return;*/
        $this->load->view('pathology/modal_edit_lab',$data);
    }
    public function edit_labs()
    {
        $id = $_POST['lab_id'];
        $lab_name = $_POST['lab_name'];
        $email = $_POST['email'];
        $address = $_POST['address'];
        $city = $_POST['city'];
        $c_state = $_POST['state'];
        $mobile_no = $_POST['mobile_no'];
        $landline_no = $_POST['landline_no'];

       
        $this->load->model('Model_pathology_labs');
        $data_category = array(
            'lab_name'=>$lab_name,
            'email'=>$email,
            'address'=>$address,
            'city'=>$city,
            'c_state'=>$c_state,
            'mobile_no'=>$mobile_no,
            'landline_no'=>$landline_no,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_pathology_labs->update_labs($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_lab_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_pathology_labs');
        $data_id = $this->Model_pathology_labs->delete_lab($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function view_pathology_test()
	{    
        $this->load->model('Model_pathology_test');
		$data['test'] = $this->Model_pathology_test->get_all_test_details();
        /*echo "<pre>";
        print_r($data);
        return;*/
		$this->load->view('pathology/view_test',$data);	
	}
	public function add_test_modal()
	{   
        $this->load->model('Model_pathology_labs');
        $data['pathology_lab'] = $this->Model_pathology_labs->get_pathology_test();
		$this->load->view('pathology/modal_add_test',$data);
	}
    public function edit_test_data()
    {   
        $id = $_GET['id'];
        $this->load->model('Model_pathology_test');
        $data['test_data'] = $this->Model_pathology_test->get_all_test_data($id);
        $this->load->model('Model_pathology_labs');
        $data['pathology_lab'] = $this->Model_pathology_labs->get_pathology_test();
        $this->load->model('Model_pathology_test_price');
        $data['pathology_test_data'] = $this->Model_pathology_test_price->get_pathology_test_price($id);
        $this->load->view('pathology/edit_test_details',$data);
    }
    public function add_price_modal()
    {  
        $test_id = $_POST['test_id'];
        $lab_id = $_POST['lab_id'];
        $price = explode(',', $_POST['price']);
        $offer_price = explode(',', $_POST['offer_price']);
        $off = explode(',', $_POST['off']);

        $this->load->model('Model_pathology_test_price');
        
            for ($i=0; $i <count($test_id) ; $i++) { 
                $data_size = array(
                    'test_id'=>$test_id,
                    'lab_id'=>$lab_id[$i],
                    'price'=>$price[$i],
                    'offer_price'=>$offer_price[$i],
                    'off'=>$off[$i],
                    'created_date_time'=>$this->current_date_time,
                );
                $this->Model_pathology_test_price->insert_pathology_test_price_data($data_size);
            }                
                 echo "Valid";   
    }
    public function edit_test_details()
    {
        $id = $_POST['test_id'];
        $test_name = $_POST['test_name'];
        $category= $_POST['category_name'];
        $description = $_POST['description'];

        $this->load->model('Model_pathology_test');
        $data_category = array(
            'test_name'=>$test_name,
            'category'=>$category,
            'description'=>$description,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_pathology_test->update_pathology_test($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_test_price_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_pathology_test_price');
        $data_id = $this->Model_pathology_test_price->delete_test_price_data($id);
        if ($data_id) {
            echo "Valid";
        }
    }
	public function add_pathology_test()
    {
        $test_name = $_POST['test_name'];
        $category= $_POST['category_name'];
        $description= $_POST['description'];
        $lab_id = explode(',', $_POST['lab_id']);
        $price = explode(',', $_POST['price']);
        $offer_price = explode(',', $_POST['offer_price']);
        $off = explode(',', $_POST['off']);

        $this->load->model('Model_pathology_test');
        $this->load->model('Model_pathology_test_price');
        $data_category = array(
            'test_name'=>$test_name,
            'category'=>$category,
            'description'=>$description,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_pathology_test->insert_pathology_test($data_category);

        if ($data_id) {
            for ($i=0; $i <count($lab_id) ; $i++) { 
                $data_size = array(
                    'test_id'=>$data_id,
                    'lab_id'=>$lab_id[$i],
                    'price'=>$price[$i],
                    'offer_price'=>$offer_price[$i],
                    'off'=>$off[$i],
                );
                $this->Model_pathology_test_price->insert_pathology_test_price($data_size);
            }

            echo "Valid";
        }
    }
    public function delete_test_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_pathology_test');
        $data_id = $this->Model_pathology_test->delete_test($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function view_health_package_test()
	{    
        $this->load->model('Model_health_package');
        $data['test'] = $this->Model_health_package->get_all_health_pkg_details();
		$this->load->view('pathology/view_health_package',$data);	
	}
    public function add_health_pkg_modal()
    {   
        $this->load->model('Model_pathology_labs');
        $data['pathology_lab'] = $this->Model_pathology_labs->get_pathology_health_labs(); 
        $this->load->view('pathology/modal_add_health_pkg',$data);   
    }
    public function update_featured()
    {
        $id = $_POST['id'];
        $this->load->model('Model_health_package');
        $featured = $this->Model_health_package->get_featured($id);
        $data_array = array();

        if ($featured->featured == 0) {
            $data_array['featured'] = 1;
        }
        else
        {
            $data_array['featured'] = 0;
        }
        $data_id  = $this->Model_health_package->update_featured($data_array,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function add_health_package_test()
    {
        $package_name = $_POST['package_name'];
        $category= $_POST['category_name'];
        $description= $_POST['description'];
        $lab_id = explode(',', $_POST['lab_id']);
        $price = explode(',', $_POST['price']);
        $offer_price = explode(',', $_POST['offer_price']);
        $off = explode(',', $_POST['off']);

        $this->load->model('Model_health_package');
        $this->load->model('Model_health_package_price');
        $data_category = array(
            'package_name'=>$package_name,
            'category'=>$category,
            'description'=>$description,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_health_package->insert_health_package($data_category);

        if ($data_id) {
            for ($i=0; $i <count($lab_id) ; $i++) { 
                $data_size = array(
                    'package_id'=>$data_id,
                    'lab_id'=>$lab_id[$i],
                    'price'=>$price[$i],
                    'offer_price'=>$offer_price[$i],
                    'off'=>$off[$i],
                    'created_date_time'=>$this->current_date_time,
                );
                $this->Model_health_package_price->insert_health_package_price($data_size);
            }

            echo "Valid";
        }
    }
    public function edit_health_price_test_data()
    {   
        $id = $_GET['id'];
        $this->load->model('Model_health_package');
        $data['test_data'] = $this->Model_health_package->get_all_health_test_data($id);
        $this->load->model('Model_pathology_labs');
        $data['pathology_lab'] = $this->Model_pathology_labs->get_pathology_test();
        $this->load->model('Model_health_package_price');
        $data['pathology_test_data'] = $this->Model_health_package_price->get_health_test_price($id);
        $this->load->view('pathology/edit_health_package_details',$data);
    }
    public function add_health_price_modal()
    {  
        $package_id = $_POST['package_id'];
        $lab_id = $_POST['lab_id'];
        $price = explode(',', $_POST['price']);
        $offer_price = explode(',', $_POST['offer_price']);
        $off = explode(',', $_POST['off']);

        $this->load->model('Model_health_package_price');
        
            for ($i=0; $i <count($package_id) ; $i++) { 
                $data_size = array(
                    'package_id'=>$package_id,
                    'lab_id'=>$lab_id[$i],
                    'price'=>$price[$i],
                    'offer_price'=>$offer_price[$i],
                    'off'=>$off[$i],
                    'created_date_time'=>$this->current_date_time,
                );
                $this->Model_health_package_price->insert_health_pkg_test_price_data($data_size);
            }                
                 echo "Valid";   
    }
    public function edit_heatlh_pkg_test_details()
    {
        $id = $_POST['package_id'];
        $package_name = $_POST['package_name'];
        $category= $_POST['category_name'];
        $description = $_POST['description'];

        $this->load->model('Model_health_package');
        $data_category = array(
            'package_name'=>$package_name,
            'category'=>$category,
            'description'=>$description,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_health_package->update_health_package($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_health_test_price_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_health_package_price');
        $data_id = $this->Model_health_package_price->delete_health_test_price_data($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_health_package_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_health_package');
        $data_id = $this->Model_health_package->delete_health_package_price_data($id);
        if ($data_id) {
            echo "Valid";
        }
    }
	public function view_executive_health_package_test()
	{    
        $this->load->model('Model_executive_health_package');
        $data['test'] = $this->Model_executive_health_package->get_all_executive_health_pkg_details();
		$this->load->view('pathology/view_executive_health_package',$data);	
	}
    public function add_executive_health_pkg_modal()
    {   
        $this->load->model('Model_pathology_labs');
        $data['pathology_lab'] = $this->Model_pathology_labs->get_pathology_executive_health_labs(); 
        $this->load->view('pathology/modal_add_executive_health_pkg',$data);   
    }
    public function add_executive_health_package_test()
    {
        $package_name = $_POST['package_name'];
        $description= $_POST['description'];
        $lab_id = explode(',', $_POST['lab_id']);
        $price = explode(',', $_POST['price']);
        $offer_price = explode(',', $_POST['offer_price']);
        $off = explode(',', $_POST['off']);

        $this->load->model('Model_executive_health_package');
        $this->load->model('Model_executive_health_package_price');
        $data_category = array(
            'package_name'=>$package_name,
            'description'=>$description,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_executive_health_package->insert_executive_health_package($data_category);

        if ($data_id) {
            for ($i=0; $i <count($lab_id) ; $i++) { 
                $data_size = array(
                    'package_id'=>$data_id,
                    'lab_id'=>$lab_id[$i],
                    'price'=>$price[$i],
                    'offer_price'=>$offer_price[$i],
                    'off'=>$off[$i],
                    'created_date_time'=>$this->current_date_time,
                );
                $this->Model_executive_health_package_price->insert_executive_health_package_price($data_size);
            }

            echo "Valid";
        }
    }
     public function edit_executive_health_price_test_data()
    {   
        $id = $_GET['id'];
        $this->load->model('Model_executive_health_package');
        $data['test_data'] = $this->Model_executive_health_package->get_all_executive_health_test_data($id);
        $this->load->model('Model_pathology_labs');
        $data['pathology_lab'] = $this->Model_pathology_labs->get_pathology_test();
        $this->load->model('Model_executive_health_package_price');
        $data['pathology_test_data'] = $this->Model_executive_health_package_price->get_executive_health_test_price($id);
        $this->load->view('pathology/edit_executive_health_package_details',$data);
    }
    public function add_executive_health_price_modal()
    {  
        $package_id = $_POST['package_id'];
        $lab_id = $_POST['lab_id'];
        $price = explode(',', $_POST['price']);
        $offer_price = explode(',', $_POST['offer_price']);
        $off = explode(',', $_POST['off']);

        $this->load->model('Model_executive_health_package_price');
        
            for ($i=0; $i <count($package_id) ; $i++) { 
                $data_size = array(
                    'package_id'=>$package_id,
                    'lab_id'=>$lab_id[$i],
                    'price'=>$price[$i],
                    'offer_price'=>$offer_price[$i],
                    'off'=>$off[$i],
                    'created_date_time'=>$this->current_date_time,
                );
                $this->Model_executive_health_package_price->insert_executive_health_pkg_test_price_data($data_size);
            }                
                 echo "Valid";   
    }
    public function edit_executive_heatlh_pkg_test_details()
    {
        $id = $_POST['package_id'];
        $package_name = $_POST['package_name'];
        $description = $_POST['description'];

        $this->load->model('Model_executive_health_package');
        $data_category = array(
            'package_name'=>$package_name,
            'description'=>$description,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_executive_health_package->update_executive_health_package($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_executive_health_test_price_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_executive_health_package_price');
        $data_id = $this->Model_executive_health_package_price->delete_executive_health_test_price_data($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_executive_health_package_details()
    {
        $id= $_POST['id'];
        $this->load->model('Model_executive_health_package');
        $data_id = $this->Model_executive_health_package->delete_executive_health_package_price_data($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function view_pathologys_paid_order_details()
    {  
        $this->load->model('Model_pathology_order');
        $data['appointment'] = $this->Model_pathology_order->get_all_paid_order_details();
        $this->load->model('Model_pathology_prescription');
        $data['pathology'] = $this->Model_pathology_prescription->get_all_pathology_prescription_confirm_details();
        $this->load->view('pathology/view_all_confirm_order_details',$data);   
    }
    public function view_pathology_prescription()
    {    
        $this->load->model('Model_pathology_prescription');
        $data['pathology'] = $this->Model_pathology_prescription->get_all_pathology_prescription_details();
        $this->load->view('pathology/view_pathology_prescription',$data);    
    }
    /*public function view_pathology_confirm_prescription()
    {    
        $this->load->model('Model_pathology_order');
        $data['appointment'] = $this->Model_pathology_order->get_all_paid_order_details();
        $this->load->model('Model_pathology_prescription');
        $data['pathology'] = $this->Model_pathology_prescription->get_all_pathology_prescription_confirm_details();
        $this->load->view('pathology/view_all_confirm_order_details',$data);    
    }*/
   /* public function view_pathology_delivered_prescription()
    {    
        $this->load->model('Model_pathology_prescription');
        $data['pathology'] = $this->Model_pathology_prescription->get_all_pathology_prescription_delivered_details();
        $this->load->view('pathology/view_all_delivered_order_details',$data);    
    }
    public function view_pathology_cancelled_prescription()
    {    
        $this->load->model('Model_pathology_prescription');
        $data['pathology'] = $this->Model_pathology_prescription->get_all_pathology_prescription_cancelled_details();
        $this->load->view('pathology/view_pathology_cancelled_prescription',$data);    
    }*/
    public function delete_pathology_cancelled_prescription()
    {
        $id= $_POST['id'];
        $this->load->model('Model_pathology_prescription');
        $data_id = $this->Model_pathology_prescription->delete_cancel_pathology_prescription($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function delete_pathology_cancelled_order()
    {
        $id= $_POST['id'];
        $this->load->model('Model_pathology_order');
        $data_id = $this->Model_pathology_order->delete_cancel_pathology_order($id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function apply_pathology_prescription_price()
    {  
        $id= $_POST['id'];
        $update_price = $_POST['update_price'];

        $this->load->model('Model_pathology_prescription');
        $data_category = array(
            'pricing'=>$update_price,
            'status' =>'1',
        );
        $data_id = $this->Model_pathology_prescription->update_pathology_prescription_pricing($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function show_pathology_prescription_user_detail_modal()
    {
        $id = $_POST['id'];
        $this->load->model('Model_pathology_prescription');
        $data['user'] = $this->Model_pathology_prescription->get_pathology_prescription_user_details($id);
        $this->load->view('pathology/modal_pathology_prescription_user_details',$data);

    }
    public function pathology_prescription_check_out_details()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_pathology_prescription'); 
        $data['check_out'] = $this->Model_pathology_prescription->get_prescription_check_out_details($user_id);
        $data['get_check_out'] = $this->Model_pathology_prescription->get_prescription_all_check_out_details($user_id);
        $this->load->view('pathology/view_prescription_check_out',$data);
    }
    public function pathology_prescription_status()
    {    
        $id = $_GET['id'];
        $this->load->model('Model_pathology_prescription'); 
        $data['deliver'] = $this->Model_pathology_prescription->get_pathology_delivered_status_details($id);
        $data['payment'] = $this->Model_pathology_prescription->get_pathology_payment_status_details($id);
        $this->load->view('pathology/pathology_prescription_status',$data);   
    } 
    public function pathology_prescription_deliver_status()
    {  
        $id = $_POST['status_id'];
        $status= $_POST['status'];

        $this->load->model('Model_pathology_prescription');
        $data_category = array(
            'status'=>$status,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_pathology_prescription->update_pathology_delivered_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }
    public function pathology_prescription_payment_status()
    {  
        $id = $_POST['payment_id'];
        $payment_type= $_POST['payment_type'];

        $this->load->model('Model_pathology_prescription');
        $data_category = array(
            'payment_type'=>$payment_type,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_pathology_prescription->update_pathology_payment_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }
    public function open_pathology_prescription_invoice()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_pathology_prescription'); 
        $data['user_details'] = $this->Model_pathology_prescription->get_prescription_invoice_details($user_id);
         $data['invoice_count'] = $this->Model_pathology_prescription->get_invoice_count();
        $data['medicine_details'] = $this->Model_pathology_prescription->get_prescription_medicine_details($user_id);
        $this->load->view('pathology/view_pathology_prescription_invoice',$data);
    }
    public function print_prescription_invoice_pdf()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_pathology_prescription'); 
        $data['user_details'] = $this->Model_pathology_prescription->get_prescription_invoice_details($user_id);
        $data['invoice_count'] = $this->Model_pathology_prescription->get_invoice_count();
        $data['medicine_details'] = $this->Model_pathology_prescription->get_prescription_medicine_details($user_id);
        $this->load->view('pathology/print_prescription_invoice',$data);
    }
    public function add_pathology_medicine_price()
    {  
        $id = $_POST['order_id'];
        $pricing=$_POST['price'];
        $medicine_name =$_POST['medicine_name'];
        $medicine_price =$_POST['medicine_price'];
        $qty =$_POST['qty'];
        
        $this->load->model('Model_pathology_prescription');
        $data_category = array(
            'pricing'=>$pricing,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_pathology_prescription->update_pathology_total_price($data_category,$id);
        
        $medicine_name = explode(',',$medicine_name);
        $medicine_price = explode(',',$medicine_price);
        $qty = explode(',',$qty);
        
        if ($data_id) {
            for ($i=0; $i <count($medicine_name) ; $i++) {
                $data_size = array(
                    'prescription_id'=>$data_id,
                    'medicine_name'=>$medicine_name[$i],
                    'medicine_price'=>$medicine_price[$i],
                    'qty'=>$qty[$i],
                    'created_date_time'=>$this->current_date_time,
                );

                $this->load->model('Model_pathology_medicine_data');   
                $this->Model_pathology_medicine_data->insert_pathology_medicine_data($data_size);
            }
             echo "Valid";
         } 
     
    }

    public function show_pathology_user_detail_modal()
    {
        $id = $_POST['id'];
        $this->load->model('Model_pathology_order');
        $data['user'] = $this->Model_pathology_order->get_pathology_user_details($id);
        $this->load->view('pathology/modal_pathology_user_details',$data);

    }
    public function update_pathology_report_modal()
    { 
      $id = $_POST['id'];
      $this->load->model('Model_pathology_order'); 
      $data['pdf'] = $this->Model_pathology_order->get_update_pathology_report_details($id);
      $this->load->view('pathology/modal_update_pathology_report',$data);
    }
    public function update_pathology_report_status()
    {  
        $id = $_POST['report_id'];
        $upload_report='';
        if(isset($_FILES['file']))
        {
            $info=pathinfo($_FILES['file']['name']);            
            if($info!='')
            {
                $time = microtime();
                $ext=$info['extension'];
                $upload_report=$_FILES['file']['name'];
                $upload_report = $time.$upload_report;
                $target='files/upload_radiology_report/'.$upload_report;
                move_uploaded_file($_FILES['file']['tmp_name'],$target);
            }
        }
        
        $this->load->model('Model_pathology_order');
        $data_category = array(
            'upload_report'=>$upload_report,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_pathology_order->update_pathology_report_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function check_out_details()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_pathology_order'); 
        $data['check_out'] = $this->Model_pathology_order->get_pathology_check_out_details($user_id);
        $data['get_check_out'] = $this->Model_pathology_order->get_pathology_all_check_out_details($user_id);
        $this->load->view('pathology/check_out',$data);
    }
    public function update_pathology_total_amount()
    {  
        $id = $_POST['order_id'];
        $gst_amount = $_POST['gst_amount'];
        $total_amount = $_POST['total_amount'];
        
        $this->load->model('Model_pathology_order');
        $data_category = array(
            'gst_amount'=>$gst_amount,
            'total_amount'=>$total_amount,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_pathology_order->update_pathology_total_price($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
    public function open_invoice()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_pathology_order'); 
        $data['user_details'] = $this->Model_pathology_order->get_pathology_invoice_details($user_id);
        $data['invoice_count'] = $this->Model_pathology_order->get_p_invoice_count();
        $this->load->view('pathology/view_invoice',$data);
    }
    public function print_invoice_pdf()
    {
        $user_id = $_GET['user_id'];
        $this->load->model('Model_pathology_order'); 
        $data['user_details'] = $this->Model_pathology_order->get_pathology_invoice_details($user_id);
        $data['invoice_count'] = $this->Model_pathology_order->get_p_invoice_count();
        $this->load->view('pathology/invoice',$data);
    }
    public function view_pathology_delivered_order()
    {  
        $this->load->model('Model_pathology_order'); 
        $data['deliver'] = $this->Model_pathology_order->get_pathology_delivered_order();
        $this->load->model('Model_pathology_prescription');
        $data['pathology'] = $this->Model_pathology_prescription->get_all_pathology_prescription_delivered_details();
        $this->load->view('pathology/view_all_delivered_order_details',$data);
    }
    public function view_pathology_cancelled_order()
    {
        $this->load->model('Model_pathology_order'); 
        $data['deliver'] = $this->Model_pathology_order->get_pathology_cancelled_order();
         $this->load->model('Model_pathology_prescription');
        $data['pathology'] = $this->Model_pathology_prescription->get_all_pathology_prescription_cancelled_details();
        $this->load->view('pathology/view_all_cancelled_order_details',$data);
    }
    public function pathology_order_status()
    {
        $id = $_GET['id'];
        $this->load->model('Model_pathology_order'); 
        $data['deliver'] = $this->Model_pathology_order->get_pathology_order_delivered_status_details($id);
        $data['payment'] = $this->Model_pathology_order->get_pathology_order_payment_status_details($id);
        $this->load->view('pathology/pathology_order_status',$data);
    }
    public function pathology_order_deliver_status()
    {  
        $id = $_POST['status_id'];
        $delivery_status= $_POST['delivery_status'];

        $this->load->model('Model_pathology_order');
        $data_category = array(
            'delivery_status'=>$delivery_status,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_pathology_order->update_pathology_order_delivered_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }
    public function pathology_order_payment_status()
    {  
        $id = $_POST['payment_id'];
        $status= $_POST['payment_status'];

        $this->load->model('Model_pathology_order');
        $data_category = array(
            'status'=>$status,
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_pathology_order->update_pathology_order_payment_status($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }

    }


}
