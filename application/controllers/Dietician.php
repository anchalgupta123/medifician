<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dietician extends CI_Controller {

	public $current_date_time;
	public $login_id;
	public $login_role;

	public function __construct()
	{
		parent::__construct();
		$this->login_id = $this->session->userdata('login_id');
		$this->login_role = $this->session->userdata('login_role');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("Asia/Kolkata");
		}
		if (!$this->login_id) {
			redirect('Login');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}
	public function view_dietician_user_details()
	{   $this->load->model('Model_dietician_user_profile');
		$data['user'] = $this->Model_dietician_user_profile->get_all_dietician_user_profile();
		$this->load->view('dietician/view_dietician_user',$data);	
	}
    public function view_dietician_order_details()
	{   
		$id = $this->login_id;
	    $this->load->model('Model_dietician_order_master');
		$data['order'] = $this->Model_dietician_order_master->get_all_dietician_order_details(); 
		$this->load->view('dietician/view_dietician_order',$data);	
	}
	public function update_diet_problem_modal()
	{   
		$id = $_POST['id'];
		$this->load->model('Model_vendor');
		$data['dietician'] = $this->Model_vendor->get_all_dietician_details();
		$this->load->model('Model_dietician_order_master');
		$data['order'] = $this->Model_dietician_order_master->get_all_diet_details($id);
		$this->load->view('dietician/model_update_diet_problem',$data);
	} 
	public function update_dietician_name_form()
    {
        $id = $_POST['order_id'];
        $dietician_id = $_POST['dietician_id'];
        $this->load->model('Model_dietician_order_master');

        $data = array(
            'dietician_id'=>$dietician_id, 
            'created_date_time'=>$this->current_date_time,
        );

        $data_id = $this->Model_dietician_order_master->update_dietician_name($data,$id);
        if ($data_id) {
            echo "Valid";
        }
    } 
    public function view_single_dietician_order_details()
	{   
		$id = $this->login_id;
	    $this->load->model('Model_dietician_order_master');
		$data['order'] = $this->Model_dietician_order_master->get_single_dietician_order_details(); 
		$this->load->view('dietician/view_single_dietician_order',$data);	
	} 
	public function show_user_detail_modal()
	{
		$id = $_POST['id'];
        $this->load->model('Model_dietician_order_master');
        $data['user'] = $this->Model_dietician_order_master->get_dietician_user_details($id);
		$this->load->view('dietician/modal_show_user_details',$data);
	}
    public function upload_diet_chart_modal()
    { 
      $id = $_POST['id'];
      $this->load->model('Model_dietician_order_master'); 
      $data['diet'] = $this->Model_dietician_order_master->get_update_diet_chart_details($id);
      /*echo "<pre>";
        print_r( $data['diet']);
        return;*/
     $this->load->view('dietician/modal_upload_diet_chart',$data);
    }
    public function update_diet_chart_details()
    {  
        $id = $_POST['diet_id'];
        $upload_diet_chart='';
        if(isset($_FILES['file']))
        {
            $info=pathinfo($_FILES['file']['name']);            
            if($info!='')
            {
                $time = microtime();
                $ext=$info['extension'];
                $upload_diet_chart=$_FILES['file']['name'];
                $upload_diet_chart = $time.$upload_diet_chart;
                $target='files/diet_chart/'.$upload_diet_chart;
                move_uploaded_file($_FILES['file']['tmp_name'],$target);
            }
        }
        
        $this->load->model('Model_dietician_order_master');
        $data_category = array(
            'upload_diet_chart'=>$upload_diet_chart,
            'created_date_time'=>$this->current_date_time,
        );
        $data_id = $this->Model_dietician_order_master->update_diet_chart($data_category,$id);
        if ($data_id) {
            echo "Valid";
        }
    }
}
