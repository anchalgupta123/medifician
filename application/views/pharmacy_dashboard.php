<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('bars/head');?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />	
</head>

<body>
	<div class="wrapper">
		<?php $this->load->view('bars/header');?>

		<!-- Sidebar -->
		<?php $this->load->view('bars/side_bar');?>
		<!-- End Sidebar -->
    
		<div class="main-panel">
			<div class="content">
		       	<div class="page-inner">
					<div class="row">
						<h3 class="title">Pharmacy</h3>
					<div class="row">
						<div class="col-sm-6 col-md-3">
							<div class="card card-stats card-primary card-round">
								<div class="card-body">
									<div class="row">
										<div class="col-5">
											<div class="icon-big text-center">
												<i class="flaticon-users"></i>
											</div>
										</div>
										<div class="col-7 col-stats">
											<div class="numbers">
												<p class="card-category">Total Orders</p>
												<h4 class="card-title"><?php echo $pharmacy_count; ?></h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="card card-stats card-info card-round">
								<div class="card-body">
									<div class="row">
										<div class="col-5">
											<div class="icon-big text-center">
												<i class="flaticon-interface-6"></i>
											</div>
										</div>
										<div class="col-7 col-stats">
											<div class="numbers">
												<p class="card-category">Pending Orders</p>
												<h4 class="card-title"><?php echo $pharmacy_pending_count; ?></h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="card card-stats card-success card-round">
								<div class="card-body ">
									<div class="row">
										<div class="col-5">
											<div class="icon-big text-center">
												<i class="flaticon-analytics"></i>
											</div>
										</div>
										<div class="col-7 col-stats">
											<div class="numbers">
												<p class="card-category">Confirm Orders</p>
												<h4 class="card-title"><?php echo $pharmacy_confirm_count; ?></h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="card card-stats card-danger card-round">
								<div class="card-body ">
									<div class="row">
										<div class="col-5">
											<div class="icon-big text-center">
												<i class="flaticon-success"></i>
											</div>
										</div>
										<div class="col-7 col-stats">
											<div class="numbers">
												<p class="card-category">Delivered Orders</p>
												<h4 class="card-title"><?php echo $pharmacy_delivered_count; ?></h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="card card-stats card-secondary card-round">
								<div class="card-body ">
									<div class="row">
										<div class="col-5">
											<div class="icon-big text-center">
												<i class="flaticon-success"></i>
											</div>
										</div>
										<div class="col-7 col-stats">
											<div class="numbers">
												<p class="card-category">Today Sell</p>
												<h4 class="card-title"><?php echo floatval($pharmacy_today_sale->price); ?></h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="card card-stats card-warning card-round">
								<div class="card-body ">
									<div class="row">
										<div class="col-5">
											<div class="icon-big text-center">
												<i class="flaticon-success"></i>
											</div>
										</div>
										<div class="col-7 col-stats">
											<div class="numbers">
												<p class="card-category">Monthly Sell</p>
												<h4 class="card-title"><?php echo floatval($pharmacy_monthly_sale->price); ?></h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-3">
							<div class="card card-stats card-primary card-round">
								<div class="card-body ">
									<div class="row">
										<div class="col-5">
											<div class="icon-big text-center">
												<i class="flaticon-success"></i>
											</div>
										</div>
										<div class="col-7 col-stats">
											<div class="numbers">
												<p class="card-category">Yearly sell</p>
												<h4 class="card-title"><?php echo floatval($pharmacy_yearly_sale->price); ?></h4>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</div>	
			  </div>
		</div>
		
		<?php $this->load->view('bars/footer')?>
		<?php $this->load->view('bars/js')?>
	</div>
</div>
	<!--   Core JS Files   -->
   
</body>
</html>