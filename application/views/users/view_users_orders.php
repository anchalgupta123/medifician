<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
	<div class="wrapper">
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Order Details</h4>
								</div>
								<div class="card-body">
									<ul class="nav nav-pills nav-secondary nav-pills-no-bd" id="pills-tab-without-border" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" id="pills-home-tab-nobd" data-toggle="pill" href="#pills-pharmacy-nobd" role="tab" aria-controls="pills-pharmacy-nobd" aria-selected="true"> Pharmacy Order</a>
										</li>

										<li class="nav-item">
											<a class="nav-link " id="pills-pathology-tab-nobd" data-toggle="pill" href="#pills-pathology-nobd" role="tab" aria-controls="pills-pathology-nobd" aria-selected="false"> Pathology Order</a>
										</li>

										<li class="nav-item">
											<a class="nav-link" id="pills-pathology-prescription-tab-nobd" data-toggle="pill" href="#pills-pathology-prescription-nobd" role="tab" aria-controls="pills-pathology-prescription-nobd" aria-selected="false"> Pathology Prescription Order</a>
										</li>

										<li class="nav-item">
											<a class="nav-link" id="pills-radiology-tab-nobd" data-toggle="pill" href="#pills-radiology-nobd" role="tab" aria-controls="pills-radiology-nobd" aria-selected="false"> Radiology Order</a>
										</li>

										<li class="nav-item">
											<a class="nav-link" id="pills-radiology_prescription-tab-nobd" data-toggle="pill" href="#pills-radiology_prescription-nobd" role="tab" aria-controls="pills-radiology_prescription-nobd" aria-selected="false"> Radiology Prescription Order</a>
										</li>

										<li class="nav-item">
											<a class="nav-link" id="pills-physiotherapy_order-tab-nobd" data-toggle="pill" href="#pills-physiotherapy_order-nobd" role="tab" aria-controls="pills-physiotherapy_order-nobd" aria-selected="false">Physiotherapy Order</a>
										</li>

										<li class="nav-item">
											<a class="nav-link" id="pills-physiotherapy_prescription-tab-nobd" data-toggle="pill" href="#pills-physiotherapy_prescription-nobd" role="tab" aria-controls="pills-physiotherapy_prescription-nobd" aria-selected="false">Physiotherapy Prescription Order</a>
										</li>

										<li class="nav-item">
											<a class="nav-link" id="pills-nursing-tab-nobd" data-toggle="pill" href="#pills-nursing-nobd" role="tab" aria-controls="pills-nursing-nobd" aria-selected="false">Nursing Order</a>
										</li>

										<li class="nav-item">
											<a class="nav-link" id="pills-dietician_order-tab-nobd" data-toggle="pill" href="#pills-dietician_order-nobd" role="tab" aria-controls="pills-dietician_order-nobd" aria-selected="false">Dietician Order</a>
										</li>
										
									</ul>
									<div class="tab-content mt-2 mb-3" id="pills-without-border-tabContent">
										<div class="tab-pane fade show active" id="pills-pharmacy-nobd" role="tabpanel" aria-labelledby="pills-home-tab-nobd">
											 <div class="table-responsive">
												<table id="multi-filter-select" class="display table table-striped table-hover">
													<thead>
														<tr>
															<th>Sr.no</th>
															<th>Name</th>
															<th>Mobile_no</th>
															<th>Problem</th>
															<th>Problem Image</th>
															<th>Total Amount</th>
															<th>Date</th>
															
														</tr>
													</thead>
												    <tbody>
														<?php $x = 1; foreach ($pharmacy_order as $key){ ?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td><?php echo $key->name; ?></td>
									                        <td><?php echo $key->mobile_no; ?></td>
									                        <td><?php echo $key->problem; ?></td>
									                        <td><?php echo $key->image; ?></td>
									                        <td><?php echo $key->total_amount; ?></td>
									                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
									                        
														</tr>
													    <?php } ?>
													</tbody> 
												</table>
											</div>
										</div>
										
                                        <div class="tab-pane fade" id="pills-pathology-nobd" role="tabpanel" aria-labelledby="pills-pathology-tab-nobd">
											<div class="table-responsive">
												<table id="multi-filter-select2" class="display table table-striped table-hover">
													<thead>
														<tr>
															<th>Sr.no</th>
															<th>Name</th>
															<th>Mobile_no</th>
															<th>Clinicle History</th>
															<th>Report</th>
															<th>Total Amount</th>
															<th>Date</th>
															
														</tr>
													</thead>
												    <tbody>
														<?php $x = 1; foreach ($pathology_order as $key){ ?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td><?php echo $key->name; ?></td>
									                        <td><?php echo $key->mobile_no; ?></td>
									                        <td><?php echo $key->clinical_history; ?></td>
									                        <td><?php echo $key->upload_report; ?></td>
									                        <td><?php echo $key->total_amount; ?></td>
									                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
									                        
														</tr>
													    <?php } ?>
													</tbody> 
												</table>
											</div>
										</div>
                                        
										<div class="tab-pane fade" id="pills-pathology-prescription-nobd" role="tabpanel" aria-labelledby="pills-pathology-prescription-tab-nobd">
											 <div class="table-responsive">
												<table id="multi-filter-select-3" class="display table table-striped table-hover">
													<thead>
														<tr>
															<th>Sr.no</th>
															<th>Name</th>
															<th>Mobile_no</th>
															<th>Problem</th>
															<th>Problem Image</th>
															<th>Total Amount</th>
															<th>Date</th>
															
														</tr>
													</thead>
												    <tbody>
														<?php $x = 1; foreach ($pathology_prescription as $key){ ?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td><?php echo $key->name; ?></td>
									                        <td><?php echo $key->mobile_no; ?></td>
									                        <td><?php echo $key->problem; ?></td>
									                        <td><?php echo $key->image; ?></td>
									                        <td><?php echo $key->total_amount; ?></td>
									                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
									                        
														</tr>
													    <?php } ?>
													</tbody> 
												</table>
											</div>
										</div>

										<div class="tab-pane fade" id="pills-radiology-nobd" role="tabpanel" aria-labelledby="pills-radiology-tab-nobd">
											<div class="table-responsive">
												<table id="multi-filter-select4" class="display table table-striped table-hover">
													<thead>
														<tr>
															<th>Sr.no</th>
															<th>Name</th>
															<th>Mobile_no</th>
															<th>Clinicle History</th>
															<th>Report</th>
															<th>Total Amount</th>
															<th>Date</th>
															
														</tr>
													</thead>
												    <tbody>
														<?php $x = 1; foreach ($radiology_order as $key){ ?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td><?php echo $key->name; ?></td>
									                        <td><?php echo $key->mobile_no; ?></td>
									                        <td><?php echo $key->clinical_history; ?></td>
									                        <td><?php echo $key->upload_report; ?></td>
									                        <td><?php echo $key->total_amount; ?></td>
									                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
									                        
														</tr>
													    <?php } ?>
													</tbody> 
												</table>
											</div>
										</div>
                                        
                                        <div class="tab-pane fade" id="pills-radiology_prescription-nobd" role="tabpanel" aria-labelledby="pills-radiology_prescription-tab-nobd">
											 <div class="table-responsive">
												<table id="multi-filter-select-5" class="display table table-striped table-hover">
													<thead>
														<tr>
															<th>Sr.no</th>
															<th>Name</th>
															<th>Mobile_no</th>
															<th>Problem</th>
															<th>Problem Image</th>
															<th>Total Amount</th>
															<th>Date</th>
															
														</tr>
													</thead>
												    <tbody>
														<?php $x = 1; foreach ($radiology_prescription as $key){ ?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td><?php echo $key->name; ?></td>
									                        <td><?php echo $key->mobile_no; ?></td>
									                        <td><?php echo $key->problem; ?></td>
									                        <td><?php echo $key->prescription_image; ?></td>
									                        <td><?php echo $key->total_amount; ?></td>
									                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
									                        
														</tr>
													    <?php } ?>
													</tbody> 
												</table>
											</div>
										</div>

										<div class="tab-pane fade" id="pills-physiotherapy_order-nobd" role="tabpanel" aria-labelledby="pills-physiotherapy_order-tab-nobd">
											<div class="table-responsive">
												<table id="multi-filter-select6" class="display table table-striped table-hover">
													<thead>
														<tr>
															<th>Sr.no</th>
															<th>Name</th>
															<th>Mobile_no</th>
															<th>Clinicle History</th>
															<th>Report</th>
															<th>Total Amount</th>
															<th>Date</th>
															
														</tr>
													</thead>
												    <tbody>
														<?php $x = 1; foreach ($physiotherapy_order as $key){ ?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td><?php echo $key->name; ?></td>
									                        <td><?php echo $key->mobile_no; ?></td>
									                        <td><?php echo $key->clinical_history; ?></td>
									                        <td><?php echo $key->upload_report; ?></td>
									                        <td><?php echo $key->total_amount; ?></td>
									                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
									                        
														</tr>
													    <?php } ?>
													</tbody> 
												</table>
											</div>
										</div>

                                        <div class="tab-pane fade" id="pills-physiotherapy_prescription-nobd" role="tabpanel" aria-labelledby="pills-physiotherapy_prescription-tab-nobd">
											 <div class="table-responsive">
												<table id="multi-filter-select-7" class="display table table-striped table-hover">
													<thead>
														<tr>
															<th>Sr.no</th>
															<th>Name</th>
															<th>Mobile_no</th>
															<th>Problem</th>
															<th>Problem Image</th>
															<th>Total Amount</th>
															<th>Date</th>
															
														</tr>
													</thead>
												    <tbody>
														<?php $x = 1; foreach ($physiotherapy_prescription as $key){ ?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td><?php echo $key->name; ?></td>
									                        <td><?php echo $key->mobile_no; ?></td>
									                        <td><?php echo $key->problem; ?></td>
									                        <td><?php echo $key->prescription_image; ?></td>
									                        <td><?php echo $key->total_amount; ?></td>
									                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
									                        
														</tr>
													    <?php } ?>
													</tbody> 
												</table>
											</div>
										</div>

										<div class="tab-pane fade" id="pills-nursing-nobd" role="tabpanel" aria-labelledby="pills-nursing-tab-nobd">
											<div class="table-responsive">
												<table id="multi-filter-select8" class="display table table-striped table-hover">
													<thead>
														<tr>
															<th>Sr.no</th>
															<th>Name</th>
															<th>Mobile No</th>
															<th>Time</th>
															<th>Price</th>
															<th>Clinical History</th>
															<th>Date</th>
														</tr>
													</thead>
												    <tbody>
														<?php $x = 1; foreach ($nursing_order as $key){ ?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td><?php echo $key->name; ?></td>
															<td><?php echo $key->mobile_no; ?></td>
									                        <td><?php echo $key->time; ?></td>
									                        <td><?php echo $key->price; ?></td>
									                        <td><?php echo $key->clinical_history; ?></td>
									                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
									                        
														</tr>
													    <?php } ?>
													</tbody> 
												</table>
											</div>
										</div>

										<div class="tab-pane fade" id="pills-dietician_order-nobd" role="tabpanel" aria-labelledby="pills-dietician_order-tab-nobd">
											<div class="table-responsive">
												<table id="multi-filter-select9" class="display table table-striped table-hover">
													<thead>
														<tr>
															<th>Sr.no</th>
															<th>Name</th>
															<th>Mobile No</th>
															<th>Clinicle History</th>
															<th>Problem</th>
															<th>Total Amount</th>
															<th>Uploaded Diet Chart</th>
															<th>Date</th>
															
														</tr>
													</thead>
												    <tbody>
														<?php $x = 1; foreach ($dietician_order as $key){ ?>
														<tr>
															<td><?php echo $x++; ?></td>
															<td><?php echo $key->name; ?></td>
															<td><?php echo $key->mobile_no; ?></td>
									                        <td><?php echo $key->clinical_history; ?></td>
									                        <td><?php echo $key->problem; ?></td>
									                        <td><?php echo $key->amount; ?></td>
									                        <td><?php echo $key->upload_diet_chart; ?></td>
									                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
					                
														</tr>
													    <?php } ?>
													</tbody> 
												</table>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
		

					</div>
				</div>
			</div>
			<?php $this->load->view('bars/footer');?>
			<?php $this->load->view('bars/js');?>

    <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
		$(document).ready(function() {
			$('#multi-filter-select2').DataTable({
			});
			
		});
		$(document).ready(function() {
			$('#multi-filter-select3').DataTable({
			});
			
		});
		$(document).ready(function() {
			$('#multi-filter-select4').DataTable({
			});
			
		});
		$(document).ready(function() {
			$('#multi-filter-select5').DataTable({
			});
			
		});
		$(document).ready(function() {
			$('#multi-filter-select6').DataTable({
			});
			
		});
		$(document).ready(function() {
			$('#multi-filter-select7').DataTable({
			});
			
		});
		$(document).ready(function() {
			$('#multi-filter-select8').DataTable({
			});
			
		});
		$(document).ready(function() {
			$('#multi-filter-select9').DataTable({
			});
			
		});
	</script>
		</div>
	</div>
</body>
</html>