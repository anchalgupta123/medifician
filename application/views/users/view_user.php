<!DOCTYPE html>
<html>
<head>
	
	<?php $this->load->view('bars/head');?>
</head>
<body>
	<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">View User Details </h4>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>Action</th>
										<th>Name</th>
										<th>Date_of_Birth</th>
										<th>Email</th>
										<th>Mobile_no</th>
										<th>Address</th>
										<th>City</th>
										<th>State</th>
										<th>Pincode</th>
										<th>Refer Code</th>
										<th>Refere Code</th>
										<th>Register Date</th>
										
									</tr>
								</thead>
							    <tbody>
									<?php $x = 1; foreach ($users as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
										<td><a style="font-size: 15px;font-weight: bold;" class="btn btn-primary btn-xs" href="<?php echo base_url()?>Users/view_users_orders?user_id=<?php echo $key->id;?>">view order</a></td>
				                        <td><?php echo $key->name; ?></td>
				                        <td><?php echo change_date_format_dmy($key->date_of_birth); ?></td>
				                        <td><?php echo $key->email; ?></td>
				                        <td><?php echo $key->mobile_no; ?></td>
				                        <td><?php echo $key->address; ?></td>
				                        <td><?php echo $key->city; ?></td>
				                        <td><?php echo $key->state; ?></td>
				                        <td><?php echo $key->pincode; ?></td>
				                        <td><?php echo $key->refer_code; ?></td>
				                        <td><?php echo $key->referer_code; ?></td>
				                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
				                        
									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
</div>
</div>
    <?php $this->load->view('bars/js');?>
    <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
	</script>
</body>
</html>