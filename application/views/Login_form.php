<!DOCTYPE html>
<html lang="en">
  <?php $this->load->view('bars/head');?>
  <style type="text/css">
    .card{
      margin-top: 100px;
      margin-left: 30%;
      margin-right: 30%;
      padding-left: 40px;
      padding-right: 40px;
      padding-top: 40px;

    }
    .card input{
      height: 40px;
    }
  </style>
  <body class="login">
    <div>
      <div class="login_wrapper">
        <div class="col-md-12">
        <div class="card">
        <div class="animate form login_form">
          <section class="login_content">
            <form id="login_form">
              <h1 class="text-center">Login Form</h1>
              <div class="form-group">
                <input type="text" id="email" class="form-control" placeholder="Email" required="" />
              </div>
              <div class="form-group">
                <input type="password" id="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div class="form-group">
                <button class="btn btn-default submit" type="submit">Log in</button>
              </div>

              <hr>

              <div class="separator">
                <div>
                  <h1 class="text-center"> Medificians </h1>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
     </div>
  </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/common.js"></script>

    <script type="text/javascript">
      $( "#login_form" ).submit(function( event ) {
        login();
        event.preventDefault();
      });
    </script>
  </body>
</html>
