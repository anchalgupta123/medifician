<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">View Prescription Details</h4>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover" >
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>Name</th>
										<th>Address</th>
										<th>Image</th>
										<th>Problem</th>
										<th>Pricing</th>
										<th>Delivery Status</th>
										<th>Payment Type</th>
										<th>Register Date</th>
										<th>Action</th>
									</tr>
								</thead>
							    <tbody>
									<?php $x = 1; foreach ($pathology as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
				                         <td><a href="javascript:void(0);" style="color:#575962;" onclick="show_pathology_prescription_user_detail_modal('<?php echo $key->id;?>');"><?php echo $key->name;?></a></td>
				                        <td><?php echo $key->address; ?></td>
				                        <td><a href="" download><img src="<?php echo base_url();?>files/prescription_img/<?php echo $key->image;?>" width="70" height="70" class="downloadable"/></a>
				                        </td>
				                        <td><?php echo $key->problem; ?></td> 
				                        <td><?php echo $key->pricing; ?></td>  	 
				                        <td><?php
				                           if($key->status==0)
				                           {
				                            echo "New Order";
				                           }
				                           if($key->status==1)
				                           {
				                            echo "Pending Order";
				                           }
				                           if($key->status==2)
				                           {
				                            echo "Confirm Order";
				                           }
				                           if($key->status==3)
				                           {
				                            echo "Delivered Order";
				                           }
				                           if($key->status==4)
				                           {
				                            echo "Cancelled Order";
				                           }
				                          ?> 
				                        </td>
				                        <td><?php
				                           if($key->payment_type==0)
				                           {
				                            echo "Cash on Delivery";
				                           }
				                           if($key->payment_type==1)
				                           {
				                            echo "Payment Gateway";
				                           }
				                           if($key->payment_type==2)
				                           {
				                            echo "Paid";
				                           }
				                          ?> 
				                        </td>
				                        <td><?php echo change_date_format_dmy($key->created_date_time);?></td>
				                        <td><a href="<?php echo base_url();?>Pathology/pathology_prescription_check_out_details?user_id=<?php echo $key->id;?>"><button type="button" style="font-weight: bold;margin-top: 5PX;" class="btn btn-primary btn-sm">CHECK OUT</button></a>
				                        <a href="<?php echo base_url();?>Pathology/pathology_prescription_status?id=<?php echo $key->id;?>"><button type="button" style="font-weight: bold;margin-top: 5PX;" class="btn btn-primary btn-sm">UPDATE STATUS</button></a></td>
									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
         <?php $this->load->view('bars/js');?>
</div>
</div>
<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
</div>
    
    <script >
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
		});
	</script>
	 <script type="text/javascript">
  	 function show_pathology_prescription_user_detail_modal(id) {
        $.ajax({
          url: base_url+"Pathology/show_pathology_prescription_user_detail_modal", 
          type : "POST",
          data: {id : id},
          success: function(result)
          {
            console.log(result);
              $('#modal_details').html(result);
              $('#modal_details').modal('show');
          }
        });
      }
</script>
	<script type="text/javascript">
	$(document).ready(function(){
    $('img.downloadable').each(function(){
        var $this = $(this);
        $this.wrap('<a href="' + $this.attr('src') + '" download />')
      });
  });
	function apply_pathology_price(id,price)
    {
    	update_price=price.value;
    $.ajax({
        url: base_url + "Pathology/apply_pathology_prescription_price", 
        type : "POST",
        data: {id:id, update_price:update_price}, 
        success: function(result)
        {
            if (result == 'Valid') 
            {
            	// alert('this is alert');
                //location.reload();
            }
        }
    });
}
function reload_page_pathology()
{
	location.reload();
}
  </script>
</body>
</html>