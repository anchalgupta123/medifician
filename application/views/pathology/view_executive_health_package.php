<!DOCTYPE html>
<html>
<head>
	
	<?php $this->load->view('bars/head');?>
</head>
<body>
	<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
		    	
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h2 class="card-title">Executive Health Package</h2>
							<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal" onclick="add_executive_health_pkg_modal();"><i class="fa fa-plus"></i> Add Executive Health Package</button>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>Test Name</th>
										<th>Lab Name</th>
										<th>Description</th>
										<th>Price</th>
										<th>Offer Price</th>
										<th>Off(in %)</th>
										<th>Register Date</th>
									</tr>
								</thead>
							    <tbody>
									 <?php $x = 1; foreach ($test as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
				                        <td><?php echo $key->package_name; ?></td>
				                        <td class="type_box">
											<?php if ($key->lab_name) {
											$lab_name =explode(',',$key->lab_name);
											foreach ($lab_name as $ty => $value) {
												echo $value ."<hr>";
											} }?>
										</td>
				                        <td class="description"><?php echo $key->description; ?></td>
				                        <td class="price_box">
											<?php if ($key->price) {
											$price =explode(',',$key->price);
											foreach ($price as $ty => $value) {
												echo $value ."<hr>";
											} }?>
										</td>
										<td class="price_box">
											<?php if ($key->offer_price) {
											$offer_price =explode(',',$key->offer_price);
											foreach ($offer_price as $ty => $value) {
												echo $value ."<hr>";
											} }?>
										</td>
										<td class="price_box">
											<?php if ($key->off) {
											$off =explode(',',$key->off);
											foreach ($off as $ty => $value) {
												echo $value ."<hr>";
											} }?>
										</td>
				                        
				                        <td><?php echo change_date_format_dmy($key->created_date_time);?></td>
				                        <td><a href="<?php echo base_url()?>Pathology/edit_executive_health_price_test_data?id=<?php echo $key->id;?>"><button type="button" class="btn btn-warning btn-sm">Edit</button></a> <button style="margin-top: 4px;margin-bottom: 4px;" type="button" class="btn btn-danger btn-sm" onclick="delete_executive_health_package('<?php echo $key->id;?>')">Delete</button></td>
									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
           <?php $this->load->view('bars/js');?>
</div>
</div>
	<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
    </div>
    <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
	</script>
	<script type="text/javascript">
		function add_executive_health_pkg_modal() {
        $.ajax({
          url: base_url+"Pathology/add_executive_health_pkg_modal", 
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
</body>
</html>