<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('bars/head');?>
	<style type="text/css">
		.type_box{
			width: 100px;
			/*text-align: center;*/
		}
		.type_box hr{
			margin: 5px 0px 5px 0px;
			border-color: #80808070!important;
		}
		.type_box hr:last-child{
			display: none;
		}
		.description{
			width: 100px;
		}
		.price_box{
			/*text-align: center;*/
			width: 50px;
		}
		.price_box hr{
          margin: 5px 0px 5px 0px;
          border-color: #80808070!important;
		}
		.price_box hr:last-child{
			display: none;
		}
	</style>
</head>
<body>
	<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">Edit Test Details</h4>
						</div>
					</div>
					<div class="card-body">
						<div class="col-md-6">
							<label>Package Name:</label>
							<div class="form-group form-group-default">
								<input id="package_name" type="text" class="form-control" placeholder="Test Name" value="<?php echo $test_data->package_name;?>">
							</div>
					   </div>
					   <div class="col-md-6">
							<label>Description:</label>
							<div class="form-group form-group-default">
								<input id="description" type="text" class="form-control" placeholder="Description" value="<?php echo $test_data->description;?>">
							</div>
							<input type="hidden" id="package_id" value="<?php echo $test_data->id;?>">
						</div>
						<div class="col-md-6">
								<label>Test Category:</label>
								<div class="form-group form-group-default">
									<select class="form-control" id="category_name">
										<option><?php echo $test_data->category;?></option>
										<option>Liver</option>
										<option>Heart</option>
										<option>Thyroid</option>
										<option>Diabetes</option>
										<option>Vitamin D</option>
										<option>Kidney</option>
									</select>
							    </div>
							</div>
						<!-- <div class="col-md-6">
							<label>Category:</label>
							<div class="form-group form-group-default">
								<input id="category" type="text" class="form-control" placeholder="Category" value="">
							</div>
							<input type="hidden" id="test_id" value="<?php echo $test_data->id;?>">
						</div> -->
						<button type="button" class="btn btn-info btn-sm" onclick="update_health_test_details();">Update Details</button>
					</div>
		                <div class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>Sr.No.</th>
                              <th>Lab Name</th>
                              <th>Price</th>
                              <th>Offer Price</th>
                              <th>Percentage</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $x = 1; foreach ($pathology_test_data as $key){ ?>
                            <tr>
                              <td><?php echo $x++; ?></td>
                              <td><?php echo $key->lab_name; ?></td>
                              <td><?php echo $key->price; ?></td>
                              <td><?php echo $key->offer_price; ?></td>
                              <td><?php echo $key->off; ?></td>
                              <td><button class="btn btn-danger btn-xs" onclick="delete_health_pkg_tests('<?php echo $key->id;?>');"><i class="fa fa-trash"></i></button></td>
                            </tr>
                          <?php } ?> 
                          </tbody>     
                        </table>
                        
                       </div>
                            <div class="row">
		                        <div class="col-sm-12">
		                          <div class="table-responsive">
		                            <table class="table table-bordered table-striped">
		                              <tbody id="price_tbl">
                                       <button class="btn btn-primary btn-sm" type="button" onclick="add_row_price11();"><i class="fa fa-plus"></i> Add Lab Price</button>  
                                     </tbody>
  		                            </table>
		                          </div>
		                        </div>
		                    </div>
		                    <div>
		                    	<button class="btn btn-danger btn-sm offset-5" style="margin-bottom: 40px;" type="button" onclick="add_price_health_test_data();" >Add Price</button>
		                    </div>
					    </div>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
         <?php $this->load->view('bars/js');?>
</div>
</div>
	<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
    </div>
    <script type="text/javascript">
          temp = 1;
          function add_row_price11()
          {
            temp++;
            $('#price_tbl').append('<tr id="price_row'+temp+'"><td><select class="lab_id" style="width: 100%;"><?php foreach($pathology_lab as $key){?><option value="<?php echo $key->id; ?>" ><?php echo $key->lab_name; ?></option><?php }?></select></td><td><input type="text" size="15" name="" style="width: 100%;" id="price_update'+temp+'" class="price" value="" placeholder="Price" onkeyup="off_calculation22('+temp+');"></td><td><input type="text" size="15" name="" id="offer_price_update'+temp+'" style="width: 100%;" class="offer_price" placeholder="Offer Price" onkeyup="off_calculate_from_offer_price2('+temp+');"></td><td><input type="text" size="5" name="" style="width: 100%;" placeholder="Off" id="off_update'+temp+'" class="off"></td><td><button class="btn btn-danger btn-xs" type="button"  onclick="remove_row(&#39;price_row'+temp+'&#39;);"><i class="fa fa-minus"></i></button></td></tr>');
          }
          function remove_row(id)
          {
            $('#'+id).remove();
          }
          
        </script>
    <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
	</script>
	<script>
    function off_calculation()
    { 
      var originalprice = $('#price_update').val();
      $('#offer_price_update').val('');
      $('#off_update').val('0');
    }
    function off_calculation22(temp)
    { 
      var originalprice34 = $('#price_update'+temp).val();
      $('#offer_price_update'+temp).val('');
      $('#off_update'+temp).val('0');
	}
    function off_calculate_from_offer_price2(temp)
    {
    	var originalprice32 = $('#price_update'+temp).val();
      var offerprice2 = $('#offer_price_update'+temp).val();
      
      var off=((originalprice32 - offerprice2)/originalprice32)* 100;

      $('#off_update'+temp).val(Math.round(off));
    }
    function off_calculate_from_offer_price()
    {
      var originalprice32 = $('#price_update').val();
      var offerprice2 = $('#offer_price_update').val();
      
      var off=((originalprice32 - offerprice2)/originalprice32)* 100;

      $('#off_update').val(Math.round(off));
    }
</script> 
</body>
</html>