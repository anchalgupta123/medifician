<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('bars/head');?>
	<style type="text/css">
		table{
			border:1px solid lightgray!important;
		}
	</style>
</head>
<body>
<!-- <div class="wrapper">		
   <div class="main-panel"> -->
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
					   <div class="d-flex align-center">
                         <img style="height: 140px;width: 290px;" class="offset-5" src="<?php echo base_url();?>files/logo.png">
					   </div>
					</div>
					<div class="card-body" style="padding-left: 35px;padding-top: 60px;">
						<div class="row">
							<div class="col-md-9">
								<h5>Date : <?php echo change_date_format_dmy($user_details->created_date_time);?></h5>
								<h5>GSTIN No. :</h5>
							</div>
							<div>
								<h5>Invoice No. :</h5>
							</div>	
						</div>
						<hr style="border-color:lightgray!important;">
						<div class="row">
							<div class="col-md-6">
								<h5>Name :&nbsp;&nbsp; <?php echo $user_details->name;?></h5>
								<h5>Mobile No. :&nbsp;&nbsp; <?php echo $user_details->p_mobile_no;?> </h5>
								<h5>Address :&nbsp;&nbsp; <?php echo $user_details->address;?></h5>
								
							</div>
							<div class="col-md-5" style="padding-left: 28%;">
								<h4>BALANCE DUE <br><small style="padding-left: 45px;">On Receipt</small></h4>
								<p style="font-size: 20px;" class="text-center"><?php echo $user_details->price;?>/-</p>
							</div>
						</div>
                        <div class="row">
                          <div class="table-responsive">
                        	<table id="multi-filter-select" class="display table table-striped table-hover table-bordered">
                        		<thead>
                        			
                        		</thead>
                        		<tbody style="text-align: right;">
                        			<tr>
                        				<th>Sr. No.</th>
                        				<th>Item</th>
                        				<th>Total</th>
                        			</tr>
                        			<tr><?php $x = 1;{ ?>
                        			 	<td><?php echo $x++;?></td>
                        			 	<td><?php echo $user_details->test_name;?></td>
                        			 	<td><?php echo $user_details->price;?></td>
                        			 	 <?php } ?>
                        			 </tr>
                        			<tr>
                                        <th></th>
                        				<th style="font-size:16px;text-align: right;">TOTAL </th>
                                        <th style="font-size: 17px;text-align: right;"><?php echo $user_details->price;?>/- Rs.</th></tr>
                        			</tr>
                        		</tbody>
                        		<tfoot>
                        			
                        		</tfoot>
                        	</table>
                          </div>
                      </div>
	                	<div class="col-md-12" style="margin-top: 30px;">
	                		<h3 style="float: right;">FOR : MEDIFICIANS</h3>
	                	</div>
	                	<div>
	                        <br>
	                		<hr style="border-color:#404450eb!important;">
		                    <h5 style="font-size:16px;">Address : H 302, Swadesh Bhawan, A.B. Road, Indore - 452010</h5>
		                    <h5 style="font-size:16px;">Email : support@medificians.com</h5>
		                    <h5 style="font-size:16px;">Mobile No. :</h5>
	                	</div>
					</div>
				</div>
		    </div>
		 </div>
	  </div>
   <!--  </div>
</div> -->
<?php $this->load->view('bars/js');?>
 <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
	</script>
</body>
</html>