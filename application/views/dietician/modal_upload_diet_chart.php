	<style type="text/css">
		.lab{
			color: black!important;
			font-weight: bold;
			padding-left: 20px;
			font-size: 16px!important;
		}
		.form-control{
		  background-color: #404450eb!important;
		  color: #fff;
          border-color: #404450eb!important;
          padding-bottom: 10px;
          font-size: 16px!important;
		}
	</style>
	<div class="modal-dialog" role="document">
			<div class="modal-content" style="height: 300px;">
				<div class="modal-header">
					<h5 class="modal-title">
						<span class="fw-mediumbold">
						Upload Report</span> 
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form name="myform" id="myform">
						<div class="row">
							<div class="col-md-12">
								
								<div class="form-group" style="padding-bottom: 15px;">
									 <input type="file" id="upload_diet_chart" name="file" required="required" class="form-control" onchange="change_img('upload_diet_chart','view_img');">
									 <input type="hidden"  id="diet_id" value="<?php echo $diet->id; ?>">
								</div>
							</div>
						</div>	
					</form>
				</div>
				<div class="modal-footer no-bd">
					<button type="button" id="addRowButton" class="btn btn-primary" onclick="update_diet_chart_details()" >Update</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>