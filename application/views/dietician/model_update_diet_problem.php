	<style type="text/css">
		.lab{
			color: black!important;
			font-weight: bold;
			padding-left: 20px;
			font-size: 16px!important;
		}
		.form-control{
		  background-color: #404450eb!important;
		  color: #fff;
          border-color: #404450eb!important;
          padding-bottom: 10px;
          font-size: 16px!important;
		}
	</style>
	<div class="modal-dialog" role="document">
			<div class="modal-content" style="height: 300px;">
				<div class="modal-header">
					<h5 class="modal-title">
						<span class="fw-mediumbold">
						Update Dietician</span> 
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="row">
							<div class="col-md-12">
								<label class="lab">Dietician Name:</label>
								<div class="form-group">
								 <select class="form-control" id="dietician_id">
									   <option>Select Dietician</option>
									<?php foreach($dietician as $key){?> 
				                       <option value="<?php echo $key->id; ?>"><?php echo $key->name; ?></option>
				                    <?php }?>
								 </select>
									  <input type="hidden" value="<?php echo $order->id; ?>" id="order_id">
								</div>
							</div>
						</div>		
					</form>
				</div>
				<div class="modal-footer no-bd">
					<button type="button" id="addRowButton" class="btn btn-primary" onclick="update_dietician_name_form();" >Update</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>