<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
<div class="wrapper">
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">View Dietician Order Details</h4>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover" >
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>UserName</th>
										<th>Problem Description</th>
										<th>Amount</th>
										<th>Register Date</th>
										<th>Action</th>
									</tr>
								</thead>
							    <tbody>
									<?php $x = 1; foreach ($order as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
				                        <td><a href="javascript:void(0);" style="color:#575962;" onclick="show_user_detail_modal('<?php echo $key->id; ?>');"><?php echo $key->name; ?></a></td>
				                        <td><?php echo $key->problem;?></td>
				                        <td><?php echo $key->amount;?></td>
				                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
				                        <td><button type="button" class="btn btn-info btn-sm" onclick="upload_diet_chart_modal('<?php echo $key->id;?>');">UPDATE</button></td>
									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
</div>
</div>
    <div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
    </div>
    <?php $this->load->view('bars/js');?>
    <script >
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});

		});
	</script>
	<script type="text/javascript">
		function upload_diet_chart_modal(id) {
        $.ajax({
          url: base_url+"Dietician/upload_diet_chart_modal",
          type : "POST",
          data :{id : id}, 
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
	<script type="text/javascript">
		function show_user_detail_modal(id) {
        $.ajax({
          url: base_url+"Dietician/show_user_detail_modal",
          type : "POST",
          data :{id : id}, 
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
</body>
</html>