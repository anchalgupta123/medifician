	<style type="text/css">
		.lab{
			color: black!important;
			font-weight: bold;
			padding-left: 20px;
			font-size: 16px!important;
		}
		.form-control{
		  background-color: #404450eb!important;
		  color: #fff;
          border-color: #404450eb!important;
          padding-bottom: 10px;
          font-size: 16px!important;
		}
	</style>
	<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">
						<span class="fw-mediumbold">
						User Details</span> 
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Name:</label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->name?>
	                </div>
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Age: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                    <?php echo age_count($user->dob);?>
	                </div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Gender: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                     <?php echo $user->gender;?>
	                </div>
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Diet For:</label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->diet_for; ?> 
	                </div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Weight:</label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->weight;?>
	                </div>
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Height: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->height; ?> 
	                </div>

				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Sleeping Hours: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->sleeping_hours; ?> 
	                </div>
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Vegeterian:  </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                    <?php echo $user->vegeterian; ?> 
	                </div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Bad Habbits: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                    <?php echo $user->habbit; ?>  
	                </div>
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Lifestyle:  </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                    <?php echo $user->life_style; ?> 
	                </div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Working Hours: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                    <?php echo $user->working_hour; ?>  
	                </div>
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Sitting Job:  </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                    <?php echo $user->sitting_job; ?> 
	                </div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Clinical History: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                    <?php echo $user->clinical_history; ?>  
	                </div>
				</div>
			</form>
				</div>
				<div class="modal-footer no-bd">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
 </div>
