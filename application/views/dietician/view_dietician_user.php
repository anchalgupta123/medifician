<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">View User Profile For Dietician</h4>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover" >
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>Name</th>
										<th>Age</th>
										<th>Gender</th>
										<th>Email</th>
										<th>Mobile_no</th>
										<th>Address</th>
										<th>Register Date</th>
									</tr>
								</thead>
							    <tbody>
									<?php $x = 1; foreach ($user as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
				                        <td><?php echo $key->user_name; ?></td>
				                        <td><?php echo $key->age; ?></td>
				                        <td><?php echo $key->gender; ?></td>
				                        <td><?php echo $key->email; ?></td>
				                        <td><?php echo $key->mobile_no; ?></td>
				                        <td><?php echo $key->address; ?></td>
				                        <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
</div>
</div>
    <?php $this->load->view('bars/js');?>
    <script >
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});

		});
	</script>
	<script type="text/javascript">
	

	</script>
</body>
</html>