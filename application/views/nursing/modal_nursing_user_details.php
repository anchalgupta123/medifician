	<style type="text/css">
		.lab{
			color: black!important;
			font-weight: bold;
			padding-left: 20px;
			font-size: 16px!important;
		}
		.form-control{
		  background-color: #404450eb!important;
		  color: #fff;
          border-color: #404450eb!important;
          padding-bottom: 10px;
          font-size: 16px!important;
		}
	</style>
	<div class="modal-dialog" role="document">
			<div class="modal-content" style="width: 630px!important;">
				<div class="modal-header">
					<h5 class="modal-title">
						<span class="fw-mediumbold">
						User Details</span> 
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Name:</label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->name?>
	                </div>
	                <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Email:</label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->email; ?>  
	                </div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Permanant Mobile: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                     <?php echo $user->p_mobile_no; ?>
	                </div>
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Alternate Mobile: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                     <?php echo $user->alt_mobile_no; ?>
	                </div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Address:</label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->address; ?>
	                </div>
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Landmark: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                    <?php echo $user->landmark; ?>
	                </div>

				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">city: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->a_city; ?>
	                </div>
	                <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">state: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->a_state; ?>
	                </div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Appointment Date: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo change_date_format_dmy($user->appointment_date); ?>
	                </div>
	                <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Appointment Time: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->appointment_time; ?>
	                </div>
				</div>
				<div class="form-group row">
					<label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Clinical History: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->clinical_history; ?>
	                </div>
	                <label class="col-md-3 col-sm-3 col-xs-12" for="first-name">Description: </label>
	                <div class="col-md-3 col-sm-3 col-xs-12">
	                   <?php echo $user->description; ?>
	                </div>
				</div>
			</form>
				</div>
				<div class="modal-footer no-bd">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
 </div>
