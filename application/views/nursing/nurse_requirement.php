<!DOCTYPE html>
<html>
<head>
	
	<?php $this->load->view('bars/head');?>
</head>
<body>
	<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h2 class="card-title">View Appointment Details</h2>
							<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal" onclick="add_nursing_modal();"><i class="fa fa-plus"></i> Add Price </button>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>Time</th>
										<th>Price</th>
										<th>Register Date</th>
										<th>Action</th>
									</tr>
								</thead>
							    <tbody>
									  <?php $x = 1; foreach ($price as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
				                        <td><?php echo $key->time; ?></td>
				                        <td><?php echo $key->price; ?></td>
				                        <td><?php echo change_date_format_dmy($key->created_date_time);?></td>
				                        <td><button type="button" class="btn btn-warning btn-sm" onclick="edit_nursing_modal('<?php echo $key->id;?>')">Edit</button>  <button style="margin-top: 4px;margin-bottom: 4px;" type="button" class="btn btn-danger btn-sm" onclick="delete_nursing_price('<?php echo $key->id;?>')">Delete</button></td>
									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
           <?php $this->load->view('bars/js');?>
</div>
</div>
	<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
    </div>
    <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
	</script>
	<script type="text/javascript">
		function add_nursing_modal() {
        $.ajax({
          url: base_url+"Nursing/add_nursing_modal", 
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
	<script type="text/javascript">
		function edit_nursing_modal(id) {
        $.ajax({
          url: base_url+"Nursing/edit_nursing_details",
          type : "POST",
          data:{id :id}, 
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
</body>
</html>