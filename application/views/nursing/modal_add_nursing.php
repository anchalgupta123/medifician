	<style type="text/css">
		label{
			color: black!important;
			font-weight: bold;
		}
	</style>
	<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header no-bd">
					<h5 class="modal-title">
						<span class="fw-mediumbold">
						Add</span> 
						<span class="fw-light">
						Lab 
						</span>
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="row">
							<div class="col-md-12">
								<label>Time:</label>
								<div class="form-group form-group-default">
									<select class="form-control" id="time">
										<option>Select Time</option>
										<option>2 Hour's</option>
										<option>4 Hour's</option>
										<option>6 Hour's</option>
										<option>8 Hour's</option>
										<option>12 Hour's</option>
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<label>Price:</label>
								<div class="form-group form-group-default">
									<input id="price" type="text" class="form-control" placeholder="price">
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer no-bd">
					<button type="button" id="addRowButton" class="btn btn-primary" onclick="add_nursing_price();" >Add</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>

