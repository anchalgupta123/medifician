<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
	<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">View Vendor Details</h4>
							<button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addRowModal" onclick="add_vendor_modal();"><i class="fa fa-plus"></i> Add Vendor</button>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>Name</th>
										<th>Company Name</th>
										<th>Email</th>
										<th>Mobile No.</th>
										<th>Address</th>
										<!-- <th>Username</th> -->
										<th>Role</th>
										<th>Action</th> 
									</tr>
								</thead>
							    <tbody>
									<?php $x = 1; foreach ($vendor as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
				                        <td><?php echo $key->name; ?></td>
				                        <td><?php echo $key->company_name; ?></td>
				                        <td><?php echo $key->email; ?></td>
				                        <td><?php echo $key->mobile_no; ?></td>
				                        <td><?php echo $key->address; ?></td>
				                       <!--  <td><?php echo $key->username; ?></td> -->
				                        <td><?php echo $key->role;?></td>
				                        <td><button style="margin-top: 4px;margin-bottom: 4px;" type="button" class="btn btn-danger btn-sm" onclick="delete_vendor_details('<?php echo $key->id;?>')">Delete</button></td> 
									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
         <?php $this->load->view('bars/js');?>
</div>
</div>
	<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
    </div>
    <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
	</script>
	<script type="text/javascript">
		function add_vendor_modal() {
        $.ajax({
          url: base_url+"Vendor/add_vendor_modal", 
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
</body>
</html>