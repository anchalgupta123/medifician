<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
	<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">View Profile</h4>
						</div>
					</div>
					<div class="card-body">
						<form name="myform">
						<div class="row">
							<div class="offset-1 col-md-6 pr-0">
								<div class="form-group row">
									<label class="col-md-4">Name :</label>
									<input id="name" value="<?php echo $profile->name;?>" type="text" class="form-control col-md-7" placeholder="Name">
								</div>
							</div>
							<div class="offset-1 col-md-6 pr-0">
								<div class="form-group row">
									<label class="col-md-4">Company Name :</label>
									<input id="company_name" value="<?php echo $profile->company_name;?>" type="text" class="form-control col-md-7" placeholder="Company Name">
								</div>
							</div>
							<div class="offset-1 col-md-6 pr-0">
								<div class="form-group row">
									<label class="col-md-4">Email <small>(Username)</small> :</label>
									<input id="email"  type="email" value="<?php echo $profile->email;?>" class="form-control col-md-7" disabled placeholder="Email">
								</div>
							</div>
							<div class="offset-1 col-md-6 pr-0">
								<div class="form-group row">
									<label class="col-md-4">Mobile No. :</label>
									<input id="mobile_no" name="mobile_no"  maxlength="10" minlength="10" type="text" value="<?php echo $profile->mobile_no;?>" class="form-control col-md-7" placeholder="Mobile No.">
								</div>
							</div>
							<div class="offset-1 col-md-6 pr-0">
								<div class="form-group row">
									<label class="col-md-4">Address :</label>
									<input id="address" name="Address"  maxlength="10" minlength="10" type="text" value="<?php echo $profile->address;?>" class="form-control col-md-7" placeholder="Mobile No.">
								</div>
							</div>
							<div class="offset-1 col-md-6 pr-0">
								<div class="form-group row ">
									<label class="col-md-4">Password :</label>
									<input id="password" type="text" value="<?php echo $profile->real_password;?>" class="form-control col-md-7" placeholder="Password">
								</div>
								<input type="hidden" value="<?php echo $profile->id; ?>" id="profile_id">
							</div>
						</div>
					</form>
					<br>
					<button type="button" id="addRowButton" class="btn btn-primary offset-4" onclick="update_vendor_profile();" >Update Profile</button>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
         <?php $this->load->view('bars/js');?>
</div>
</div>
	<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
    </div>
    <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
	</script>
</body>
</html>