	<style type="text/css">
		.lab{
			color: black!important;
			font-weight: bold;
		}
	</style>
	<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header no-bd">
					<h5 class="modal-title">
						<span class="fw-mediumbold">
						Add</span> 
						<span class="fw-light">
						Vendor
						</span>
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form name="myform">
						<div class="row">
							<div class="col-md-6 pr-0">
								<div class="form-group">
									<label>Name</label>
									<input id="name" type="text" class="form-control" placeholder="Name">
								</div>
							</div>
							<div class="col-md-6 pr-0">
								<div class="form-group">
									<label>Company Name</label>
									<input id="company_name" type="text" class="form-control" placeholder="Company Name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Email</label>
									<input id="email" type="email" onblur="validateEmail(this);" class="form-control" placeholder="Email">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Mobile No.</label>
									<input id="mobile_no" name="mobile_no" onkeypress="return isNumber(event)" onblur="validPhone(this);"  maxlength="10" minlength="10" type="text" class="form-control" placeholder="Mobile No.">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Address</label>
									<input id="address" type="text" class="form-control" placeholder="Address">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="">Password</label>
									<input id="password" type="password" name="password"  onblur="validation()" class="form-control" placeholder="Password">
								</div>
							</div>
							<div class="col-md-12">
								<label  style="color: black!important;font-weight: bold;">Role :</label>
							</div>
                            <div class="form-group">
							<div class="selectgroup selectgroup-pills">
								<label class="selectgroup-item">
									<input type="checkbox" id="role" name="value" value="Pharmacy" class="selectgroup-input">
									<span class="selectgroup-button">Pharmacy</span>
								</label>
								<label class="selectgroup-item">
									<input type="checkbox" id="role2" name="value" value="Dietician" class="selectgroup-input">
									<span class="selectgroup-button">Dietician</span>
								</label>
								<label class="selectgroup-item">
									<input type="checkbox" id="role3" name="value" value="Nursing" class="selectgroup-input">
									<span class="selectgroup-button">Nursing</span>
								</label>
								<label class="selectgroup-item">
									<input type="checkbox" id="role4" name="value" value="Physiotherapy" class="selectgroup-input">
									<span class="selectgroup-button">Physiotherapy</span>
								</label>
								<label class="selectgroup-item">
									<input type="checkbox" id="role5" name="value" value="Radiologist" class="selectgroup-input">
									<span class="selectgroup-button">Radiologist</span>
								</label>
								<label class="selectgroup-item">
									<input type="checkbox" id="role6" name="value" value="Pathology" class="selectgroup-input">
									<span class="selectgroup-button">Pathology</span>
								</label>
							</div>
						</div>		
        			</div>
					</form>
				</div>
				<div class="modal-footer no-bd">
					<button type="button" id="addRowButton" class="btn btn-primary" onclick="add_vendor();" >Add</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
<script>  
	function validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField.value) == false) 
        {
            alert('Invalid Email Address !Please Fill Correct Email');
            return false;
        }

        return true;

    }
    function isNumber(evt) {
	  evt = (evt) ? evt : window.event;
	  var charCode = (evt.which) ? evt.which : evt.keyCode;
	  if (charCode > 31 && (charCode < 48 || charCode > 57 )) {
	    alert("Please enter only Numbers.");
	    return false;
	  }
	  return true;
	}
	function validPhone(){
		var mobile_no= document.myform.mobile_no.value;
		if (mobile_no.length<10){
	    alert ("Mobile Number Must be 10 Characters");
	    return false;
	    }
	    return true;
	}
	function validation(){
	    var password= document.myform.password.value;
	    if (password.length<8){
	        alert ("Password Must be 8 Characters");
	        return false;
	        }
	    return true;
    }
	</script> 


