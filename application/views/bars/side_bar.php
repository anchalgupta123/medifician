<?php $login_role = $this->session->userdata('login_role');
$role=str_replace(', ', ',', $login_role);
?>
<div class="wrapper">	
<div class="sidebar sidebar-style-2" data-background-color="dark2">
		<div class="sidebar-wrapper scrollbar scrollbar-inner">
			<div class="sidebar-content">
				<ul class="nav nav-primary">
					<?php if($role=='Admin'){?>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Dashboard">
							<i class="fas fa-layer-group"></i>
							<p>Dashboard</p>
						</a>	
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Vendor/view_banner">
							<i class="fas fa-image"></i>
							<p>Banner</p>
						</a>	
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Users/view_users_profile">
							<i class="fas fa-users"></i>
							<p>User Profile</p>
						</a>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#profile">
							<i class="fas fa-file-medical"></i>
							<p>Online Pharmacy</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="profile">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url();?>Pharmacy/view_prescription">
										<span class="sub-item">Uploaded Prescription</span>
									</a>
								</li>
								<li>
									<a data-toggle="collapse" href="#profile1">
										<span class="sub-item" style=""></span>
										<p>Orders</p>
										<span class="caret"></span>
									</a>
									<div class="collapse" id="profile1">
										<ul class="nav nav-collapse">
											<li class="nav-item">
												<a href="<?php echo base_url();?>Pharmacy/view_confirm_order_details">
													<span class="sub-item">Confirm Order</span>
												</a>
											</li>
											<li class="nav-item">
												<a href="<?php echo base_url();?>Pharmacy/view_delivered_order_details">
													<span class="sub-item">Delivered Order</span>
												</a>
											</li>
											<li class="nav-item">
												<a href="<?php echo base_url();?>Pharmacy/view_canceled_order_details">
													<span class="sub-item">Cancelled Order</span>
												</a>
											</li>
										</ul>
									</div>
								</li> 
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#Pathology">
							<i class="fas fa-user-md"></i>
							<p>Pathology</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="Pathology">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url();?>Pathology/view_pathology_labs">
										<span class="sub-item">Add Labs</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>Pathology/view_pathology_test">
										<span class="sub-item">Add Tests</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>Pathology/view_health_package_test">
										<span class="sub-item">Add Health Packages</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>Pathology/view_executive_health_package_test">
										<span class="sub-item">Add Executive Health Packages</span>
									</a>
								</li>
								
								<li>
									<a  href="<?php echo base_url();?>Pathology/view_pathology_prescription">
										<span class="sub-item" style=""></span>
										<p>Uploaded Prescription</p>
									</a>
								</li> 
								<li>
									<a data-toggle="collapse" href="#order_list">
										<span class="sub-item" style=""></span>
										<p>Order List</p>
										<span class="caret"></span>
									</a>
									<div class="collapse" id="order_list">
										<ul class="nav nav-collapse">
											<li class="nav-item">
												<a href="<?php echo base_url();?>Pathology/view_pathologys_paid_order_details">
													<span class="sub-item">Confirm Order</span>
												</a>
											</li>
											<li class="nav-item">
												<a href="<?php echo base_url();?>Pathology/view_pathology_delivered_order">
													<span class="sub-item">Delivered Order</span>
												</a>
											</li>
											<li class="nav-item">
												<a href="<?php echo base_url();?>Pathology/view_pathology_cancelled_order">
													<span class="sub-item">Cancelled Order</span>
												</a>
											</li>
										</ul>
									</div>
								</li> 
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#charts">
							<i class="fas fa-x-ray"></i>
							<p>Radiology</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="charts">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url();?>Radiology/view_radiology_labs">
										<span class="sub-item">Add Diagonostics Centre</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>Radiology/view_radiology_test">
										<span class="sub-item">Add Investigation</span>
									</a>
								</li>
								<li>
									<a  href="<?php echo base_url();?>Radiology/view_radiology_prescription">
										<span class="sub-item" style=""></span>
										<p>Uploaded Prescription</p>
									</a>
								</li> 
								<li>
									<a data-toggle="collapse" href="#Radio_order_list">
										<span class="sub-item" style=""></span>
										<p>Order List</p>
										<span class="caret"></span>
									</a>
									<div class="collapse" id="Radio_order_list">
										<ul class="nav nav-collapse">
											<li class="nav-item">
												<a href="<?php echo base_url();?>Radiology/view_radiology_paid_order_details">
													<span class="sub-item">Confirm Order</span>
												</a>
											</li>
											<li class="nav-item">
												<a href="<?php echo base_url();?>Radiology/view_radiology_delivered_order">
													<span class="sub-item">Delivered Order</span>
												</a>
											</li>
											<li class="nav-item">
												<a href="<?php echo base_url();?>Radiology/view_radiology_cancelled_order">
													<span class="sub-item">Cancelled Order</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#Physiotherapy">
							<i class="fas fa-first-aid"></i>
							<p>Physiotherapy</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="Physiotherapy">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url();?>Physiotherapy/view_physiotherapy_labs">
										<span class="sub-item">Add Physiotherapist Centre</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>Physiotherapy/view_physiotherapy_test">
										<span class="sub-item">Add Therapy</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>Physiotherapy/view_physiotherapist_prescription">
										<span class="sub-item" style=""></span>
										<p>Uploaded Prescription</p>
									</a>
									
								</li>
								<li>
									<a data-toggle="collapse" href="#Physio_order_list">
										<span class="sub-item" style=""></span>
										<p>Order List</p>
										<span class="caret"></span>
									</a>
									<div class="collapse" id="Physio_order_list">
										<ul class="nav nav-collapse">
											<li class="nav-item">
												<a href="<?php echo base_url();?>Physiotherapy/view_physiotherapy_paid_details">
													<span class="sub-item">Confirm Order</span>
												</a>
											</li>
											<li class="nav-item">
												<a href="<?php echo base_url();?>Physiotherapy/view_physiotherapy_delivered_order">
													<span class="sub-item">Delivered Order</span>
												</a>
											</li>
											<li class="nav-item">
												<a href="<?php echo base_url();?>Physiotherapy/view_physiotherapy_cancelled_order">
													<span class="sub-item">Cancelled Order</span>
												</a>
											</li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#Nursing">
							<i class="fas fa-user-md"></i>
							<p>Nursing Care</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="Nursing">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url();?>Nursing/view_nursing_add_details">
										<span class="sub-item">Add Nursing Price</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>Nursing/view_nursing_pending_details">
										<span class="sub-item">Pending Order</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>Nursing/view_nursing_order">
										<span class="sub-item">Order List</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#maps">
							<i class="fab fa-nutritionix" style="font-size: 25px;"></i>
							<p>Nutrition & <br>Diet Consultant</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="maps">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url();?>Dietician/view_dietician_order_details">
										<span class="sub-item">Order Details</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Vendor/view_vendor">
							<i class="fas fa-users"></i>
							<p> Vendor</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Vendor/onscreen_user_notification">
							<i class="fas fa-envelope"></i>
							<p>On Screen Notification</p>
						</a>	
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Vendor/bell_user_notification">
							<i class="far fa-bell"></i>
							<p>Bell Notifications</p>
						</a>	
					</li>
					<?php }?>
					<?php if($role=='Pharmacy'){?>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Dashboard/pharmacy_dashboard">
							<i class="fas fa-layer-group"></i>
							<p>Dashboard</p>
						</a>
						<li class="nav-item">
						<a data-toggle="collapse" href="#profile">
							<i class="fas fa-file-medical"></i>
							<p>Online Pharmacy</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="profile">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url();?>Pharmacy/view_prescription">
										<span class="sub-item">Uploaded Prescription</span>
									</a>
								</li>
								<li>
									<a data-toggle="collapse" href="#profile1">
										<span class="sub-item" style=""></span>
										<p>Orders</p>
										<span class="caret"></span>
									</a>
									<div class="collapse" id="profile1">
										<ul class="nav nav-collapse">
											<li class="nav-item">
												<a href="<?php echo base_url();?>Pharmacy/view_confirm_order_details">
													<span class="sub-item">Confirm Order</span>
												</a>
											</li>
											<li class="nav-item">
												<a href="<?php echo base_url();?>Pharmacy/view_delivered_order_details">
													<span class="sub-item">Delivered Order</span>
												</a>
											</li>
											<li class="nav-item">
												<a href="<?php echo base_url();?>Pharmacy/view_canceled_order_details">
													<span class="sub-item">Cancelled Order</span>
												</a>
											</li>
										</ul>
									</div>
								</li> 
							</ul>
						</div>
					</li>	
					</li>
					<?php }?>
					<?php if($role=='Dietician'){?>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Dashboard/ditesian_dashboard">
							<i class="fas fa-layer-group"></i>
							<p>Dashboard</p>
							<!-- <span class="caret"></span> -->
						</a>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#order">
							<i class="fas fa-users"></i>
							<p>Order Master</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="order">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url();?>Dietician/view_single_dietician_order_details">
										<span class="sub-item">Order Details</span>
									</a>
								</li>
							</ul>
						</div>	
					</li>
					<?php }?>
					<?php if($role=='Nursing'){?>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Dashboard/nursing_dashboard">
							<i class="fas fa-layer-group"></i>
							<p>Dashboard</p>
							<span class="caret"></span>
						</a>
						<a href="<?php echo base_url();?>">
							<i class="fas fa-layer-group"></i>
							<p>Nursing</p>
							<span class="caret"></span>
						</a>	
					</li>
					<?php }?>
					<?php if($role=='Physiotherapy'){?>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Dashboard/physiotherapy_dashboard">
							<i class="fas fa-layer-group"></i>
							<p>Dashboard</p>
							<span class="caret"></span>
						</a>
						<a href="<?php echo base_url();?>">
							<i class="fas fa-layer-group"></i>
							<p>Physiotherapy</p>
							<span class="caret"></span>
						</a>	
					</li>
					<?php }?>
					<?php
					 if($role=='Dietician,Nursing' || $role=='Nursing,Dietician'){?>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Dashboard">
							<i class="fas fa-layer-group"></i>
							<p>Dashboard</p>
							<span class="caret"></span>
						</a>
						<a href="<?php echo base_url();?>Diatician/view_single_dietician_order_details">
							<i class="fas fa-layer-group"></i>
							<p>Diatician</p>
							<span class="caret"></span>
						</a>	
						<a href="<?php echo base_url();?>">
							<i class="fas fa-layer-group"></i>
							<p>Nursing</p>
							<span class="caret"></span>
						</a>	
					</li>
					<?php }?>
					<?php if($role=='Dietician,Physiotherapy'||$role=='Physiotherapy,Dietician'){?>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Dashboard">
							<i class="fas fa-layer-group"></i>
							<p>Dashboard</p>
							<span class="caret"></span>
						</a>
						<a href="<?php echo base_url();?>Dietician/view_single_dietician_order_details">
							<i class="fas fa-layer-group"></i>
							<p>Diatician</p>
							<span class="caret"></span>
						</a>	
						<a href="<?php echo base_url();?>">
							<i class="fas fa-layer-group"></i>
							<p>Physiotherapy</p>
							<span class="caret"></span>
						</a>	
					</li>
					<?php }?>
					<?php if($role=='Nursing,Physiotherapy'||$role=='Physiotherapy,Nursing'){?>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Dashboard">
							<i class="fas fa-layer-group"></i>
							<p>Dashboard</p>
							<span class="caret"></span>
						</a>
						<a href="<?php echo base_url();?>">
							<i class="fas fa-layer-group"></i>
							<p>Nursing</p>
							<span class="caret"></span>
						</a>	
						<a href="<?php echo base_url();?>">
							<i class="fas fa-layer-group"></i>
							<p>Physiotherapy</p>
							<span class="caret"></span>
						</a>	
					</li>
					<?php }?>
					<?php if($role=='Dietician,Nursing,Physiotherapy' ||$role=='Dietician,Physiotherapy,Nursing'){?>
					<li class="nav-item">
						<a href="<?php echo base_url();?>Dashboard">
							<i class="fas fa-layer-group"></i>
							<p>Dashboard</p>
							<span class="caret"></span>
						</a>
						<a href="<?php echo base_url();?>Dietician/view_single_dietician_order_details">
							<i class="fas fa-layer-group"></i>
							<p>Diatician</p>
							<span class="caret"></span>
						</a>
						<a href="<?php echo base_url();?>">
							<i class="fas fa-layer-group"></i>
							<p>Nursing</p>
							<span class="caret"></span>
						</a>	
						<a href="<?php echo base_url();?>">
							<i class="fas fa-layer-group"></i>
							<p>Physiotherapy</p>
							<span class="caret"></span>
						</a>	
					</li>
					<?php }?>
					  
				</ul>
			</div>
		</div>
	</div>
</div>

