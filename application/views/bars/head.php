    <!-- Fonts and icons -->
	
   <link rel="icon" href="<?php echo base_url();?>assets/img/icon.ico" type="image/x-icon"/>
	<!-- CSS Files -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/atlantis.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/demo.css">
	<!-- CSS Just for demo purpose, don't include it in your project -->
	
    <base href="<?php echo base_url();?>">


    <link rel="icon" href="<?php echo base_url();?>files/logo.png" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?php echo base_url();?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?php echo base_url();?>assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->