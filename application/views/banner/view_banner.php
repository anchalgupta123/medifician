<!DOCTYPE html>
<html>
<head>
	
	<?php $this->load->view('bars/head');?>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
</head>
<body>
	<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
                  <div class="card-header">
					<div class="d-flex align-items-center">
						<h4 class="card-title">Add Banner Image</h4>
					</div>
				</div>
                  <!-- <div class="x_content"> -->
                  	<div class="card-body">
                    <br />
                   	<form name="myform" id="myform">
						<div class="row">
							<div class="col-md-6 pr-0">
								<label>Banner Image <span class="required" style="color: red;">*</span></label>
								<div class="form-group form-group-default" style="padding-bottom: 15px;">
									 <input type="file" id="banner_image" name="file" required="required" class="form-control" onchange="change_img('banner_image','view_img');">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									 <img src="#" id="view_img" width="120" height="80">
								</div>
							</div>
							
						</div>
						<button type="button" id="addRowButton" class="btn btn-primary" onclick="add_banner();">Add</button>
		 			</form>
		     	</div>
		     </div>
		<div class="card">
				<div class="card-header">
					<div class="d-flex align-items-center">
						<h4 class="card-title">View Banner </h4>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="multi-filter-select" class="display table table-striped table-hover">
						<thead>
		                    <tr>
		                      <th>Sr.No.</th>
		                      <th>Image</th>
		                      <th>Date/Time</th>
		                      <th>Action</th>
		                    </tr>
		                  </thead>
		                  <tbody>
		                    <?php $x = 1; foreach ($banner as $key){ ?>
		                    <tr>
		                      <td><?php echo $x++; ?></td>
		                      <td><img src="<?php echo base_url();?>files/banner_img/<?php echo $key->banner_image; ?>" width="50"></a></td>
		                      <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
		                      <td><button class="btn btn-danger btn-xs" id="alert_demo_7" onclick="delete_banner('<?php echo $key->id;?>');"><i class="fa fa-trash"></i></button></td>
		                    </tr>
		                    <?php } ?>
		                  </tbody>
						</table>
					</div>
				</div>
			</div>
		 </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
</div>
</div>
    <?php $this->load->view('bars/js');?>
    <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
</script>
</body>
</html>