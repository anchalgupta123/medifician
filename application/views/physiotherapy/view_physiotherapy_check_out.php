<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
    .lab label{
      font-size: 16px!important;
    }
    input[type="text"]:disabled {
    /*background:red!important;*/
    color: black!important;
    }
  </style>
  <?php $this->load->view('bars/head');?>
</head>
<body>
  <div class="wrapper">
  
    <?php $this->load->view('bars/header');?>
    <?php $this->load->view('bars/side_bar');?>
  
   <div class="main-panel">
    <div class="content">
     <div class="page-inner">
        <div class="col-md-12">
        <div class="card">
                  <div class="card-header">
          <div class="d-flex align-items-center">
            <h4 class="card-title">User Order Details</h4>
          </div>
        </div>
      <div class="card-body">
        <br />
        <form name="myform" id="myform">
            <div class="row lab">
              <div class="col-md-7 ">
                <label>Name<span class="required">*</span></label>
                <div class="form-group " style="padding-bottom: 15px;">
                   <input type="text" id="name" required="required" class="form-control" required style="height: 50px;" disabled value="<?php echo $check_out->name;?>">
                </div>
              </div>
              <div class="col-md-7 ">
                <label>Test Name<span class="required">*</span></label>
                <div class="form-group " style="padding-bottom: 15px;">
                   <input type="text" id="test_name" required="required" class="form-control" required style="height: 50px;" disabled value="<?php echo $check_out->test_name;?>">
                </div>
              </div>
              <div class="col-md-7">
                <label>Price<span class="required">*</span></label>
                <div class="form-group" style="padding-bottom: 15px;">
                   <input type="text" id="price" required="required" class="form-control" required style="height: 50px;" disabled value="<?php echo $check_out->price;?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2">
                <label>GST (in %):<span class="required">*</span></label>
                <div class="form-group" style="padding-bottom: 15px;">
                   <input type="text" id="gst_percent_amount" onkeyup="add_gst_calculation();" required="required" class="form-control" required style="height: 50px;">
                </div>
              </div>
              <div class="col-md-5">
                <label>GST Amount<span class="required">*</span></label>
                <div class="form-group" style="padding-bottom: 15px;">
                   <input type="text" id="gst_amount" required="required" class="form-control" required style="height: 50px;">
                </div>
              </div>
              <div class="col-md-7">
                <label>Total Amount:<span class="required">*</span></label>
                <div class="form-group" style="padding-bottom: 15px;">
                   <input type="text" id="total_amount" required="required" class="form-control" required style="height: 50px;">
                </div>
              </div>
              <input type="hidden" id="order_id" value="<?php echo $check_out->id;?>" >
            </div>
            <div>
            <button type="button" class="btn btn-primary" id="btn_submit" onclick="update_physiotherapy_total_amount();" style="margin-left: 50%;" >Submit</button>
          </div>
          </form>
          </div>
         </div>
          <div class="card">
              <div class="card-header">
                <div class="d-flex align-items-center">
                  <h4 class="card-title">View Details </h4>
                </div>
              </div>
              <div class="card-body">
               <div class="table-responsive">
              <table id="multi-filter-select" class="display table table-striped table-hover">
                <thead>
                  <tr>
                    <th>Sr.no</th>
                    <th>User Name</th>
                    <th>Item Name</th>
                    <th>Price</th>
                    <th>GST Amount</th>
                    <th>Total Amount</th>
                    <th>Register Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    <?php $x = 1; foreach ($get_check_out as $key){ ?>
                  <tr>
                    <td><?php echo $x++; ?></td>
                    <td><a href="javascript:void(0);" style="text-decoration: none;color:#9097a5;" onclick="show_pathology_user_detail_modal(<?php echo $key->id; ?>);"><?php echo $key->name; ?></a></td>
                    <td><?php echo $key->test_name; ?></td>
                    <td><?php echo $key->price; ?></td>
                    <td><?php echo $key->gst_amount; ?></td>
                    <td><?php echo $key->total_amount; ?></td>
                    <td><?php echo $key->created_date_time; ?></td> 
                    <td><a target="_blanck" href="<?php echo base_url();?>Physiotherapy/open_physiotherapy_invoice?user_id=<?php echo $key->id;?>"><button type="button" style="font-weight: bold;margin-top: 5PX;" class="btn btn-primary btn-sm">GENERATE INVOICE</button></a></td>      
                  </tr>
                    <?php } ?>
                </tbody> 
              </table>
            </div>
              </div>
          </div>
     </div>
    </div>
  </div>
         <?php $this->load->view('bars/footer');?>
</div>
</div>
    <?php $this->load->view('bars/js');?>
    <script>
    $(document).ready(function() {
      $('#multi-filter-select').DataTable({
      });
      
    });
  </script>
  <script type="text/javascript">
    function add_gst_calculation() {
      
      var sub_total_amount = parseFloat($('#price').val());
      var gst_percent_amount = parseFloat($('#gst_percent_amount').val());

      var gst_amount=(gst_percent_amount/100)*sub_total_amount;

      var total_calculate_add_gst=sub_total_amount +gst_amount;
      $('#gst_amount').val(Math.round(gst_amount));
      $('#total_amount').val(Math.round(total_calculate_add_gst));

    }
  </script>
</body>
</html>