	<style type="text/css">
		label{
			color: black!important;
			font-weight: bold;
		}
	</style>
	<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header no-bd">
					<h5 class="modal-title">
						<span class="fw-mediumbold">
						Add</span> 
						<span class="fw-light">
						Lab 
						</span>
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form name="myform" method="post">
						<div class="row">
							<div class="col-md-6 pr-0">
								<label>Lab Name:</label>
								<div class="form-group form-group-default">
									<input id="lab_name" type="text" class="form-control" placeholder="Lab Name" required>
								</div>
							</div>
							<div class="col-md-6">
								<label>Email:</label>
								<div class="form-group form-group-default">
									
									<input id="email" type="email" onblur="validateEmail(this);" class="form-control" placeholder="Email" required>
								</div>
							</div>
							<div class="col-sm-12">
								<label>Address:</label>
								<div class="form-group form-group-default">
									
									<textarea id="address" type="text" class="form-control" placeholder="Lab Address" required></textarea>
								</div>
							</div>
							<div class="col-md-6 pr-0">
								<label>City:</label>
								<div class="form-group form-group-default">
									
									<input id="p_city" type="text" class="form-control" placeholder="city" required>
								</div>
							</div>
							<div class="col-md-6">
								<label>State:</label>
								<div class="form-group form-group-default">
									
									<input id="p_state" type="text" class="form-control" placeholder="State" required>
								</div>
							</div>
							<div class="col-md-6 pr-0">
								<label>Mobile No:</label>
								<div class="form-group form-group-default">
									
									<input id="mobile_no" name="mobile_no" type="text" onkeypress="return isNumber(event)" minlength="10" maxlength="10" onblur="validPhone(this);"  class="form-control" placeholder="Mobile No" required>
								</div>
							</div>
							<div class="col-md-6">
								<label>Landline No:</label>
								<div class="form-group form-group-default">
									
									<input id="landline_no" name="landline_no" onkeypress="return isNumber(event)" minlength="10" maxlength="10" onblur="validLandline(this);" type="text" class="form-control" placeholder="Landline No" required>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer no-bd">
					<button type="button" id="addRowButton" class="btn btn-primary" onclick="add_physiotherapy_labs();" >Add</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
	<script>  
	function validateEmail(emailField){
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(emailField.value) == false) 
        {
            alert('Invalid Email Address');
            return false;
        }

        return true;

    }
    function validPhone(){
		var mobile_no= document.myform.mobile_no.value;
		if (mobile_no.length<10){
	    alert ("Mobile Number Must be 10 Characters");
	    return false;
	    }
	    return true;
	}
    function isNumber(evt) {
	  evt = (evt) ? evt : window.event;
	  var charCode = (evt.which) ? evt.which : evt.keyCode;
	  if (charCode > 31 && (charCode < 48 || charCode > 57 )) {
	    alert("Please enter only Numbers.");
	    return false;
	  }
	  return true;
	}
    function validLandline(){
		var landline_no= document.myform.landline_no.value;
		if (landline_no.length<10){
	    alert ("Landline Number Must be 10 Characters");
	    return false;
	    }
	    return true;
	}

	</script> 
