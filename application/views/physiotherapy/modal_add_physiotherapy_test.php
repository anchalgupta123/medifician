	<style type="text/css">
		.modal-content{
			width: 700px!important;
		}
		label{
			color: black!important;
			font-weight: bold;
		}
	</style>
	<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header no-bd">
					<h5 class="modal-title">
						<span class="fw-mediumbold">
						Add</span> 
						<span class="fw-light">
						Test
						</span>
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="row">
							<div class="col-md-6">
								<label>Test Name:</label>
								<div class="form-group form-group-default">
									<input id="test_name" type="text" class="form-control" placeholder="Test Name">
								</div>
							</div>
							<div class="col-md-6">
								<label>Description:</label>
								<div class="form-group form-group-default">
									<input id="description" type="email" class="form-control" placeholder="Description">
								</div>
							</div>
						    <div class="row">
		                        <div class="col-sm-12">
		                          <div class="table-responsive">
		                            <table class="table table-bordered table-striped">
		                              <thead>
		                                <tr>
		                                  
		                                  <th>lab name</th>
		                                  <th>Price</th>
		                                  <th>Offer Price</th>
		                                  <th>Off</th>
		                                  <th>Action</th>
		                                </tr>
		                              </thead>
		                              <tbody id="price_tbl">
			                            <tr id="price_row1">
			                              <td>
			                              	<select style="width: 100%;" class="lab_id">
												<?php foreach($physio_lab as $key){?> 
							                       <option value="<?php echo $key->id; ?>" ><?php echo $key->lab_name; ?></option>
							                    <?php }?>
							                    <!-- <option>1</option> -->
											</select></td>
			                              <td><input type="text" size="15" name="price" style="width: 100%;" class="price" id="price_update" onkeyup="off_calculation();"></td>
			                              <td><input type="text" size="15" onkeyup="off_calculate_from_offer_price();" name="offer_price" style="width: 100%;" class="offer_price" id="offer_price_update"></td>
				                           <td><input type="text" size="5" id="off_update" name="off" style="width: 100%;" class="off"></td>
			                              <td><button class="btn btn-danger btn-xs" type="button" onclick="remove_row('price_row1');"><i class="fa fa-minus"></i></button></td>
			                            </tr>
                                     </tbody>
                                     <td>
                                     <button class="btn btn-primary btn-sm" type="button" onclick="add_row_price();"><i class="fa fa-plus"></i> Add Price</button></td>
		                            </table>
		                          </div>
		                        </div>
		                      </div>
						</div>
					</form>
				</div>
				<div class="modal-footer no-bd">
					<button type="button" class="btn btn-primary" onclick="add_physiotherapy_test();" >Add</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>

 <script type="text/javascript">
          temp = 1;
          function add_row_price()
          {
            temp++;
            $('#price_tbl').append('<tr id="price_row'+temp+'"><td><select class="lab_id" style="width: 100%;"><?php foreach($physio_lab as $key){?><option value="<?php echo $key->id; ?>" ><?php echo $key->lab_name; ?></option><?php }?></select</td><td><input type="text" size="15" name="" style="width: 100%;" id="price_update'+temp+'" class="price"  onkeyup="off_calculation22('+temp+');"></td><td><input type="text" size="15" name="" style="width: 100%;" id="offer_price_update'+temp+'" onkeyup="off_calculate_from_offer_price2('+temp+');" class="offer_price"></td><td><input type="text" size="5" name="" style="width: 100%;" class="off" id="off_update'+temp+'"></td><td><button class="btn btn-danger btn-xs" type="button"  onclick="remove_row(&#39;price_row'+temp+'&#39;);"><i class="fa fa-minus"></i></button></td></tr>');
          }
          function remove_row(id)
          {
            $('#'+id).remove();
          }
          
        </script> 
    <script type="text/javascript">
 
     function off_calculation()
    { 
      var originalprice = $('#price_update').val();
      $('#offer_price_update').val('');
      $('#off_update').val('0');
    }
    function off_calculation22(temp)
    { 
      var originalprice34 = $('#price_update'+temp).val();
      $('#offer_price_update'+temp).val('');
      $('#off_update'+temp).val('0');
	}
    function off_calculate_from_offer_price2(temp)
    {
    	var originalprice32 = $('#price_update'+temp).val();
      var offerprice2 = $('#offer_price_update'+temp).val();
      
      var off=((originalprice32 - offerprice2)/originalprice32)* 100;

      $('#off_update'+temp).val(Math.round(off));
    }
    function off_calculate_from_offer_price()
    {
    	var originalprice32 = $('#price_update').val();
      var offerprice2 = $('#offer_price_update').val();
      
      var off=((originalprice32 - offerprice2)/originalprice32)* 100;

      $('#off_update').val(Math.round(off));
    }

  </script>
