	<style type="text/css">
		label{
			color: black!important;
			font-weight: bold;
		}
	</style>
	<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header no-bd">
					<h5 class="modal-title">
						<span class="fw-mediumbold">
						Update Lab </span>
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form name="myform">  
						<div class="row">
							<div class="col-md-6 pr-0">
								<label>Lab Name <span class="required" style="color: red;">*</span> :</label>
								<div class="form-group form-group-default">
									<input id="lab_name" name="lab_name" type="text" class="form-control" placeholder="Lab Name" value="<?php echo $lab_data->lab_name;?>"  class="required">
								</div>
								
								<input type="hidden" id="lab_id" value="<?php echo $lab_data->id;?>" name="">
							</div>
							<div class="col-md-6">
								<label>Email <span class="required" style="color: red;">*</span> :</label>
								<div class="form-group form-group-default">
									
									<input id="email" type="email" onblur="validateEmail(this);" name="email" class="form-control" value="<?php echo $lab_data->email;?>" placeholder="Email"  required="required">
								</div>
							</div>
							<div class="col-sm-12">
								<label>Address <span class="required" style="color: red;">*</span> :</label>
								<div class="form-group form-group-default">
									
									<textarea id="address" name="address" type="text" class="form-control" placeholder="Lab Address" required="required"><?php echo $lab_data->address;?></textarea>
								</div>
							</div>
							<div class="col-md-6 pr-0">
								<label>City <span class="required" style="color: red;">*</span> :</label>
								<div class="form-group form-group-default">
									
									<input id="p_city" name="city" type="text" class="form-control" placeholder="city" value="<?php echo $lab_data->p_city;?>"  required="required">
								</div>
							</div>
							<div class="col-md-6">
								<label>State <span class="required" style="color: red;">*</span> :</label>
								<div class="form-group form-group-default">
									
									<input id="p_state" name="state" type="text" value="<?php echo $lab_data->p_state;?>" class="form-control" placeholder="State"  required="required">
								</div>
							</div>
							<div class="col-md-6 pr-0">
								<label>Mobile No <span class="required" style="color: red;">*</span> :</label>
								<div class="form-group form-group-default">	
									<input id="mobile_no" name="mobile_no" type="text" class="form-control" placeholder="Mobile No" onkeypress="return isNumber(event)" onblur="validPhone();" maxlength="10" minlength="10" value="<?php echo $lab_data->mobile_no;?>"  required="required">
								</div>
							</div>
							<div class="col-md-6">
								<label>Landline No <span class="required" style="color: red;">*</span> :</label>
								<div class="form-group form-group-default">
									
									<input id="landline_no" name="landline_no"  onkeypress="return isNumber(event)" onblur="validLandline();" type="text" class="form-control" placeholder="Landline No" value="<?php echo $lab_data->landline_no;?>" required="required">
								</div>
							</div>
							<p id="demo"></p>
						</div>
					</form>
				</div>
				<div class="modal-footer no-bd">
					<button type="button" id="demo"  class="btn btn-primary" onclick="edit_physiotherapy_lab_details();" >Add</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>
		<script type="text/javascript">
		function validateEmail(emailField){
		    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

		    if (reg.test(emailField.value) == false) 
		    {
		        alert('Invalid Email Address');
		        return false;
		    }

		    return true;
		}
		function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		alert("Please enter only Numbers.");
		return false;
		}
		return true;
		}
		function validPhone(){
			var mobile_no= document.myform.mobile_no.value;
			if (mobile_no.length<10){
		    alert ("Mobile Number Must be 10 Characters");
		    return false;
		    }
		    return true;
		}
		/*function validLandline(){
			var landline_no= document.myform.landline_no.value;
			if (landline_no.length<10 || landline_no.length>10){
		    alert ("Landline Number Must be 10-12 Characters");
		    return false;
		    }
		    return true;
		}*/
		</script>
