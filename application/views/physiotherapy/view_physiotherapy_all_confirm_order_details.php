<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
	<div class="wrapper">
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Order Details</h4>
								</div>
								<div class="card-body">
									<ul class="nav nav-pills nav-secondary nav-pills-no-bd" id="pills-tab-without-border" role="tablist">

										<li class="nav-item">
											<a class="nav-link active " id="pills-physiotherapy-tab-nobd" data-toggle="pill" href="#pills-physiotherapy-nobd" role="tab" aria-controls="pills-physiotherapy-nobd" aria-selected="false"> Physiotherapy Order</a>
										</li>

										<li class="nav-item">
											<a class="nav-link" id="pills-physiotherapy-prescription-tab-nobd" data-toggle="pill" href="#pills-physiotherapy-prescription-nobd" role="tab" aria-controls="pills-physiotherapy-prescription-nobd" aria-selected="false"> Physiotherapy Prescription Order</a>
										</li>

									</ul>
									<div class="tab-content mt-2 mb-3" id="pills-without-border-tabContent">
                                        <div class="tab-pane fade show active" id="pills-physiotherapy-nobd" role="tabpanel" aria-labelledby="pills-physiotherapy-tab-nobd">
											<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>UserName</th>
										<th>Test Name</th>
										<th>Lab Name</th>
										<th>Price</th>
										<th>Payment Type</th>
										<th>Payment Status</th>
										<th>Delivery Status</th>
										<th>Action</th>
										<th>Register Date</th>
									</tr>
								</thead>
							    <tbody>
									  <?php $x = 1; foreach ($order as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
										<td><a href="javascript:void(0);" style="text-decoration: none;color:#575962;" onclick="show_physiotherapy_user_detail_modal(<?php echo $key->id; ?>);"><?php echo $key->name; ?></a></td>
										<td><?php echo $key->test_name;?></td>
									    <td><?php echo $key->lab_name;  ?></td>
									    <td><?php echo $key->price; ?></td>
									    <td><?php 
										 if($key->txn_type=='0') 
                                          {
                                          	echo "Cash On Delivery";
                                          }
                                          if($key->txn_type=='1') 
                                          {
                                          	echo "Payment Gateway";
                                          }
										?></td>
										<td> 
										 <?php 
                                           if ($key->status =='0') 
                                           {
                                           	echo "Unpaid";
                                           }
                                          if($key->status=='1') 
                                          {
                                          	echo "Paid";
                                          } 
				                        ?>
										</td>
										<td>
											<a href="<?php echo base_url();?>Physiotherapy/physiotherapy_order_status?id=<?php echo $key->id;?>"><button type="button" style="font-weight: bold;margin-top: 5PX;" class="btn btn-primary btn-sm">UPDATE STATUS</button></a>
										</td>
				                        <td><?php echo change_date_format_dmy($key->created_date_time);?></td>
				                        <td><button type="button" class="btn btn-info btn-sm" style="font-weight: bold;" onclick="update_physiotherapy_report_modal('<?php echo $key->id; ?>')">UPLOAD REPORT</button>
				                        	<br><a href="<?php echo base_url();?>Physiotherapy/physiotherapy_check_out_details?user_id=<?php echo $key->id;?>"><button type="button" style="font-weight: bold;margin-top: 5PX;" class="btn btn-primary btn-sm">CHECK OUT</button></a></td>
									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
					</div>
                                        
					<div class="tab-pane fade" id="pills-physiotherapy-prescription-nobd" role="tabpanel" aria-labelledby="pills-physiotherapy-prescription-tab-nobd">
						<div class="table-responsive">
							<table id="multi-filter-select2" class="display table table-striped table-hover" >
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>UserName</th>
										<th>Address</th>
										<th>Prescription Image</th>
										<th>Description</th>
										<th>Pricing</th>
										<th>Delivery Status</th>
										<th>Payment Type</th>
										<th>Register Date</th>
										<th>Action</th>
									</tr>
								</thead>
							    <tbody>
									<?php $x = 1; foreach ($physiotherapy as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
				                        <td><a href="javascript:void(0);" style="color:#575962;" onclick="show_physiotherapy_prescription_user_detail_modal('<?php echo $key->id;?>');"><?php echo $key->name;?></a></td>
				                        <td><?php echo $key->address; ?></td>
				                        <td><a href="" download><img src="<?php echo base_url();?>files/prescription_img/<?php echo $key->prescription_image;?>" width="70" height="70" class="downloadable"/></a>
				                        </td>
				                        <td><?php echo $key->problem; ?></td>
				                        <td><?php echo $key->pricing; ?></td>
				                        
				                          <td><?php
				                           if($key->status==0)
				                           {
				                            echo "New Order";
				                           }
				                           if($key->status==1)
				                           {
				                            echo "Pending Order";
				                           }
				                           if($key->status==2)
				                           {
				                            echo "Confirm Order";
				                           }
				                           if($key->status==3)
				                           {
				                            echo "Delivered Order";
				                           }
				                          ?> 
				                        </td>
				                        <td><?php
				                           if($key->payment_type==0)
				                           {
				                            echo "Cash on Delivery";
				                           }
				                           if($key->payment_type==1)
				                           {
				                            echo "Payment Gateway";
				                           }
				                           if($key->payment_type==2)
				                           {
				                            echo "Paid";
				                           }
				                          ?> 
				                        </td>
				                        <td><?php echo change_date_format_dmy($key->created_date_time);?></td>
				                        <td><a href="<?php echo base_url();?>Physiotherapy/physiotherapy_prescription_check_out_details?user_id=<?php echo $key->id;?>"><button type="button" style="font-weight: bold;margin-top: 5PX;" class="btn btn-primary btn-sm">CHECK OUT</button></a>
				                        <a href="<?php echo base_url();?>Physiotherapy/physiotherapy_prescription_status?id=<?php echo $key->id;?>"><button type="button" style="font-weight: bold;margin-top: 5PX;" class="btn btn-primary btn-sm">UPDATE STATUS</button></a></td>
									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
										</div>
									</div>
								</div>
							</div>
						</div>
		

					</div>
				</div>
			</div>
			<?php $this->load->view('bars/footer');?>
			<?php $this->load->view('bars/js');?>

		</div>
	</div>
	<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
    </div>
    <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
		$(document).ready(function() {
			$('#multi-filter-select2').DataTable({
			});
			
		});
		
	</script>
	<script type="text/javascript">
		function update_physiotherapy_report_modal(id) {
        $.ajax({
          url: base_url+"Physiotherapy/update_physiotherapy_report_modal", 
          type : "POST",
          data :{id : id},
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
	<script type="text/javascript">
  	 function show_physiotherapy_user_detail_modal(id) {
        $.ajax({
          url: base_url+"Physiotherapy/show_physiotherapy_user_detail_modal", 
          type : "POST",
          data: {id : id},
          success: function(result)
          {
            console.log(result);
              $('#modal_details').html(result);
              $('#modal_details').modal('show');
          }
        });
      }
  </script>
  <script type="text/javascript">
	$(document).ready(function(){
    $('img.downloadable').each(function(){
        var $this = $(this);
        $this.wrap('<a href="' + $this.attr('src') + '" download />')
      });
  });
  </script>
  <script type="text/javascript">
  	 function show_physiotherapy_prescription_user_detail_modal(id) {
        $.ajax({
          url: base_url+"Physiotherapy/show_physiotherapy_prescription_user_detail_modal", 
          type : "POST",
          data: {id : id},
          success: function(result)
          {
            console.log(result);
              $('#modal_details').html(result);
              $('#modal_details').modal('show');
          }
        });
      }
    
   function apply_physiotherapy_price(id,price)
    {
    	update_price=price.value;
    $.ajax({
        url: base_url + "Physiotherapy/apply_physiotherapy_price", 
        type : "POST",
        data: {id:id, update_price:update_price}, 
        success: function(result)
        {
            if (result == 'Valid') 
            {
            	// alert('this is alert');
                //location.reload();
            }
        }
    });
}
function reload_page()
{
	location.reload();
}
  </script>
</body>
</html>