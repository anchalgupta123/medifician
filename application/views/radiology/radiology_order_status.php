<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
	<div class="wrapper">
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Update Status</h4>
								</div>
								<div class="card-body">
									<ul class="nav nav-pills nav-secondary nav-pills-no-bd" id="pills-tab-without-border" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" id="pills-home-tab-nobd" data-toggle="pill" href="#pills-home-nobd" role="tab" aria-controls="pills-home-nobd" aria-selected="true">Payment Status</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" id="pills-profile-tab-nobd" data-toggle="pill" href="#pills-profile-nobd" role="tab" aria-controls="pills-profile-nobd" aria-selected="false">Delivery Status</a>
										</li>
									</ul>
									<div class="tab-content mt-2 mb-3" id="pills-without-border-tabContent">
										<div class="tab-pane fade show active" id="pills-home-nobd" role="tabpanel" aria-labelledby="pills-home-tab-nobd">
										 <div class="form-group col-md-6">
											<select id="payment_status" class="form-control">
												<option value="1">Paid</option>
												
											</select>
											<input type="hidden"  id="payment_id" value="<?php echo $payment->id; ?>">
										</div>
										<button type="button" id="addRowButton" class="btn btn-primary" onclick="radiology_order_payment_status()" >Update</button>
										</div>
										<div class="tab-pane fade" id="pills-profile-nobd" role="tabpanel" aria-labelledby="pills-profile-tab-nobd">
										<div class="form-group col-md-6">
											<select id="delivery_status" class="form-control">
												<option value="1">Delivered</option>
												<option value="2">Cancelled</option>
												
											</select>
											<input type="hidden"  id="status_id" value="<?php echo $deliver->id; ?>">
										</div>
										<button type="button" id="addRowButton" class="btn btn-primary" onclick="radiology_order_deliver_status()" >Update</button>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php $this->load->view('bars/footer');?>
			<?php $this->load->view('bars/js');?>
		</div>
	</div>
</body>
</html>