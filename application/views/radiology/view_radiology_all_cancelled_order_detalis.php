<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
	<div class="wrapper">
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Order Details</h4>
								</div>
								<div class="card-body">
									<ul class="nav nav-pills nav-secondary nav-pills-no-bd" id="pills-tab-without-border" role="tablist">

										<li class="nav-item">
											<a class="nav-link active " id="pills-radiology-tab-nobd" data-toggle="pill" href="#pills-radiology-nobd" role="tab" aria-controls="pills-radiology-nobd" aria-selected="false"> Radiology Order</a>
										</li>

										<li class="nav-item">
											<a class="nav-link" id="pills-radiology-prescription-tab-nobd" data-toggle="pill" href="#pills-radiology-prescription-nobd" role="tab" aria-controls="pills-radiology-prescription-nobd" aria-selected="false"> Radiology Prescription Order</a>
										</li>

									</ul>
				<div class="tab-content mt-2 mb-3" id="pills-without-border-tabContent">
                    <div class="tab-pane fade show active" id="pills-radiology-nobd" role="tabpanel" aria-labelledby="pills-radiology-tab-nobd">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>UserName</th>
										<th>Test Name</th>
										<th>Lab Name</th>
										<th>Price</th>
										<th>Payment Type</th>
										<th>Payment Status</th>
										<th>Delivery Status</th>
										<th>Register Date</th>
										<th>Action</th>

									</tr>
								</thead>
							    <tbody>
									  <?php $x = 1; foreach ($deliver as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
										<td><a href="javascript:void(0);" style="text-decoration: none;color:#575269;" onclick="show_radiology_user_detail_modal(<?php echo $key->id; ?>);"><?php echo $key->name; ?></a></td>
										<td><?php echo $key->test_name; ?></td>
										<td><?php echo $key->lab_name; ?></td>
										<td><?php echo $key->price; ?></td>
										<td><?php 
										 if($key->txn_type=='0') 
                                          {
                                          	echo "Cash On Delivery";
                                          }
                                          if($key->txn_type=='1') 
                                          {
                                          	echo "Payment Gateway";
                                          }
										?></td>
										<td> 
										 <?php 
                                           if ($key->status =='0') 
                                           {
                                           	echo "Unpaid";
                                           }
                                          if($key->status=='1') 
                                          {
                                          	echo "Paid";
                                          } 
				                        ?>
										</td>
										<td>
										 <?php 
                                           if ($key->delivery_status =='0') 
                                           {
                                           	echo "Confirm Order";
                                           }
                                           if($key->delivery_status=='1') 
                                           {
                                          	echo "Delivered Order";
                                           } 
                                           if($key->delivery_status=='1') 
                                           {
                                          	echo "Cancelled Order";
                                           } 
				                        ?>
										</td>
										
				                        <td><?php echo change_date_format_dmy($key->created_date_time);?></td>
				                        <td><button class="btn btn-sm btn-danger" onclick="delete_radiology_cancelled_order('<?php echo $key->id; ?>');">Delete</button></td>

									</tr>
								    <?php } ?>
								</tbody>  
							</table>
						</div>
					</div>
                                        
					<div class="tab-pane fade" id="pills-radiology-prescription-nobd" role="tabpanel" aria-labelledby="pills-radiology-prescription-tab-nobd">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover" >
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>Name</th>
										<th>Address</th>
										<th>Prescription Image</th>
										<th>Description</th>
										<th>Pricing</th>
										<th>Delivery Status</th>
										<th>Register Date</th>
										<th>Action</th>
									</tr>
								</thead>
							    <tbody>
									<?php $x = 1; foreach ($radiology as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
				                        <td><a href="javascript:void(0);" style="color:#575962;" onclick="show_radiology_prescription_user_detail_modal('<?php echo $key->id;?>');"><?php echo $key->name;?></a></td>
				                        <td><?php echo $key->address; ?></td>
				                        <td><a href="" download><img src="<?php echo base_url();?>files/prescription_img/<?php echo $key->prescription_image;?>" width="70" height="70" class="downloadable"/></a>
				                        </td>
				                        <td><?php echo $key->problem; ?></td>
				                        <td><?php echo $key->price;?></td>
				                        </td>
				                        </form>  
				                          <td><?php
				                           if($key->status=='0')
				                           {
				                            echo "New Order";
				                           }
				                           if($key->status=='1')
				                           {
				                            echo "Pending Order";
				                           }
				                           if($key->status=='2')
				                           {
				                            echo "Confirm Order";
				                           }
				                           if($key->status=='3')
				                           {
				                            echo "Delivered Order";
				                           }
				                           if($key->status=='4')
				                           {
				                            echo "Cancelled Order";
				                           }
				                          ?> 
				                        </td>
				                        <td><?php echo change_date_format_dmy($key->created_date_time);?></td>
				                        <td><button class="btn btn-sm btn-danger" onclick="delete_radiology_cancelled_prescription('<?php echo $key->id; ?>');">Delete</button></td>
									</tr>
								    <?php } ?>
								</tbody> 
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
			<?php $this->load->view('bars/footer');?>
			<?php $this->load->view('bars/js');?>
<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
    </div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
		$(document).ready(function() {
			$('#multi-filter-select2').DataTable({
			});
			
		});
	</script>
	<script type="text/javascript">
		function update_radiology_cod_status_modal(id) {
        $.ajax({
          url: base_url+"Radiology/update_radiology_cod_status_modal", 
          type : "POST",
          data :{id : id},
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
	<script type="text/javascript">
		function update_radiology_report_modal(id) {
        $.ajax({
          url: base_url+"Radiology/update_radiology_report_modal", 
          type : "POST",
          data :{id : id},
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
	<script type="text/javascript">
  	 function show_radiology_user_detail_modal(id) {
        $.ajax({
          url: base_url+"Radiology/show_radiology_user_detail_modal", 
          type : "POST",
          data: {id : id},
          success: function(result)
          {
            console.log(result);
              $('#modal_details').html(result);
              $('#modal_details').modal('show');
          }
        });
      }
  </script>
  <script type="text/javascript">
		function show_radiology_prescription_user_detail_modal(id) {
        $.ajax({
          url: base_url+"Radiology/show_radiology_prescription_user_detail_modal", 
          type : "POST",
          data: {id : id},
          success: function(result)
          {
            console.log(result);
              $('#modal_details').html(result);
              $('#modal_details').modal('show');
          }
        });
      }
    
	</script>
	<script type="text/javascript">
	$(document).ready(function(){
    $('img.downloadable').each(function(){
        var $this = $(this);
        $this.wrap('<a href="' + $this.attr('src') + '" download />')
      });
  });
  </script>
</body>
</html>