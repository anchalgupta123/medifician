<!DOCTYPE html>
<html>
<head>
	
	<?php $this->load->view('bars/head');?>
</head>
<body>
	<div class="wrapper">
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h2 class="card-title">View Radiology Order Details</h2>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover">
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>UserName</th>
										<th>Test Name</th>
										<th>Lab Name</th>
										<th>Price</th>
										<th>Payment Type</th>
										<th>Payment Status</th>
										<th>Delivery Status</th>
										<th>Register Date</th>
										<th>Upload Report</th>
									</tr>
								</thead>
							    <tbody>
									  <?php $x = 1; foreach ($order as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
										<td><a href="javascript:void(0);" style="text-decoration: none;color:#575269;" onclick="show_radiology_user_detail_modal(<?php echo $key->id; ?>);"><?php echo $key->name; ?></a></td>
										<td><?php echo $key->test_name; ?></td>
										<td><?php echo $key->lab_name; ?></td>
										<td><?php echo $key->price; ?></td>
										<td><?php 
										 if($key->txn_type=='0') 
                                          {
                                          	echo "Cash On Delivery";
                                          }
                                          if($key->txn_type=='1') 
                                          {
                                          	echo "Payment Gateway";
                                          }
										?></td>
										<td> 
										 <?php 
                                           if ($key->status =='0') 
                                           {
                                           	echo "Unpaid";
                                           }
                                          if($key->status=='1') 
                                          {
                                          	echo "Paid";
                                          } 
				                        ?>
										</td>
										<td>
											<a href="<?php echo base_url();?>Radiology/radiology_order_status?id=<?php echo $key->id;?>"><button type="button" style="font-weight: bold;margin-top: 5PX;" class="btn btn-primary btn-sm">UPDATE STATUS</button></a>
										</td>
										
				                        <td><?php echo change_date_format_dmy($key->created_date_time);?></td>
				                        <td><button type="button" class="btn btn-info btn-sm" style="font-weight: bold;" onclick="update_radiology_report_modal('<?php echo $key->id; ?>')">UPLOAD REPORT</button>
				                        	<br><a href="<?php echo base_url();?>Radiology/radiology_check_out_details?user_id=<?php echo $key->id;?>"><button type="button" style="font-weight: bold;margin-top: 5PX;" class="btn btn-primary btn-sm">CHECK OUT</button></a></td>

									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
           <?php $this->load->view('bars/js');?>
</div>
</div>
	<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
    </div>
    <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
	</script>
	<script type="text/javascript">
		function update_radiology_cod_status_modal(id) {
        $.ajax({
          url: base_url+"Radiology/update_radiology_cod_status_modal", 
          type : "POST",
          data :{id : id},
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
	<script type="text/javascript">
		function update_radiology_report_modal(id) {
        $.ajax({
          url: base_url+"Radiology/update_radiology_report_modal", 
          type : "POST",
          data :{id : id},
          success: function(result)
          {
            $('#modal_details').html(result);
            $('#modal_details').modal('show');
          }
        });
      }
	</script>
	<script type="text/javascript">
  	 function show_radiology_user_detail_modal(id) {
        $.ajax({
          url: base_url+"Radiology/show_radiology_user_detail_modal", 
          type : "POST",
          data: {id : id},
          success: function(result)
          {
            console.log(result);
              $('#modal_details').html(result);
              $('#modal_details').modal('show');
          }
        });
      }
  </script>

</body>
</html>