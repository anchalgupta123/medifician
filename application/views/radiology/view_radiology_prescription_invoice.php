<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	<style type="text/css">
		/*h5{
			font-weight: bold!important;
		}*/
	</style>	
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
		    	<h1><center>INVOICE</center></h1>
		    	<a class="btn btn-primary" href="<?php echo base_url();?>Radiology/print_radiology_prescription_invoice_pdf?user_id=<?php echo $user_details->id;?>" style="float:right;margin-left: -10%;margin-top: -5%;"><i class="fa fa-print"></i> Print</a>
				<div class="card">
					<div class="card-header">
					   <div class="d-flex align-center">
                         <img style="height: 140px;width: 290px;" class="offset-4" src="<?php echo base_url();?>files/logo.png">
					   </div>
					</div>
					<div class="card-body" style="padding-left: 35px;padding-top: 60px;">
						<div class="row">
							<div class="col-md-9">
								<h5>Date : <?php echo change_date_format_dmy($user_details->created_date_time);?></h5>
								<h5>GSTIN No. :</h5>
							</div>
							<div>
								<h5>Invoice No. :</h5>
							</div>	
						</div>
						<hr style="border-color:#404450eb!important;">
						<div class="row">
							<div class="col-md-6">
								<h5>Name :&nbsp;&nbsp; <?php echo $user_details->name;?></h5>
								<h5>Mobile No. :&nbsp;&nbsp; <?php echo $user_details->mobile_no;?> </h5>
								<h5>Address :&nbsp;&nbsp; <?php echo $user_details->address;?></h5>
							</div>
							<div class="col-md-5" style="padding-left: 28%;">
								<h4>BALANCE DUE <br><small style="padding-left: 50px;">On Receipt</small></h4>
								<p style="font-size: 20px;padding-left: 50px;" class="text-center"><?php echo $user_details->price;?>/-</p>
							</div>
						</div>
                        <div class="row">
                          <div class="table-responsive">
                        	<table id="multi-filter-select" class="display table table-striped table-hover">
                        		<tbody style="text-align: right;">
                        			<tr>
                        				<th>Sr. No.</th>
                        				<th>Item</th>
                        				<th>Qty</th>
                        				<th>Price</th>
                        			</tr>
                        		      <?php $x = 1; foreach ($medicine_details as $key){ ?>
                        			 	<tr>
                        			 		<td><?php echo $x++;?></td>
                        			 		<td><?php echo $key->medicine_name;?></td>
                        			 		<td><?php echo $key->qty;?></td>
                        			 		<td><?php echo $key->medicine_price;?></td>
                        			 	</tr>
                        			  <?php } ?>
                        			<tr>
                        				<th></th>
                        				<th></th>
                        				<th style="text-align:right;font-size:16px;">Subtotal </th>
                                        <th><?php echo $user_details->price;?></th>
                        			</tr>
                        			<tr>
                        				<th></th>
                                        <th></th>
                        				<th style="font-size:16px;text-align: right;">TOTAL </th>
                                        <th style="font-size: 17px;text-align: right;"><?php echo $user_details->price;?>/- Rs.</th></tr>
                        			</tr>
                        		</tbody>
                        	</table>
                          </div>
                      </div>
	                	<div class="col-md-12" style="margin-top: 30px;">
	                		<h3 style="float: right;">FOR : MEDIFICIANS</h3>
	                	</div>
	                	<div>
	                        <br>
	                		<hr style="border-color:#404450eb!important;">
	                		<h5 style="font-size:16px;">Address : H 302, Swadesh Bhawan, A.B. Road, Indore - 452010</h5>
	                		<h5 style="font-size:16px;">Email : support@medificians.com</h5>
	                		<h5 style="font-size:16px;">Mobile No. :</h5>
	                	</div>
					</div>
				</div>
		    </div>
		 </div>
	  </div>
        <?php $this->load->view('bars/footer');?>
        <?php $this->load->view('bars/js');?>
    </div>
</div>
 <script>
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});
			
		});
	</script>
	 <script type="text/javascript">
      function printData()
      {
         var divToPrint=document.getElementById("printarea");
         newWin= window.open("");
         newWin.document.write(divToPrint.outerHTML);
         newWin.print();
         newWin.close();
      }

      $('.btn').on('load',function(){
      printData();
      })
    </script>
</body>
</html>