<!DOCTYPE html>
<html>
<head>
  <style type="text/css">
    .lab label{
      font-size: 16px!important;
    }
    input[type="text"]:disabled {
    color: black!important;
    }
    #price_row1 td input {
      height: 40px!important;
    }
    .type_box{
      width: 100px;
      /*text-align: center;*/
    }
    .type_box hr{
      margin: 5px 0px 5px 0px;
      border-color: #80808070!important;
    }
    .type_box hr:last-child{
      display: none;
    }
  </style>
  <?php $this->load->view('bars/head');?>
</head>
<body>
  <div class="wrapper">
  
    <?php $this->load->view('bars/header');?>
    <?php $this->load->view('bars/side_bar');?>
  
   <div class="main-panel">
    <div class="content">
     <div class="page-inner">
      <div class="col-md-12">
        <div class="card">
                  <div class="card-header">
          <div class="d-flex align-items-center">
            <h4 class="card-title">User Order Details</h4>
          </div>
        </div>
      <div class="card-body">
        <br />
        <form name="myform" id="myform">
            <div class="row lab">
              <div class="col-md-6 ">
                <label>Name <span class="required">*</span></label>
                <div class="form-group " style="padding-bottom: 15px;">
                   <input type="text" id="name" required="required" class="form-control" required style="height: 50px;" value="<?php echo $check_out->name;?>" disabled>
                </div>
                <label>Email <span class="required">*</span></label>
                <div class="form-group " style="padding-bottom: 15px;">
                   <input type="text" id="test_name" required="required" class="form-control" required style="height: 50px;"  value="<?php echo $check_out->email;?>" disabled >
                </div>
                <label>Mobile No. <span class="required">*</span></label>
                <div class="form-group " style="padding-bottom: 15px;">
                   <input type="text" id="test_name" required="required" class="form-control" required style="height: 50px;"  value="<?php echo $check_out->mobile_no;?>" disabled >
                </div>
                <label>Address <span class="required">*</span></label>
                <div class="form-group " style="padding-bottom: 15px;">
                   <input type="text" id="test_name" required="required" class="form-control" required style="height: 50px;"  value="<?php echo $check_out->address;?>" disabled >
                </div>
              </div>
              <div class="col-md-6 ">
                <label>Prescription Image <span class="required">*</span></label>
                <div class="form-group " style="padding-bottom: 15px;">
                   <img src="<?php echo base_url();?>files/prescription_img/<?php echo $check_out->prescription_image;?>" width="100%" height="100%">
                </div>
              </div>
            </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                      <tbody id="price_tbl">
                      <tr id="price_row1">
                        <td><input type="text" size="15" name="medicine_name" style="width: 100%;" class="medicine_name form-control" placeholder="Item Name"></td>
                        <td><input type="text" size="5" name="qty" style="width: 100%;" placeholder="Quantity" class="qty form-control"></td>
                        <td><input type="text" size="5" name="medicine_price" style="width: 100%;" placeholder="Item Price" class="medicine_price form-control"></td>
                        <td><button class="btn btn-danger btn-xs" type="button" onclick="remove_row('price_row1');"><i class="fa fa-minus"></i></button></td>
                      </tr>
                         </tbody>
                         
                         <button class="btn btn-primary btn-sm" type="button" onclick="add_row_price();"><i class="fa fa-plus"></i> Add Medicine</button>
                    </table>
                  </div>
                </div>
              </div>
                <div class="row lab">
                  <div class="col-md-7 ">
                    <label>Total Amount<span class="required">*</span></label>
                    <div class="form-group " style="padding-bottom: 15px;">
                       <input type="text" id="price" required="required" value="<?php echo $check_out->price;?>" class="form-control" required style="height: 50px;">
                    </div>
                  </div>
                </div>
              <div class="row">
              <input type="hidden" id="order_id" value="<?php echo $check_out->id;?>" >
              <button type="button" class="btn btn-primary" id="btn_submit" onclick="add_radiology_medicine_price();" style="margin-left: 50%;" >Submit</button>
           </div>
          </form>
          </div>
        </div>


        <div class="card">
              <div class="card-header">
                <div class="d-flex align-items-center">
                  <h4 class="card-title">View Details </h4>
                </div>
              </div>
              <div class="card-body">
               <div class="table-responsive">
              <table id="multi-filter-select" class="display table table-striped table-hover">
                <thead>
                  <tr>
                    <th>Sr.no</th>
                    <th>Name</th>
                    <th>Item Name</th>
                    <th>Item Price</th>
                    <th>Total Amount</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                    <?php $x = 1; foreach ($get_check_out as $key){ ?>
                  <tr>
                    <td><?php echo $x++; ?></td>
                    <td><?php echo $key->name;?></td>
                    <td class="type_box">
                      <?php if ($key->medicine_name) {
                      $medicine_name =explode(',',$key->medicine_name);
                      foreach ($medicine_name as $ty => $value) {
                        echo $value ."<hr>";
                      } }?>
                    </td>
                    <td class="type_box">
                      <?php if ($key->medicine_price) {
                      $medicine_price =explode(',',$key->medicine_price);
                      foreach ($medicine_price as $ty => $value) {
                        echo $value ."<hr>";
                      } }?>
                    </td>
                    <td><?php echo $key->price; ?></td>
                    <td><a target="_blanck" href="<?php echo base_url();?>Radiology/open_radiology_prescription_invoice?user_id=<?php echo $key->id;?>"><button type="button" style="font-weight: bold;margin-top: 5PX;" class="btn btn-primary btn-sm">GENERATE INVOICE</button></a></td>      
                  </tr>
                    <?php } ?>
                </tbody> 
              </table>
            </div>
          </div>
        </div>
     </div>
  </div>
</div>
</div>
         <?php $this->load->view('bars/footer');?>
</div>
</div>
<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
</div>
    <?php $this->load->view('bars/js');?>
    <script>
    $(document).ready(function() {
      $('#multi-filter-select').DataTable({
      });
      
    });
  </script>
  <script type="text/javascript">
      temp = 1;
      function add_row_price()
      {
        temp++;
        $('#price_tbl').append('<tr id="price_row'+temp+'"><td><input type="text" size="15" name="" style="width: 100%;height:40px!important;" placeholder="Item Name" class="medicine_name form-control"></td><td><input type="text" size="5" name="qty" style="width: 100%;height:40px!important;" placeholder="Quantity" class="qty form-control"></td><td><input type="text" size="5" name="" placeholder="Item Price" style="width: 100%;height:40px!important;" class="medicine_price form-control"></td><td><button class="btn btn-danger btn-xs" type="button"  onclick="remove_row(&#39;price_row'+temp+'&#39;);"><i class="fa fa-minus"></i></button></td></tr>');
      }
      function remove_row(id)
      {
        $('#'+id).remove();
      }
      
  </script>
  <script type="text/javascript">
    function add_gst_calculation() {
      
      var sub_total_amount = parseFloat($('#price').val());
      var gst_percent_amount = parseFloat($('#gst_percent').val());

      var gst_amount=(gst_percent_amount/100)*sub_total_amount;

      var total_calculate_add_gst=sub_total_amount +gst_amount;
      $('#gst_amount').val(Math.round(gst_amount));
      $('#total_amount').val(Math.round(total_calculate_add_gst));

    }
  </script>
 
</body>
</html>