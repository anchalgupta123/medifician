<!DOCTYPE html>
<html>
<head>
  
  <?php $this->load->view('bars/head');?>
</head>
<body>
  <div class="wrapper">
  
    <?php $this->load->view('bars/header');?>
    <?php $this->load->view('bars/side_bar');?>
  
   <div class="main-panel">
    <div class="content">
     <div class="page-inner">
        <div class="col-md-12">
        <div class="card">
                  <div class="card-header">
          <div class="d-flex align-items-center">
            <h4 class="card-title">Add On Screen Notification</h4>
          </div>
        </div>
      <div class="card-body">
        <br />
        <form>
            <div class="row">
              <div class="col-md-6 ">
                <label>Start Date<span class="required">*</span></label>
                <div class="form-group form-group-default" style="padding-bottom: 15px;">
                   <input type="date" id="start_date" required="required" class="form-control" required style="height: 50px;">
                </div>
              </div>
              <div class="col-md-6">
                <label>End Date<span class="required">*</span></label>
                <div class="form-group form-group-default" style="padding-bottom: 15px;">
                   <input type="date" id="end_date" required="required" class="form-control" required style="height: 50px;">
                </div>
              </div>
              <div class="col-md-12">
                <label>Notification<span class="required">*</span></label>
                <div class="form-group form-group-default">
                   <textarea type="text" id="notification" required="required" class="form-control" required style="height: 100px;"></textarea>
                  <input type="hidden" id="notification_id" name="">
                </div>
              </div>
            </div>
            <button type="button" class="btn btn-primary" id="btn_submit" onclick="save_user_onscreen_notification();" style="margin-left: 50%;" >Submit</button>
          </form>
          </div>
         </div>
    <div class="card">
        <div class="card-header">
          <div class="d-flex align-items-center">
            <h4 class="card-title">View On Screen Notification </h4>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="multi-filter-select" class="display table table-striped table-hover">
                  <thead>
                        <tr>
                          <th>Sr. No.</th>
                          <th>Start Date</th>
                          <th>End Date</th>
                          <th>Notification</th>
                          <th>Notification Date/Time</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $x = 1; foreach ($notify as $key){ ?>
                        <tr>
                          <td><?php echo $x++; ?></td>
                          <td><?php echo change_date_format_dmy($key->start_date); ?></td>
                          <td><?php echo change_date_format_dmy($key->end_date); ?></td>
                          <td><?php echo $key->notification; ?></td>
                          <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
                          <td><button class="btn btn-sm btn-danger" onclick="delete_onscreen_notification('<?php echo $key->id; ?>');">Delete</button></td>
                        </tr>
                        <?php } ?>
                      </tbody>
            </table>
          </div>
        </div>
      </div>
     </div>
    </div>
  </div>
         <?php $this->load->view('bars/footer');?>
</div>
</div>
    <?php $this->load->view('bars/js');?>
    <script>
    $(document).ready(function() {
      $('#multi-filter-select').DataTable({
      });
      
    });
  </script>
</body>
</html>