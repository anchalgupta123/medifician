<!DOCTYPE html>
<html>
<head>
  
  <?php $this->load->view('bars/head');?>
</head>
<body>
  <div class="wrapper">
  
    <?php $this->load->view('bars/header');?>
    <?php $this->load->view('bars/side_bar');?>
   <div class="main-panel">
    <div class="content">
     <div class="page-inner">
        <div class="col-md-12">
           <button class="btn btn-warning" onclick="open_message_modal();">Send Notification</button><br><br>
        <div class="card">
          <div class="card-header">
            <div class="d-flex align-items-center">
              <h4 class="card-title">View User Details </h4>
            </div>
          </div>
          <div class="card-body">
            <label><input type="checkbox" name="" id="select_all">Select All</label>
            <div class="table-responsive">
              <table id="multi-filter-select" class="display table table-striped table-hover">
                <thead>
                  <tr>
                    <th>Sr.no</th>
                    <th>Check</th>
                    <th>Name</th>
                    <th>Date_of_Birth</th>
                    <th>Email</th>
                    <th>Mobile_no</th>
                    <th>Address</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Pincode</th>
                    <th>Register Date</th>
                  </tr>
                </thead>
                  <tbody>
                  <?php $x = 1; foreach ($users as $key){ ?>
                  <tr>
                      <td><?php echo $x++; ?></td>
                      <td><input type="checkbox" name="" class="user_ids" value="<?php echo $key->id;?>"></td>
                      <td><?php echo $key->name; ?></td>
                      <td><?php echo $key->date_of_birth; ?></td>
                      <td><?php echo $key->email; ?></td>
                      <td><?php echo $key->mobile_no; ?></td>
                      <td><?php echo $key->address; ?></td>
                      <td><?php echo $key->city; ?></td>
                      <td><?php echo $key->state; ?></td>
                      <td><?php echo $key->pincode; ?></td>
                      <td><?php echo change_date_format_dmy($key->created_date_time); ?></td>
                  </tr>
                    <?php } ?>
                </tbody> 
              </table>
            </div>
          </div>
        </div>
        </div>
    </div>
  </div>
         <?php $this->load->view('bars/footer');?>
</div>
</div>
<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
</div>
    <?php $this->load->view('bars/js');?>
    <script>
    $(document).ready(function() {
      $('#multi-filter-select').DataTable({
      });
      
    });
  function open_message_modal(id)
  {
    user_ids = '';
    $( ".user_ids" ).each(function( index ) {
        if ($(this).prop("checked") == true) 
        {
            user_ids = user_ids+ ','+ $(this).val();
        }
    });

    user_ids = user_ids.replace(/^,/, '');
    if (user_ids == '') 
    {
      alert('Please select any user for send message!');
      return false;
    }
    else{
      $.ajax({
        url: base_url+"Users/get_message_for_notification_modal", 
        type : "POST",
        data: {id : id},
        success: function(result)
        {
          console.log(result);
          $('#modal_details').html(result);
          $('#modal_details').modal('show');
        }
      });
    }
  }
  $("#select_all").change(function(){
    if ($(this).prop("checked") == true)
    {
      $('.user_ids').prop('checked', true);
    }
    else
    {
      $('.user_ids').prop('checked', false);

    }
  });
  </script>
</body>
</html>