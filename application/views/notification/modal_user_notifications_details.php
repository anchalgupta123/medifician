<div class="modal-dialog">
    <div class="modal-content">
        <form id="form_message">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Notification For All Users</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Notifications * </label>
                    <textarea class="form-control" id="notification"></textarea>
                </div>
               <!--  <input type="hidden" id="user_id"> -->
            </div>
            <div class="modal-footer">
                <button type="sumbit" class="btn btn-primary" onclick="save_notifications_users();">Send</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>

<!-- <script type="text/javascript">
    $( "#form_message" ).submit(function( event ) {
        send_message_to_all();
        // alert();
        event.preventDefault();
    });
</script> -->
  
  <script type="text/javascript">
  function save_notifications_users()
  {   
     user_ids = '';
    $( ".user_ids" ).each(function( index ) {
        if ($(this).prop("checked") == true) 
        {
            user_ids = user_ids+ ','+ $(this).val();
        }
    });
    user_ids = user_ids.replace(/^,/, '');
    notification = $('#notification').val();

    var formData = new FormData();
    formData.append('notification',notification);
    formData.append('user_ids',user_ids);
    if (user_ids == '') 
    {
      alert('Please select any user for send message!');
      return false;
    }
    else{
      $.ajax({
        url : base_url+"Users/save_bell_notification",
        type : "POST",
        data : formData,
        processData:false,
        contentType:false,
        success:function(result)
        {
            if (result == 'Valid') 
            {
                alert('One to many message send!');
               /* $('#modal_details').modal('hide');*/
                location.reload();
            }
        }
    });
  }
}
  </script>