<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('bars/head');?>
</head>
<body>
<div class="wrapper">
	
		<?php $this->load->view('bars/header');?>
		<?php $this->load->view('bars/side_bar');?>
	
   <div class="main-panel">
	  <div class="content">
		 <div class="page-inner">
		    <div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<div class="d-flex align-items-center">
							<h4 class="card-title">View Confirm Order Details</h4>
						</div>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="multi-filter-select" class="display table table-striped table-hover" >
								<thead>
									<tr>
										<th>Sr.no</th>
										<th>Name</th>
										<th>Address</th>
										<th>Image</th>
										<th>Pricing</th>
										<th>Delivery Status</th>
										<th>Register Date</th>
										<th>Action</th>
									</tr>
								</thead>
							    <tbody>
									<?php $x = 1; foreach ($order as $key){ ?>
									<tr>
										<td><?php echo $x++; ?></td>
				                        <td><a href="javascript:void(0);" style="color:#575962;" onclick="show_pharmacy_user_detail_modal('<?php echo $key->id;?>');"><?php echo $key->name;?></a></td>
				                        <td><?php echo $key->address; ?></td>
				                        <td><a href="" download><img src="<?php echo base_url();?>files/prescription_img/<?php echo $key->image;?>" width="70" height="70" class="downloadable"/></a>
				                        </td>
				                        <td><?php echo $key->pricing;?></td>
				                          <td><?php
				                           if($key->status==2)
				                           {
				                            echo "Confirm Order";
				                           }
				                           if($key->status==3)
				                           {
				                            echo "Delivered Order";
				                           }
				                           if($key->status==4)
				                           {
				                            echo "Canceled Order";
				                           }
				                          ?> 
				                        </td>
				                        <td><?php echo change_date_format_dmy($key->created_date_time);?></td>
				                        <td><button class="btn btn-sm btn-danger" onclick="delete_canceled_order('<?php echo $key->id; ?>');">Delete</button></td>
									</tr>
								    <?php } ?>
								</tbody> 
							</table>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
         <?php $this->load->view('bars/footer');?>
</div>
</div>
<div class="modal" id="modal_details"  tabindex="-1" role="dialog" aria-hidden="true">
      
    </div>
    <?php $this->load->view('bars/js');?>
    <script >
		$(document).ready(function() {
			$('#multi-filter-select').DataTable({
			});

		});
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
	    $('img.downloadable').each(function(){
	        var $this = $(this);
	        $this.wrap('<a href="' + $this.attr('src') + '" download />')
	      });
	  });
  </script>
   <script type="text/javascript">
  	 function show_pharmacy_user_detail_modal(id) {
        $.ajax({
          url: base_url+"Pharmacy/show_pharmacy_user_detail_modal", 
          type : "POST",
          data: {id : id},
          success: function(result)
          {
            console.log(result);
              $('#modal_details').html(result);
              $('#modal_details').modal('show');
          }
        });
      }    
</script>
</body>
</html>