	<style type="text/css">
		.lab{
			color: black!important;
			font-weight: bold;
			padding-left: 20px;
			font-size: 16px!important;
		}
		.form-control{
		  background-color: #404450eb!important;
		  color: #fff;
          border-color: #404450eb!important;
          padding-bottom: 10px;
          font-size: 16px!important;
		}
	</style>
	<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">
						<span class="fw-mediumbold">
						User Notification</span> 
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form class="form-horizontal">
				<div class="form-group row">
					<label class="col-md-12 col-sm-12 col-xs-12" for="first-name">Name:</label>
	                <div class="col-md-12 col-sm-12 col-xs-12">
	                  <textarea type="text" id="notification" class="form-control"></textarea>
	                </div>
	                <input type="hidden" name="" id="user_id" value="<?php echo $user->user_id;?>">
				</div>
			</form>
				</div>
				<div class="modal-footer no-bd">
					<button type="button" onclick="save_notifications_users();" class="btn btn-primary" data-dismiss="modal">Submit</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
				<div class="modal-footer no-bd">
					
				</div>
			</div>
 </div>
 <script type="text/javascript">
    function save_notifications_users()
  {   
    user_id = $('#user_id').val();
    notification = $('#notification').val();
   
    var formData = new FormData();
    formData.append('user_id',user_id);
    formData.append('notification',notification);
    $.ajax({
        url : base_url+"Pharmacy/save_bell_notification",
        type : "POST",
        data : formData,
        processData:false,
        contentType:false,
        success:function(result)
        {
        	 $('#preloader').hide(); 
            if (result == 'Valid') 
            {
                alert('Notification Added successfully');
                location.reload();
            }
        }
    });
}
  </script>
