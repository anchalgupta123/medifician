<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view('bars/head');?>
</head>
<body>
    <div class="content">
     <div class="page-inner">
       <div class="col-md-12">
        <div class="card">
          <div class="card-header">
             <div class="d-flex align-center">
                <img style="height: 140px;width: 290px;" class="offset-5" src="<?php echo base_url();?>files/logo.png">
             </div>
          </div>
          <div class="card-body" style="padding-left: 35px;padding-top: 60px;">
            <div class="row">
              <div class="col-md-9">
                <h5>Date : <?php echo change_date_format_dmy($user_details->created_date_time);?></h5>
                <h5>GSTIN No. :</h5>
              </div>
              <div>
                <h5>Invoice No. : <?php echo $invoice_count; ?></h5>
              </div>  
            </div>
            <hr style="border-color:#404450eb!important;">
            <div class="row">
              <div class="col-md-9">
                <h5>Authorised Vendor : <?php echo $user_details->company_name;?> </h5>
              </div>
              <div>
                <h5>Licence No. :</h5>
              </div>  
            </div>
            <hr style="border-color:#404450eb!important;">
            <div class="row">
              <div class="col-md-6">
                <h5>Name :&nbsp;&nbsp; <?php echo $user_details->name;?></h5>
                <h5>Mobile No. :&nbsp;&nbsp; <?php echo $user_details->mobile_no;?> </h5>
                <h5>Address :&nbsp;&nbsp; <?php echo $user_details->address;?></h5>
              </div>
              <div class="col-md-5" style="padding-left: 28%;">
                <h4>BALANCE DUE <br><small style="padding-left: 50px;">On Receipt</small></h4>
                <p style="font-size: 20px;padding-left: 75px;" class="text-center"><?php echo $user_details->total_amount;?>/-</p>
              </div>
            </div>
            <div class="row">
              <div class="table-responsive">
              <table id="multi-filter-select" class="display table table-striped table-hover">
                <tbody style="text-align: right;">
                  <tr>
                    <th>Sr. No.</th>
                    <th>Description</th>
                    <th>Qty</th>
                    <th>GST Price</th>
                    <th>Price</th>
                  </tr>
                      <?php $x = 1; foreach ($medicine_details as $key){ ?>
                    <tr>
                      <td><?php echo $x++;?></td>
                      <td><?php echo $key->medicine_name;?></td>
                      <td><?php echo $key->qty;?></td>
                      <td><?php echo $key->medicine_gst_price;?></td>
                      <td><?php echo $key->medicine_price;?></td>
                    </tr>
                    <?php } ?>
                  <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th style="text-align:right;font-size:16px;">Subtotal </th>
                    <th><?php echo $user_details->pricing;?></th>
                  </tr>
                  <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th style="font-size:16px;text-align: right;">TOTAL </th>
                    <th style="font-size: 17px;text-align: right;"><?php echo $user_details->total_amount;?>/- Rs.</th></tr>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
              <div class="col-md-12" style="margin-top: 30px;">
                <h3 style="float: right;">FOR : MEDIFICIANS</h3>
              </div>
              <div>
                <br>
                <hr style="border-color:#404450eb!important;">
                <h5 style="font-size:16px;">Address : H 302, Swadesh Bhawan, A.B. Road, Indore - 452010</h5>
                <h5 style="font-size:16px;">Email : support@medificians.com</h5>
                <h5 style="font-size:16px;">Mobile No. :</h5>
              </div>
          </div>
      </div>
    </div>
        <?php $this->load->view('bars/js');?>
 <script>
    $(document).ready(function() {
      $('#multi-filter-select').DataTable({
      });
      
    });
  </script>
   <script type="text/javascript">
      function printData()
      {
         var divToPrint=document.getElementById("printarea");
         newWin= window.open("");
         newWin.document.write(divToPrint.outerHTML);
         newWin.print();
         newWin.close();
      }

      $('.btn').on('load',function(){
      printData();
      })
    </script>
</body>
</html>