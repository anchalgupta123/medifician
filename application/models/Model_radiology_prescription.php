<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_radiology_prescription extends CI_Model {

	public $table ='radiology_prescription';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function count_radiology_prescription_today_sell($start_date,$end_date)
    {
        $sql = "SELECT count(id) as count ,SUM(total_amount) as total_amount FROM `radiology_prescription` WHERE date(created_date_time) >='$start_date' AND date(created_date_time) <= '$end_date' AND status='2';";
        $query = $this->db->query($sql);
        return $query->row();
    }
	public function count_total_radiology_prescription_orders()
	{
		$sql = "SELECT count(id) as count FROM radiology_prescription WHERE status='2'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function get_all_radiology_prescription_details()
	{
		$sql = "SELECT RP.* FROM radiology_prescription RP WHERE status='0' or status='1'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_radiology_prescription_of_perticuler_order($id)
	{
		$sql = "SELECT RP.*,UM.name,UM.mobile_no FROM radiology_prescription RP,user_master UM where RP.user_id = UM.id AND RP.user_id='$id'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function update_radiology_prescription_pricing($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_radiology_prescription_user_details($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
 	}
 	public function get_all_radiology_prescription_confirm_details()
	{
		$sql = "SELECT PP.* FROM radiology_prescription PP WHERE status='2'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_radiology_prescription_delivered_details()
	{
		$sql = "SELECT PP.* FROM radiology_prescription PP WHERE status='3'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_radiology_prescription_cancelled_details()
	{
		$sql = "SELECT PP.* FROM radiology_prescription PP WHERE status='4'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_radiology_prescription_check_out_details($id)
	{
		 $sql = "SELECT * from radiology_prescription Where id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}	
	public function update_radiology_total_price($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_radiology_prescription_all_check_out_details($id)
	{
		 $sql = "SELECT p.*,(SELECT  GROUP_CONCAT(pmd.medicine_name SEPARATOR ',') FROM radiology_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_name,(SELECT  GROUP_CONCAT(pmd.medicine_price SEPARATOR ',') FROM radiology_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_price FROM radiology_prescription p WHERE p.id = '$id'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_radiology_prescription_invoice_details($id)
	{
		$sql = "SELECT p.*,(SELECT  GROUP_CONCAT(pmd.medicine_name SEPARATOR ',') FROM radiology_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_name,(SELECT  GROUP_CONCAT(pmd.medicine_price SEPARATOR ',') FROM radiology_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_price FROM radiology_prescription p WHERE p.id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_radiology_prescription_medicine_details($id)
	{
		$sql = "SELECT * from radiology_medicine_data WHERE prescription_id='$id'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function delete_cancel_radiology_prescription($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
    public function get_radiology_delivered_status_details($id)
	{
		$sql = "SELECT * FROM radiology_prescription where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_radiology_delivered_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_radiology_payment_status_details($id)
	{
		 $sql = "SELECT * FROM radiology_prescription where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_radiology_payment_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	




 }
	