<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_user_otp extends CI_Model {

	public $table = 'user_otp';

	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function check_mobile_otp($mobile)
	{
		$sql ="SELECT * FROM `user_otp` WHERE mobile_no ='$mobile'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function get_user_details($mobile)
	{
		$sql = "SELECT * FROM $this->table WHERE mobile = '$mobile'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function insert_otp($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update_otp($data,$mobile)
	{
		$this->db->where('mobile_no', $mobile);
		$this->db->update($this->table, $data);
		return $mobile;
	}
	public function verify_user_otp_details($mobile,$otp)
	{
		$sql = "SELECT * FROM $this->table WHERE mobile_no = '$mobile' and otp = '$otp'";
		$res = $this->db->query($sql);
		return $res->row();
	}
}

/* End of file Model_user_otp.php */
/* Location: ./application/models/Model_user_otp.php */