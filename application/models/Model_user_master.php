<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_user_master extends CI_Model {

	public $table ='user_master';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function check_mobile_details($mobile)
	{
		$sql ="SELECT * FROM `user_master` WHERE mobile_no = '$mobile'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function insert_customer($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	public function get_customer_by_id($id)
	{
		$sql ="SELECT * FROM `user_master` WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function update_user_profile($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_all_users_details()
	{
		$sql = "SELECT * FROM $this->table";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_users_order_details($id)
	{
		/*$sql = "SELECT PO.*,PL.lab_name,PT.test_name,PT.description,UM.name FROM pathology_order PO,pathology_labs PL,pathology_test PT,user_master UM WHERE PO.test_id = PT.id AND PL.id = PO.lab_id AND PO.user_id = UM.id AND PO.user_id = '$id'";*/
		$sql ="SELECT PO.*,PL.lab_name,PT.test_name,PT.description,UM.name FROM pathology_order PO,pathology_labs PL,pathology_test PT,user_master UM WHERE PO.test_id = PT.id AND PL.id = PO.lab_id AND PO.user_id = UM.id AND PO.user_id = '$id' UNION SELECT RO.*,RL.lab_name,RT.test_name,RT.description,UM.name FROM radiology_order RO,radiology_labs RL,radiology_test RT,user_master UM WHERE RO.test_id = RT.id AND RL.id=RO.lab_id AND RO.user_id = UM.id AND RO.user_id = '$id' UNION SELECT PA.*,PL.lab_name,PT.test_name,PP.price,UM.name FROM physiotherapy_order PA,physiotherapy_labs PL,physiotherapy_test PT,physiotherapy_test_price PP,user_master UM WHERE PA.lab_id = PL.id AND PA.test_id =PT.id AND PA.price = PP.price AND PA.user_id = UM.id AND PA.user_id = '$id'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_notification_users_details()
	{
		$sql = "SELECT * FROM $this->table";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_notification_users_details($id)
	{
		$sql = "SELECT * FROM $this->table WHERE user_id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	 public function get_citizen_details_id($id)
    {
        $sql = "SELECT * FROM $this->table WHERE user_id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
    }
    public function get_count_total_users()
	{
		$sql = "SELECT count(id) as count FROM user_master";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}

}

