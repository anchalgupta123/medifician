<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pathology_labs extends CI_Model {

	public $table ='pathology_labs';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_labs_details()
	{
		$sql = "SELECT * FROM $this->table";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function	insert_labs($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
	public function get_all_lab_data($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function update_labs($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	
	public function delete_lab($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
	public function get_all_test_details()
	{
		$sql = "SELECT * FROM $this->table";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_pathology_test()
	{
		$sql = "SELECT * FROM pathology_labs";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_pathology_health_labs()
	{
		$sql = "SELECT * FROM pathology_labs";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_pathology_executive_health_labs()
	{
		$sql = "SELECT * FROM pathology_labs";
		$res = $this->db->query($sql);
		return $res->result();
	}
 }
