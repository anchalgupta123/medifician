<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_physiotherapy_order extends CI_Model {

	public $table ='physiotherapy_order';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
    public function get_count_total_physiotherapy_orders()
	{
		$sql = "SELECT count(id) as count FROM physiotherapy_order";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_total_physiotherapy_pending_orders()
	{
		$sql = "SELECT count(id) as count FROM physiotherapy_order WHERE status='0'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function get_all_physiotherapy_order_of_perticuler_order($id)
	{
		$sql = "SELECT Po.clinical_history,Po.upload_report,Po.total_amount,Po.created_date_time,um.mobile_no,um.name FROM physiotherapy_order Po,user_master um Where Po.user_id=um.id AND Po.user_id='$id'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function count_total_physiotherapy_confirm_orders()
	{
		$sql = "SELECT count(id) as count FROM physiotherapy_order WHERE status='1'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_physiotherapy_today_sell($start_date,$end_date)
    {
        $sql = "SELECT count(id) as count ,SUM(total_amount) as total_amount FROM `physiotherapy_order` WHERE date(created_date_time) >='$start_date' AND date(created_date_time) <= '$end_date';";
        $query = $this->db->query($sql);
        return $query->row();
    }
	public function get_all_physiotherapy_paid_order_details()
	{
		$sql = "SELECT PA.*,PL.lab_name,PT.test_name,PP.price,UM.name FROM physiotherapy_order PA,physiotherapy_labs PL,physiotherapy_test PT,physiotherapy_test_price PP,user_master UM WHERE PA.lab_id = PL.id AND PA.test_id =PT.id AND PA.price = PP.price AND PA.user_id = UM.id  AND PA.delivery_status = '0'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_physiotherapy_delivered_order()
	{
		$sql = "SELECT PA.*,PL.lab_name,PT.test_name,PP.price,UM.name FROM physiotherapy_order PA,physiotherapy_labs PL,physiotherapy_test PT,physiotherapy_test_price PP,user_master UM WHERE PA.lab_id = PL.id AND PA.test_id =PT.id AND PA.price = PP.price AND PA.user_id = UM.id  AND PA.delivery_status = '1'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_physiotherapy_cancelled_order()
	{
		$sql = "SELECT PA.*,PL.lab_name,PT.test_name,PP.price,UM.name FROM physiotherapy_order PA,physiotherapy_labs PL,physiotherapy_test PT,physiotherapy_test_price PP,user_master UM WHERE PA.lab_id = PL.id AND PA.test_id =PT.id AND PA.price = PP.price AND PA.user_id = UM.id  AND PA.delivery_status = '2'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function delete_cancel_physiotherapy_order($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        return $id;
    }
	public function update_physiotherapy_report_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_update_physiotherapy_report_details($id)
	{
		 $sql = "SELECT * FROM physiotherapy_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_physiotherapy_user_details($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
 	}
 	public function get_physiotherapy_check_out_details($id)
	{
		 $sql = "SELECT PO.*,PT.test_name FROM physiotherapy_order PO,physiotherapy_test PT WHERE PO.test_id=PT.id AND PO.id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_physiotherapy_total_price($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_physiotherapy_all_check_out_details($id)
	{
		$sql = "SELECT PO.*,PT.test_name FROM physiotherapy_order PO,physiotherapy_test PT WHERE PO.test_id=PT.id AND PO.id = '$id'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_physiotherapy_invoice_details($id)
	{
		 $sql = "SELECT PO.*,PT.test_name FROM physiotherapy_order PO,physiotherapy_test PT WHERE PO.test_id=PT.id AND PO.id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_physiotherapy_order_delivered_status_details($id)
	{
		 $sql = "SELECT * FROM physiotherapy_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_physiotherapy_order_payment_status_details($id)
	{
		 $sql = "SELECT * FROM physiotherapy_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_physiotherapy_order_delivered_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function update_physiotherapy_order_payment_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}

 }
