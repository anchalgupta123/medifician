<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_nursing_details extends CI_Model {

	public $table ='nursing_requirement';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_nursing_details()
	{
		$sql = "SELECT * FROM $this->table ORDER BY id DESC";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function	insert_nursing_price($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
	public function get_all_nursing_data($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function update_nursing_data($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function delete_nursing_data($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
	
 }
