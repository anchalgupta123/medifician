<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_radiology_medicine_data extends CI_Model {

	public $table = 'radiology_medicine_data';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert_radiology_medicine_data($data_size)
	{
		$this->db->insert($this->table, $data_size);
		return $this->db->insert_id();
	}

}

/* End of file Model_message_master.php */
/* Location: ./application/models/Model_message_master.php */