<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_radiology_test extends CI_Model {

	public $table ='radiology_test';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_radiology_test_details()
	{	
		$sql="SELECT rt.id,rt.test_name,rt.description,(SELECT  GROUP_CONCAT(rl.lab_name SEPARATOR ',') FROM radiology_labs rl,radiology_test_price rtp WHERE rl.id=rtp.lab_id AND rtp.test_id=rt.id) as lab_name,(SELECT  GROUP_CONCAT(rtp.price SEPARATOR ',') FROM radiology_test_price rtp WHERE rt.id=rtp.test_id) as price,(SELECT  GROUP_CONCAT(rtp.offer_price SEPARATOR ',') FROM radiology_test_price rtp WHERE rt.id=rtp.test_id) as offer_price,(SELECT  GROUP_CONCAT(rtp.off SEPARATOR ',') FROM radiology_test_price rtp WHERE rt.id=rtp.test_id) as off,(SELECT  GROUP_CONCAT(rtp.created_date_time SEPARATOR ',') FROM radiology_test_price rtp WHERE rt.id=rtp.test_id) as created_date_time FROM radiology_test rt ";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function	insert_radiology_test($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
	public function delete_radio_tests($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
	public function get_all_radiology_test_data($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function update_radiology_test($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
 }
