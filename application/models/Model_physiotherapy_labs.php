<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_physiotherapy_labs extends CI_Model {

	public $table ='physiotherapy_labs';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_physiotherapy_labs_details()
	{
		$sql = "SELECT * FROM $this->table";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function	insert_physiotherapy_labs($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
	public function get_physiotherapy_test()
	{
		$sql = "SELECT * FROM physiotherapy_labs";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_physiotherapy_lab_data($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function update_physiotherapy_labs($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function delete_physiotherapy_labs($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
 }
