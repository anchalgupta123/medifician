<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_dietician_user_profile extends CI_Model {

	public $table ='dietician_user_profile';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_dietician_user_profile()
	{
		$sql = "SELECT * FROM $this->table";
		$res = $this->db->query($sql);
		return $res->result();
	}
 }
