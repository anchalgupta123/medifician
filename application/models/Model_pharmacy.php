<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pharmacy extends CI_Model {

	public $table ='pharmacy';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_count_total_pharmacy_orders()
	{
		$sql = "SELECT count(id) as count FROM pharmacy";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_total_pharmacy_pending_orders()
	{
		$sql = "SELECT count(id) as count FROM pharmacy WHERE status='0' or status='1'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_total_pharmacy_confirm_orders()
	{
		$sql = "SELECT count(id) as count FROM pharmacy WHERE status='2'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function get_all_pharmacy_order_of_perticuler_order($id)
	{
		$sql = "SELECT P.problem,P.image,P.total_amount,P.created_date_time,um.mobile_no,um.name FROM pharmacy P,user_master um Where P.user_id=um.id AND P.user_id='$id'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function count_total_pharmacy_delivered_orders()
	{
		$sql = "SELECT count(id) as count FROM pharmacy WHERE status='3'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_pharmacy_today_sell($start_date,$end_date)
    {
        $sql = "SELECT count(id) as count ,SUM(total_amount) as price FROM `pharmacy` WHERE date(created_date_time) >='$start_date' AND date(created_date_time) <= '$end_date';";
        $query = $this->db->query($sql);
        return $query->row();
    }
    public function get_update_delivered_status_details($id)
	{
		 $sql = "SELECT * FROM pharmacy where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_delivered_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_update_payment_status_details($id)
	{
		 $sql = "SELECT * FROM pharmacy where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_payment_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_all_pharmacy_details()
	{   
		$sql = "SELECT P.* FROM pharmacy P WHERE status='0' or status='1'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function update_pricing($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_all_confirm_order_details()
	{
		$sql = "SELECT P.* FROM pharmacy P WHERE status='2'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_delivered_order_details()
	{
		$sql = "SELECT P.* FROM pharmacy P WHERE status='3'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_canceled_order_details()
	{
		$sql = "SELECT P.* FROM pharmacy P WHERE status='4'";
		$res = $this->db->query($sql);
		return $res->result();
	}	
	public function delete_cancel_order($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
	public function get_pharmacy_user_details($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
 	}
 	public function get_pharmacy_check_out_details($id)
	{
		 $sql = "SELECT * from pharmacy Where id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_pharmacy_total_price($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_pharmacy_all_check_out_details($id)
	{
		 $sql = "SELECT p.*,(SELECT  GROUP_CONCAT(pmd.medicine_name SEPARATOR ',') FROM pharmacy_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_name,(SELECT  GROUP_CONCAT(pmd.medicine_price SEPARATOR ',') FROM pharmacy_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_price FROM pharmacy p WHERE p.id = '$id'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_pharmacy_invoice_details($id)
	{
		 $sql = "SELECT p.*,l.company_name,(SELECT  GROUP_CONCAT(pmd.medicine_name SEPARATOR ',') FROM pharmacy_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_name,(SELECT  GROUP_CONCAT(pmd.medicine_price SEPARATOR ',') FROM pharmacy_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_price,(SELECT  GROUP_CONCAT(pmd.qty SEPARATOR ',') FROM pharmacy_medicine_data pmd WHERE p.id=pmd.prescription_id) as qty FROM pharmacy p,login l WHERE p.id = '$id' AND l.id=$this->login_id";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_pharmacy_medicine_details($id)
	{
		 $sql = "SELECT * from pharmacy_medicine_data WHERE prescription_id='$id'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_print_pharmacy_invoice_details($id)
	{
		 $sql = "SELECT p.*,l.company_name,(SELECT  GROUP_CONCAT(pmd.medicine_name SEPARATOR ',') FROM pharmacy_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_name,(SELECT  GROUP_CONCAT(pmd.medicine_price SEPARATOR ',') FROM pharmacy_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_price FROM pharmacy p WHERE p.id = '$id' AND l.id=$this->login_id";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_pp_invoice_count()
	{
		$sql = "SELECT count(id) as count FROM `pharmacy`";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
 }
	