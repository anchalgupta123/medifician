<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_notification_onscreen_user extends CI_Model {

	public $table = 'notification_onscreen_user';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert_notifications_onscreen($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_all_notification_onscreen()
	{
		$sql = "SELECT * FROM `notification_onscreen_user` ORDER BY id DESC";
		$res = $this->db->query($sql);
		return $res->result();
	}

	public function get_onscreen_notification($id)
	{
		$sql = "SELECT * FROM `notification_onscreen_user` WHERE id = '$id' ORDER BY id DESC";
		$res = $this->db->query($sql);
		return $res->row();
	}

	public function get_date_notification()
	{
		$date = date('Y-m-d');

		$sql = "SELECT * FROM `notification_onscreen_user` WHERE start_date <= '$date' AND end_date >= '$date' ORDER BY id DESC";
		$res = $this->db->query($sql);
		return $res->row();
	}

	public function delete_onscreen_notification($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}

}

/* End of file Model_notification_onscreen_user.php */
/* Location: ./application/models/Model_notification_onscreen_user.php */