<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_radiology_order extends CI_Model {

	public $table ='radiology_order';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
    public function get_count_total_radiology_orders()
	{
		$sql = "SELECT count(id) as count FROM radiology_order";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_total_radiology_pending_orders()
	{
		$sql = "SELECT count(id) as count FROM radiology_order WHERE status='0'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_total_radiology_confirm_orders()
	{
		$sql = "SELECT count(id) as count FROM radiology_order WHERE status='1'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_radiology_today_sell($start_date,$end_date)
    {
        $sql = "SELECT count(id) as count ,SUM(total_amount) as total_amount FROM `radiology_order` WHERE date(created_date_time) >='$start_date' AND date(created_date_time) <= '$end_date';";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_all_radiology_order_of_perticuler_order($id)
	{
		$sql = "SELECT ro.clinical_history,ro.upload_report,ro.total_amount,ro.created_date_time,um.mobile_no,um.name FROM radiology_order ro,user_master um Where ro.user_id=um.id AND ro.user_id='$id'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_radiology_cod_order_details()
	{
		$sql = "SELECT RO.*,RL.lab_name,RT.test_name,RT.description,UM.name FROM radiology_order RO,radiology_labs RL,radiology_test RT,user_master UM WHERE RO.test_id = RT.id AND RL.id=RO.lab_id AND RO.user_id = UM.id AND status='0'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function update_radiology_cod_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_update_radiology_cod_status_details($id)
	{
		 $sql = "SELECT * FROM pathology_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_all_radiology_paid_order_details()
	{
		$sql = "SELECT RO.*,RL.lab_name,RT.test_name,RT.description,UM.name FROM radiology_order RO,radiology_labs RL,radiology_test RT,user_master UM WHERE RO.test_id = RT.id AND RL.id=RO.lab_id AND RO.user_id = UM.id AND delivery_status='0'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_radiology_delivered_order()
	{
		$sql = "SELECT RO.*,RL.lab_name,RT.test_name,RT.description,UM.name FROM radiology_order RO,radiology_labs RL,radiology_test RT,user_master UM WHERE RO.test_id = RT.id AND RL.id=RO.lab_id AND RO.user_id = UM.id AND delivery_status='1'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_radiology_cancelled_order()
	{
		$sql = "SELECT RO.*,RL.lab_name,RT.test_name,RT.description,UM.name FROM radiology_order RO,radiology_labs RL,radiology_test RT,user_master UM WHERE RO.test_id = RT.id AND RL.id=RO.lab_id AND RO.user_id = UM.id AND delivery_status='2'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function delete_cancel_radiology_order($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
	public function update_radiology_report_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_update_radiology_report_details($id)
	{
		 $sql = "SELECT * FROM radiology_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_radiology_user_details($id)
    {
        $sql = "SELECT * FROM $this->table WHERE id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
    }
    public function get_radiology_check_out_details($id)
	{
		 $sql = "SELECT RO.*,RT.test_name FROM radiology_order RO,radiology_test RT WHERE RO.test_id=RT.id AND RO.id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_radiology_total_price($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_radiology_all_check_out_details($id)
	{
		$sql = "SELECT RO.*,RT.test_name FROM radiology_order RO,radiology_test RT WHERE RO.test_id=RT.id AND RO.id = '$id'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_radiology_invoice_details($id)
	{
		 $sql = "SELECT PO.*,PT.test_name,PL.lab_name FROM radiology_order PO,radiology_test PT,radiology_labs PL WHERE PO.test_id=PT.id AND PO.id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_radiology_order_delivered_status_details($id)
	{
		 $sql = "SELECT * FROM radiology_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_radiology_order_payment_status_details($id)
	{
		 $sql = "SELECT * FROM radiology_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_radiology_order_delivered_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function update_radiology_order_payment_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	
 }
