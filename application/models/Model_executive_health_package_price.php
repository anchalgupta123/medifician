<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_executive_health_package_price extends CI_Model {

	public $table ='executive_health_package_price';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function	insert_executive_health_package_price($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
	public function get_executive_health_test_price($id)
	{
		$sql="SELECT PP.*,PL.lab_name FROM executive_health_package_price PP,pathology_labs PL  where PP.lab_id= PL.id AND package_id='$id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function delete_executive_health_test_price_data($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
	public function	insert_executive_health_pkg_test_price_data($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
 }
