<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_notification_user extends CI_Model {

	public $table = 'notification_user';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert_notification_user($data_array)
	{
		$this->db->insert($this->table, $data_array);
		return $this->db->insert_id();
	}

}

/* End of file Model_notification_user.php */
/* Location: ./application/models/Model_notification_user.php */