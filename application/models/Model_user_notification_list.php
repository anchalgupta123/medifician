<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_user_notification_list extends CI_Model {

	public $table = 'user_notification_list';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert_notifications($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_count_notification_by_status($id,$status)
	{
		$sql = "SELECT count(id) as count FROM `user_notification_list` WHERE notification_id = '$id' AND iss_seen = '$status'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}

	public function get_user_notification($id)
	{
		$sql = "SELECT UN.*,NU.notification FROM user_notification_list UN,notification_user NU WHERE NU.id=UN.notification_id ";
		$res = $this->db->query($sql);
		return $res->result();
	}

	public function get_user_notification_count($user_id)
	{
		$sql = "SELECT count(id) as count FROM `user_notification_list` WHERE user_id = '$user_id' AND iss_seen = '0'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}

	public function update_user_notification($user_id)
	{
		$data_update = array(
			'iss_seen'=>1,
		);

		$this->db->where('user_id', $user_id);
		$this->db->update($this->table, $data_update);
		return $user_id;
	}

}

/* End of file Model_user_notification_list.php */
/* Location: ./application/models/Model_user_notification_list.php */