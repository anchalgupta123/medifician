<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_vendor extends CI_Model {

	public $table ='login';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_vendor_details()
	{
		$sql = "SELECT * FROM $this->table";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function insert_vendor($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
	public function get_all_dietician_details()
	{
		$sql ="SELECT * FROM `login` where role='Dietician, Nursing' OR role='Dietician' OR role='Dietician, Nursing, Physiotherapy' OR role='Dietician, Physiotherapy' OR role='Pharmacy, Dietician, Nursing, Physiotherapy, Radiologist, Pathology'";
		$res = $this->db->query($sql);
		return $res->result();
	}
 }
