<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_executive_health_package extends CI_Model {

	public $table ='executive_health_package';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_executive_health_pkg_details()
	{
		$sql = "SELECT hp.id,hp.package_name,hp.description,(SELECT  GROUP_CONCAT(pl.lab_name SEPARATOR ',') FROM pathology_labs pl,executive_health_package_price hpp WHERE pl.id=hpp.lab_id AND hpp.package_id=hp.id) as lab_name,(SELECT  GROUP_CONCAT(hpp.price SEPARATOR ',') FROM executive_health_package_price hpp WHERE hp.id=hpp.package_id) as price,(SELECT  GROUP_CONCAT(hpp.offer_price SEPARATOR ',') FROM executive_health_package_price hpp WHERE hp.id=hpp.package_id) as offer_price,(SELECT  GROUP_CONCAT(hpp.off SEPARATOR ',') FROM executive_health_package_price hpp WHERE hp.id=hpp.package_id) as off,(SELECT  GROUP_CONCAT(hpp.created_date_time SEPARATOR ',') FROM executive_health_package_price hpp WHERE hp.id=hpp.package_id) as created_date_time FROM executive_health_package hp ";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function	insert_executive_health_package($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
	public function delete_executive_health_package_price_data($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
	public function get_all_executive_health_test_data($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function update_executive_health_package($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function delete_executive_test($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}

}
/*SELECT HP.*,PL.lab_name FROM health_package HP,pathology_labs PL where HP.lab_id=Pl.id*/
