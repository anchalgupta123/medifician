<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_nursing_appointment extends CI_Model {

	public $table ='nursing_appointment';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_nursing_appointment_details()
	{
		$sql = "SELECT * FROM $this->table";
		$res = $this->db->query($sql);
		return $res->result();
	}
	
 }
