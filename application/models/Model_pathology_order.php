<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pathology_order extends CI_Model {

	public $table ='pathology_order';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_count_total_pathology_orders()
	{
		$sql = "SELECT count(id) as count FROM pathology_order";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_total_pathology_pending_orders()
	{
		$sql = "SELECT count(id) as count FROM pathology_order WHERE status='0'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_total_pathology_confirm_orders()
	{
		$sql = "SELECT count(id) as count FROM pathology_order WHERE status='1'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}

	public function get_all_pathology_order_of_perticuler_order($id)
	{
		$sql = "SELECT pto.clinical_history,pto.upload_report,pto.total_amount,pto.created_date_time,um.mobile_no,um.name FROM pathology_order pto,user_master um Where pto.user_id=um.id AND pto.user_id='$id'";
		$res = $this->db->query($sql);
		return $res->result();
	}

	public function count_pathology_today_sell($start_date,$end_date)
    {
        $sql = "SELECT count(id) as count ,SUM(total_amount) as total_amount FROM `pathology_order` WHERE date(created_date_time) >='$start_date' AND date(created_date_time) <= '$end_date';";
        $query = $this->db->query($sql);
        return $query->row();
    }
	public function get_all_pending_order_details()
	{
		$sql ="SELECT PO.*,PL.lab_name,PT.test_name,PT.description,UM.name FROM pathology_order PO,pathology_labs PL,pathology_test PT,user_master UM WHERE PO.test_id = PT.id AND PL.id = PO.lab_id AND PO.user_id = UM.id AND status='0'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function update_cod_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_update_cod_status_details($id)
	{
		 $sql = "SELECT * FROM pathology_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}

	public function get_all_paid_order_details()
	{
		$sql = "SELECT PO.*,PL.lab_name,PT.test_name,PT.description,UM.name FROM pathology_order PO,pathology_labs PL,pathology_test PT,user_master UM WHERE PO.test_id = PT.id AND PL.id = PO.lab_id AND PO.user_id = UM.id AND delivery_status='0'";
		$res = $this->db->query($sql);
		return $res->result();
	}
    public function update_pathology_report_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_update_pathology_report_details($id)
	{
		 $sql = "SELECT * FROM pathology_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_pathology_user_details($user_id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$user_id'";
		$res = $this->db->query($sql);
		return $res->row();
 	}
	public function get_pathology_check_out_details($id)
	{
		 $sql = "SELECT PO.*,PT.test_name FROM pathology_order PO,pathology_test PT WHERE PO.test_id=PT.id AND PO.id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_pathology_total_price($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_pathology_all_check_out_details($id)
	{
		$sql = "SELECT PO.*,PT.test_name FROM pathology_order PO,pathology_test PT WHERE PO.test_id=PT.id AND PO.id = '$id'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_pathology_invoice_details($id)
	{
		 $sql = "SELECT PO.*,PT.test_name,PL.lab_name FROM pathology_order PO,pathology_test PT,pathology_labs PL WHERE PO.test_id=PT.id AND PO.id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_pathology_delivered_order()
	{
		$sql = "SELECT PO.*,PL.lab_name,PT.test_name,PT.description,UM.name FROM pathology_order PO,pathology_labs PL,pathology_test PT,user_master UM WHERE PO.test_id = PT.id AND PL.id = PO.lab_id AND PO.user_id = UM.id AND delivery_status='1'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_pathology_cancelled_order()
	{
		$sql = "SELECT PO.*,PL.lab_name,PT.test_name,PT.description,UM.name FROM pathology_order PO,pathology_labs PL,pathology_test PT,user_master UM WHERE PO.test_id = PT.id AND PL.id = PO.lab_id AND PO.user_id = UM.id AND delivery_status='2'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function delete_cancel_pathology_order($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        return $id;
    }
	public function get_pathology_order_delivered_status_details($id)
	{
		 $sql = "SELECT * FROM pathology_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_pathology_order_payment_status_details($id)
	{
		 $sql = "SELECT * FROM pathology_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_pathology_order_delivered_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function update_pathology_order_payment_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_p_invoice_count()
	{
		$sql = "SELECT count(id) as count FROM `pathology_order`";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
 }
