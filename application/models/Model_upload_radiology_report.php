<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_upload_radiology_report extends CI_Model {

	public $table ='upload_radiology_report';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function insert_pdf_report($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
	/*public function get_all_banner()
	{
		$sql = "SELECT * FROM $this->table";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function delete_banner($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}*/
	
}

/* End of file Model_product_category.php */
/* Location: ./application/models/Model_product_category.php */