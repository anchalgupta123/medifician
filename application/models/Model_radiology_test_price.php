<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_radiology_test_price extends CI_Model {

	public $table ='radiology_test_price';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function	insert_radiology_test_price($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
	/*SELECT RL.lab_name,RT.test_name FROM radiology_lab_price RP,radiology_labs RL,radiology_test RT WHERE RP.test_id = RT.id AND RL.id=RP.lab_id AND RT.id='4'*/
	public function get_radiology_test_price($id)
	{
		$sql="SELECT PP.*,PL.lab_name FROM radiology_test_price PP,radiology_labs PL  where PP.lab_id= PL.id AND test_id='$id'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function delete_radiology_test_price_data($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
	public function	insert_radiology_test_price_data($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
 }
