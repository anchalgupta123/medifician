<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_nursing_order extends CI_Model {

	public $table ='nursing_order';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_count_total_nursing_orders()
	{
		$sql = "SELECT count(id) as count FROM nursing_order";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function get_count_total_nursing_pending_orders()
	{
		$sql = "SELECT count(id) as count FROM nursing_order WHERE status='0'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function get_count_total_nursing_confirm_orders()
	{
		$sql = "SELECT count(id) as count FROM nursing_order WHERE status='1'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_nursing_today_sell($start_date,$end_date)
    {
        $sql = "SELECT count(id) as count ,SUM(price) as price FROM `nursing_order` WHERE date(created_date_time) >='$start_date' AND date(created_date_time) <= '$end_date';";
        $query = $this->db->query($sql);
        return $query->row();
    }
	public function get_all_nursing_order()
	{
		$sql = "SELECT NO.*,UM.name FROM nursing_order NO,user_master UM where NO.user_id = UM.id AND  status='1'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_nursing_order_of_perticuler_order($id)
	{
		$sql = "SELECT NO.*,UM.name,UM.mobile_no FROM nursing_order NO,user_master UM where NO.user_id = UM.id AND NO.user_id='$id'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_nursing_pending_details()
	{
		$sql = "SELECT NO.*,UM.name FROM nursing_order NO,user_master UM where NO.user_id = UM.id AND  status='0'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function update_nursing_price($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_update_nursing_details($id)
	{
		 $sql = "SELECT * FROM nursing_order where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_nursing_user_details($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
 	}
	
 }
