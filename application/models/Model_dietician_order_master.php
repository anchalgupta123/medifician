<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_dietician_order_master extends CI_Model {

	public $table ='dietician_order_master';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_count_total_dietician_orders()
	{
		$sql = "SELECT count(id) as count FROM dietician_order_master";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
	public function count_dietician_today_sell($start_date,$end_date)
    {
        $sql = "SELECT count(id) as count ,SUM(amount) as amount FROM `dietician_order_master` WHERE date(created_date_time) >='$start_date' AND date(created_date_time) <= '$end_date';";
        $query = $this->db->query($sql);
        return $query->row();
    }
	public function get_all_dietician_order_details()
	{
		$sql = "SELECT DM.*,UM.name FROM `dietician_order_master` DM,user_master UM WHERE DM.user_id = UM.id";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_dietician_order_of_perticuler_order($id)
	{
		$sql = "SELECT DM.*,UM.name,UM.mobile_no FROM `dietician_order_master` DM,user_master UM WHERE DM.user_id = UM.id AND DM.user_id='$id'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function update_dietician_name($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_all_diet_details($id)
	{
		$sql = "SELECT * From dietician_order_master where id='$id'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function get_single_dietician_order_details()
	{
		$sql = "SELECT DM.*,UM.name FROM `dietician_order_master` DM,user_master UM WHERE DM.user_id = UM.id AND dietician_id = $this->login_id";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_dietician_user_details($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
 	}
 	public function update_diet_chart($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_update_diet_chart_details($id)
	{
		 $sql = "SELECT * FROM dietician_order_master where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
 }
