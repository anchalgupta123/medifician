<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pathology_prescription extends CI_Model {

	public $table ='pathology_prescription';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function count_total_pathology_prescription_orders()
	{
		$sql = "SELECT count(id) as count FROM pathology_prescription WHERE status='2'";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}

	public function count_pathology_prescription_today_sell($start_date,$end_date)
    {
        $sql = "SELECT count(id) as count ,SUM(total_amount) as total_amount FROM `pathology_prescription` WHERE date(created_date_time) >='$start_date' AND date(created_date_time) <= '$end_date' AND status='2';";
        $query = $this->db->query($sql);
        return $query->row();
    }
	public function get_all_pathology_prescription_details()
	{
		$sql = "SELECT PP.* FROM pathology_prescription PP WHERE status='0' or status='1'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_pathology_prescription_confirm_details()
	{
		$sql = "SELECT PP.* FROM pathology_prescription PP WHERE status='2'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_all_pathology_prescription_delivered_details()
	{
		$sql = "SELECT PP.* FROM pathology_prescription PP WHERE status='3'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_pathology_delivered_status_details($id)
	{
		 $sql = "SELECT * FROM pathology_prescription where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_all_pathology_prescription_cancelled_details()
	{
		$sql = "SELECT PP.* FROM pathology_prescription PP WHERE status='4'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function delete_cancel_pathology_prescription($id)
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        return $id;
    }
	
	public function update_pathology_delivered_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_all_pathology_prescription_of_perticuler_order($id)
	{
		$sql = "SELECT PP.*,UM.name,UM.mobile_no FROM pathology_prescription PP,user_master UM where PP.user_id = UM.id AND PP.user_id='$id'";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function get_pathology_payment_status_details($id)
	{
		 $sql = "SELECT * FROM pathology_prescription where id='$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function update_pathology_payment_status($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function update_pathology_prescription_pricing($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_pathology_prescription_user_details($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
 	}
 	public function get_prescription_check_out_details($id)
	{
		 $sql = "SELECT * from pathology_prescription Where id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}	
	public function update_pathology_total_price($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
	public function get_prescription_all_check_out_details($id)
	{
		 $sql = "SELECT p.*,(SELECT  GROUP_CONCAT(pmd.medicine_name SEPARATOR ',') FROM pathology_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_name,(SELECT  GROUP_CONCAT(pmd.medicine_price SEPARATOR ',') FROM pathology_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_price FROM pathology_prescription p WHERE p.id = '$id'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_prescription_invoice_details($id)
	{
		$sql = "SELECT p.*,(SELECT  GROUP_CONCAT(pmd.medicine_name SEPARATOR ',') FROM pathology_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_name,(SELECT  GROUP_CONCAT(pmd.medicine_price SEPARATOR ',') FROM pathology_medicine_data pmd WHERE p.id=pmd.prescription_id) as medicine_price FROM pathology_prescription p WHERE p.id = '$id'";
        $res = $this->db->query($sql);
        return $res->row();
	}
	public function get_prescription_medicine_details($id)
	{
		 $sql = "SELECT * from pathology_medicine_data WHERE prescription_id='$id'";
        $res = $this->db->query($sql);
        return $res->result();
	}
	public function get_invoice_count()
	{
		$sql = "SELECT count(id) as count FROM `pathology_prescription`";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}
 }
	