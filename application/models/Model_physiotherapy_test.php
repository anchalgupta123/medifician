<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_physiotherapy_test extends CI_Model {

	public $table ='physiotherapy_test';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	public function get_all_physiotherapy_test_details()
	{ 
    	$sql="SELECT pt.id,pt.test_name,pt.description,(SELECT  GROUP_CONCAT(pl.lab_name SEPARATOR ',') FROM physiotherapy_labs pl,physiotherapy_test_price ptp WHERE pl.id=ptp.lab_id AND ptp.test_id=pt.id) as lab_name,(SELECT  GROUP_CONCAT(ptp.price SEPARATOR ',') FROM physiotherapy_test_price ptp WHERE pt.id=ptp.test_id) as price,(SELECT  GROUP_CONCAT(ptp.offer_price SEPARATOR ',') FROM physiotherapy_test_price ptp WHERE pt.id=ptp.test_id) as offer_price,(SELECT  GROUP_CONCAT(ptp.off SEPARATOR ',') FROM physiotherapy_test_price ptp WHERE pt.id=ptp.test_id) as off,(SELECT  GROUP_CONCAT(ptp.created_date_time SEPARATOR ',') FROM physiotherapy_test_price ptp WHERE pt.id=ptp.test_id) as created_date_time FROM physiotherapy_test pt ";
		$res = $this->db->query($sql);
		return $res->result();
	}
	public function	insert_physiotherapy_test($data_category)
	{
		$this->db->insert($this->table, $data_category);
		return $this->db->insert_id();
	}
	public function delete_physiotherapy_tests($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
		return $id;
	}
	public function get_all_physiotherapy_test_data($id)
	{
		$sql = "SELECT * FROM $this->table WHERE id = '$id'";
		$res = $this->db->query($sql);
		return $res->row();
	}
	public function update_physiotherapy_test($data,$id)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, $data);
		return $id;
	}
 }
