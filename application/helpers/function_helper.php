<?php

function random_num($size) {
	$alpha_key = '';
	$keys = range('A', 'Z');

	for ($i = 0; $i < 2; $i++) {
		$alpha_key .= $keys[array_rand($keys)];
	}

	$length = $size - 2;

	$key = '';
	$keys = range(0, 9);

	for ($i = 0; $i < $length; $i++) {
		$key .= $keys[array_rand($keys)];
	}

	return $alpha_key . $key;
}

function json_output($statusHeader,$response)
{
	$ci =& get_instance();
	$ci->output->set_content_type('application/json');
	$ci->output->set_status_header($statusHeader);
	$ci->output->set_output(json_encode($response));
}

function change_date_format_dmy($date)
{
	if ($date != '0000-00-00') {
		$date = date("d-m-Y", strtotime($date));
		return $date;
	}
	else{
		return '__-__-____';
	}
}

function change_date_format_ymd($date)
{
	if ($date != '00-00-0000') {
		$date = date("Y-m-d", strtotime($date));
		return $date;
	}
	else{
		return '__-__-____';
	}
}

function age_count($date)
{
	if ($date == '0000-00-00') {
		return "";	
	}
	else
	{
		$today = date("Y-m-d");
		$diff = date_diff(date_create($date), date_create($today));
		return $diff->format('%y');
	}
}

function send_otp_sms1($mobileNumber, $message)
{
	//Your authentication key
	//Sender ID, While using route4 sender id should be 6 characters long.
	$senderId = "GHARTK";
	// $message = "Hello, Welcome to Splection. Please verify your phone number by entering One Time Password(OTP). Your OTP is :".$otpCode.".";
	//Your message to send, Add URL encoding here.
	$message = urlencode($message);
	//Define route 
	$route = "4";    //1-Promotional, 4-Transactional, OTP
	//Prepare you post parameters
	$postData = array(
		'sender' => $senderId,
		'route' => $route
	);
	//API URL
	// $url="http://bulksms.msghouse.in/api/sendhttp.php?authkey=7481Alfes954O95cc7dffc&mobiles=".$mobileNumber."&message=".$message;
	$url = "http://bulksms.msghouse.in/api/sendhttp.php?authkey=7924AVDKVzQ2d5d8605ac&mobiles=".$mobileNumber."&message=".$message;
	// init the resource
	$ch = curl_init();
	curl_setopt_array($ch, array(
	    CURLOPT_URL => $url,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_POST => true,
	    CURLOPT_POSTFIELDS => $postData
	    //,CURLOPT_FOLLOWLOCATION => true
	));
	//Ignore SSL certificate verification
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


	//get response
	$output = curl_exec($ch);

	//Print error if any
	if(curl_errno($ch))
	{
	   return 'error:' . curl_error($ch);
	}

	curl_close($ch);
}

function sendPushNotificationToFCMSever($title,$message,$regId,$include_image='') {

	error_reporting(-1);
    ini_set('display_errors', 'On');
  	require_once ('class.firebase.php');
    require_once ('class.push.php');

    $firebase = new Firebase();
    $push = new Push();

    $payload = array();

    $push->setTitle($title);
    $push->setMessage($message);
    if ($include_image) {
        $push->setImage('https://api.androidhive.info/images/minion.jpg');
    } else {
        $push->setImage('');
    }

    $json = '';
    $response = '';

    $json = $push->getPush();
    $response = $firebase->send($regId, $json);

    return $response;
    
}