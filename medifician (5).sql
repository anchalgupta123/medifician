-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2020 at 02:24 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medifician`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner_master`
--

CREATE TABLE `banner_master` (
  `id` int(11) NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner_master`
--

INSERT INTO `banner_master` (`id`, `banner_image`, `created_date_time`) VALUES
(5, '0.36813100 1577360341banner.png', '2019-12-26 17:09:01'),
(7, '0.79413900 1577360425Mavenlink Blog Pic.png', '2019-12-26 17:10:25'),
(8, '0.45670600 15773604932.png', '2019-12-26 17:11:33'),
(25, '0.49550900 15798494970.66971400 157416751668469159_111456566859511_4983000930602450944_n (1).jpg', '2020-01-24 12:34:57');

-- --------------------------------------------------------

--
-- Table structure for table `dietician_order_master`
--

CREATE TABLE `dietician_order_master` (
  `id` int(11) NOT NULL,
  `dietician_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(255) NOT NULL,
  `diet_for` varchar(255) NOT NULL,
  `weight` varchar(255) NOT NULL,
  `height` varchar(255) NOT NULL,
  `sleeping_hours` varchar(255) NOT NULL,
  `vegeterian` varchar(255) NOT NULL,
  `habbit` varchar(255) NOT NULL,
  `life_style` varchar(255) NOT NULL,
  `working_hour` varchar(255) NOT NULL,
  `sitting_job` varchar(255) NOT NULL,
  `clinical_history` varchar(255) NOT NULL,
  `problem` varchar(200) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `upload_diet_chart` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dietician_order_master`
--

INSERT INTO `dietician_order_master` (`id`, `dietician_id`, `user_id`, `name`, `dob`, `gender`, `diet_for`, `weight`, `height`, `sleeping_hours`, `vegeterian`, `habbit`, `life_style`, `working_hour`, `sitting_job`, `clinical_history`, `problem`, `amount`, `upload_diet_chart`, `created_date_time`) VALUES
(1, 7, 1, 'anchal', '1997-04-04', 'Female', 'Weight loss', '50', '5.5', '8 hour\'s', 'vegeterian', 'no', 'bjhd', '8 hour\'s', 'yes', 'no', 'I am Very Fat, Please Help Me.', '500', '', '2020-01-24 12:38:27'),
(3, 3, 2, 'dbfjg', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', 'dfvdv', '500', '0.25178100 15784886181320_(5) (2).pdf', '2020-01-08 18:33:38');

-- --------------------------------------------------------

--
-- Table structure for table `executive_health_package`
--

CREATE TABLE `executive_health_package` (
  `id` int(11) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `executive_health_package`
--

INSERT INTO `executive_health_package` (`id`, `package_name`, `description`, `created_date_time`) VALUES
(2, 'Regular ', 'dvscsedcd', '2020-02-07 18:39:07');

-- --------------------------------------------------------

--
-- Table structure for table `executive_health_package_price`
--

CREATE TABLE `executive_health_package_price` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `offer_price` varchar(255) NOT NULL,
  `off` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `executive_health_package_price`
--

INSERT INTO `executive_health_package_price` (`id`, `package_id`, `lab_id`, `price`, `offer_price`, `off`, `created_date_time`) VALUES
(1, 1, 1, '500', '400', '20', '2020-01-20 12:15:36'),
(3, 1, 2, '6000', '5000', '17', '2020-02-07 18:38:24'),
(4, 2, 1, '6000', '5500', '8', '2020-02-07 18:39:07');

-- --------------------------------------------------------

--
-- Table structure for table `generate_invoice_no`
--

CREATE TABLE `generate_invoice_no` (
  `id` int(11) NOT NULL,
  `invoice_no` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `health_package`
--

CREATE TABLE `health_package` (
  `id` int(11) NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `featured` tinyint(4) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `health_package`
--

INSERT INTO `health_package` (`id`, `package_name`, `category`, `featured`, `description`, `created_date_time`) VALUES
(2, 'dmfvkjd', '', 0, 'mjdcjd', '2020-01-17 18:00:22'),
(3, 'Full package', '', 0, 'this package is helpful', '2020-01-17 18:03:38'),
(4, 'Regular', 'Heart', 1, 'this package include 10 regular tests', '2020-02-07 18:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `health_package_price`
--

CREATE TABLE `health_package_price` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `offer_price` varchar(255) NOT NULL,
  `off` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `health_package_price`
--

INSERT INTO `health_package_price` (`id`, `package_id`, `lab_id`, `price`, `offer_price`, `off`, `created_date_time`) VALUES
(2, 2, 2, '3000', '2700', '15', '2020-01-17 18:00:22'),
(3, 3, 2, '5899', '5800', '2', '2020-01-17 18:03:38'),
(5, 4, 2, '3000', '2500', '17', '2020-02-07 17:41:23'),
(6, 4, 1, '4000', '3500', '13', '2020-02-07 18:02:53');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `real_password` varchar(20) NOT NULL,
  `role` varchar(100) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `name`, `company_name`, `email`, `mobile_no`, `address`, `password`, `real_password`, `role`, `created_date_time`) VALUES
(1, 'Admin', 'Medificians', 'admin', '7855651546', 'Indore', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'Admin', '2020-02-07 11:34:31'),
(3, 'harshal', '', 'harshal@gmail.com', '986875875', 'navalkaha', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'Dietician', '2019-12-24 14:09:00'),
(5, 'Anjali Gupta', '', 'komalgup27@gmail.com', '8223016122', 'indore', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'Physiotherapy', '2019-12-24 14:21:27'),
(7, 'kajal', '', 'abc@gmail.com', '4651551242', 'geetabhavan ,indore', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'Dietician, Nursing, Physiotherapy', '2019-12-24 15:21:12'),
(8, 'vinay', '', 'vinay@gmail.com', '87584378', 'indore', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'Dietician', '2019-12-24 17:15:32'),
(13, 'ajay', '', 'ajay@gmail.com', '8943578943', 'indore', '4266bf8d3dc65bc84fd3badf2edfdbe7', '1234456', 'Dietician, Physiotherapy', '2019-12-24 18:05:32'),
(20, 'Pharmacy', '', 'p123@gmail.com', '1234567891', 'indore', 'fcea920f7412b5da7be0cf42b8c93759', '1234567', 'Pharmacy', '2020-01-25 17:20:41'),
(22, 'jjdchj', '', 'hjdchd', 'dvf df ', 'fdv ', '9400ebf223f50ff8fccb32ed13ea819e', 'c ', 'Pharmacy', '2020-01-08 14:07:48'),
(23, 'Anchal Gupta  ', 'A Medical Store', 'anchal123@gmail.com', '1323256366', 'geetabhavan ,indore ', '25d55ad283aa400af464c76d713c07ad', '12345678', 'Pharmacy', '2020-01-25 17:36:18');

-- --------------------------------------------------------

--
-- Table structure for table `notification_message_master`
--

CREATE TABLE `notification_message_master` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notification_message_master`
--

INSERT INTO `notification_message_master` (`id`, `user_id`, `message_id`, `created_date_time`) VALUES
(1, 0, 51, '2020-01-11 12:04:57'),
(2, 0, 52, '2020-01-11 12:05:11'),
(3, 0, 53, '2020-01-11 12:11:19'),
(4, 1, 54, '2020-01-11 12:12:59'),
(5, 2, 54, '2020-01-11 12:12:59'),
(6, 1, 55, '2020-01-14 12:12:39'),
(7, 2, 55, '2020-01-14 12:12:39'),
(8, 3, 55, '2020-01-14 12:12:39'),
(9, 4, 55, '2020-01-14 12:12:39');

-- --------------------------------------------------------

--
-- Table structure for table `notification_onscreen_user`
--

CREATE TABLE `notification_onscreen_user` (
  `id` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `notification` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notification_onscreen_user`
--

INSERT INTO `notification_onscreen_user` (`id`, `start_date`, `end_date`, `notification`, `created_date_time`) VALUES
(2, '2020-01-11', '2020-01-11', 'kjdfhvk', '2020-01-03 13:48:53');

-- --------------------------------------------------------

--
-- Table structure for table `notification_user`
--

CREATE TABLE `notification_user` (
  `id` int(11) NOT NULL,
  `notification` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notification_user`
--

INSERT INTO `notification_user` (`id`, `notification`, `created_date_time`) VALUES
(1, 'your order is confirm', '2020-01-10 00:00:00'),
(17, 'We gave you price Please Accept It', '2020-01-10 14:23:04'),
(19, '', '2020-01-10 14:33:01'),
(20, 'hello', '2020-01-10 15:14:15'),
(21, 'hjckhvk', '2020-01-10 15:15:10'),
(30, 'hello', '2020-01-10 17:59:35'),
(31, 'mnbg', '2020-01-10 18:38:20'),
(32, 'hello this is for all', '2020-01-10 19:10:24'),
(33, 'hgyufyuf', '2020-01-10 19:14:04'),
(34, 'mcn kj', '2020-01-10 19:14:50'),
(35, 'ggggggggggggg', '2020-01-10 19:15:45'),
(36, 'hello', '2020-01-10 19:19:25'),
(46, 'hello nfvjdfi', '2020-01-10 21:39:24'),
(47, 'xmn x', '2020-01-10 22:08:36'),
(48, 'jhdgc iugdof ', '2020-01-11 11:53:34'),
(49, 'jkhkjhkjh', '2020-01-11 12:01:27'),
(50, 'jhjhjghj', '2020-01-11 12:03:48'),
(51, 'hjgjhghjg', '2020-01-11 12:04:57'),
(52, 'hjgjhghjghjghjg', '2020-01-11 12:05:11'),
(53, 'jhghjghjghjg', '2020-01-11 12:11:19'),
(54, 'change by medifician notification', '2020-01-11 12:12:59'),
(55, 'this is notification', '2020-01-14 12:12:39');

-- --------------------------------------------------------

--
-- Table structure for table `nursing_order`
--

CREATE TABLE `nursing_order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  `p_mobile_no` varchar(255) NOT NULL,
  `alt_mobile_no` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `a_city` varchar(255) NOT NULL,
  `a_state` varchar(255) NOT NULL,
  `appointment_date` datetime NOT NULL,
  `appointment_time` varchar(100) NOT NULL,
  `clinical_history` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nursing_order`
--

INSERT INTO `nursing_order` (`id`, `user_id`, `time`, `price`, `status`, `name`, `p_mobile_no`, `alt_mobile_no`, `email`, `address`, `landmark`, `a_city`, `a_state`, `appointment_date`, `appointment_time`, `clinical_history`, `description`, `created_date_time`) VALUES
(1, 1, '2 Hour\'s\r\n', '500', 1, 'aaa', '325154561', '216521', 'abc@gmail.com', 'geeta bhawan', 'kautilya acadmy', 'indore', 'mp', '2020-01-02 03:15:00', '04:30 PM', 'no ', 'vgazvxusgxuys', '0000-00-00 00:00:00'),
(2, 2, '10 Hour\'s\r\n', '', 0, 'bbb', '34656245', '348421565', 'aaa@gmail.com', 'djbfvu', 'jsbcc', 'indore', 'mp', '2020-01-02 00:00:00', '12:15 PM', '56423165545454', 'jdcbhhsdvc', '2020-01-07 18:17:46');

-- --------------------------------------------------------

--
-- Table structure for table `nursing_requirement`
--

CREATE TABLE `nursing_requirement` (
  `id` int(11) NOT NULL,
  `time` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nursing_requirement`
--

INSERT INTO `nursing_requirement` (`id`, `time`, `price`, `created_date_time`) VALUES
(1, '2 Hour\'s', '600', '2020-02-04 12:18:20'),
(2, '2 Hour\'s', '500', '2020-01-02 12:25:08'),
(3, '2 Hour\'s', '500', '2020-01-02 12:25:08'),
(4, '8 Hour\'s', '1500', '2020-01-02 12:28:30'),
(5, '4 Hour\'s', '500', '2020-01-03 13:39:36');

-- --------------------------------------------------------

--
-- Table structure for table `pathology_labs`
--

CREATE TABLE `pathology_labs` (
  `id` int(11) NOT NULL,
  `lab_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `c_state` varchar(200) NOT NULL,
  `mobile_no` varchar(200) NOT NULL,
  `landline_no` varchar(100) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pathology_labs`
--

INSERT INTO `pathology_labs` (`id`, `lab_name`, `email`, `address`, `city`, `c_state`, `mobile_no`, `landline_no`, `created_date_time`) VALUES
(1, 'ABC', 'abc@gmail.com', 'indore', 'indore', 'Madhya Pradesh', '4651551242', '23165454', '2019-12-28 14:36:36'),
(2, 'Dr. Path', 'abc@gmail.com', 'geeta bhawan', 'indore', 'Madhya Pradesh', '986875875', '07673-232125', '2019-12-26 18:55:01');

-- --------------------------------------------------------

--
-- Table structure for table `pathology_medicine_data`
--

CREATE TABLE `pathology_medicine_data` (
  `id` int(11) NOT NULL,
  `prescription_id` int(11) NOT NULL,
  `medicine_name` varchar(255) NOT NULL,
  `medicine_price` varchar(255) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pathology_medicine_data`
--

INSERT INTO `pathology_medicine_data` (`id`, `prescription_id`, `medicine_name`, `medicine_price`, `qty`, `created_date_time`) VALUES
(1, 2, 'nhvhjvj', '345', '4', '2020-01-14 19:25:33'),
(2, 2, 'bhfd', '2121', '1', '2020-01-25 13:48:24'),
(3, 2, 'jdbcj', '311', '2', '2020-01-25 13:49:15');

-- --------------------------------------------------------

--
-- Table structure for table `pathology_order`
--

CREATE TABLE `pathology_order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `gst_amount` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `txn_type` tinyint(4) NOT NULL,
  `txn_id` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `delivery_status` tinyint(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `p_mobile_no` varchar(100) NOT NULL,
  `alt_mobile_no` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `landmark` varchar(100) NOT NULL,
  `a_city` varchar(100) NOT NULL,
  `a_state` varchar(100) NOT NULL,
  `appointment_date` datetime NOT NULL,
  `appointment_time` varchar(100) NOT NULL,
  `clinical_history` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `upload_report` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pathology_order`
--

INSERT INTO `pathology_order` (`id`, `user_id`, `test_id`, `lab_id`, `price`, `gst_amount`, `total_amount`, `txn_type`, `txn_id`, `status`, `delivery_status`, `name`, `p_mobile_no`, `alt_mobile_no`, `email`, `address`, `landmark`, `a_city`, `a_state`, `appointment_date`, `appointment_time`, `clinical_history`, `description`, `upload_report`, `created_date_time`) VALUES
(1, 1, 13, 1, '20', '4', '24', 1, '', 1, 0, 'jrf', '165445', '5451', 'abc@gmail.com', 'ebfiueuirf', 'jufhuie', 'rgh', 'hfj', '2020-01-28 00:00:00', '10 AM', ' jhgi', 'nbvfti', '0.88172400 15784863148944_(8) (4).pdf', '2020-01-21 12:34:52'),
(2, 3, 13, 2, '500', '90', '590', 0, '54545', 1, 1, 'hhjhjhb', '656', '3265965', 'abc@gmail.com', 'knkjgj', 'jj', 'kjhiu', 'kjhg', '2020-01-08 00:00:00', '7 PM', 'fghnjm,', 'rtghjk', '', '2020-01-21 12:15:30');

-- --------------------------------------------------------

--
-- Table structure for table `pathology_prescription`
--

CREATE TABLE `pathology_prescription` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `clinical_history` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `problem` varchar(255) NOT NULL,
  `pricing` varchar(255) NOT NULL,
  `payment_type` tinyint(4) NOT NULL,
  `gst_percent` varchar(255) NOT NULL,
  `gst_amount` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pathology_prescription`
--

INSERT INTO `pathology_prescription` (`id`, `user_id`, `name`, `email`, `mobile_no`, `address`, `city`, `state`, `clinical_history`, `description`, `image`, `problem`, `pricing`, `payment_type`, `gst_percent`, `gst_amount`, `total_amount`, `status`, `created_date_time`) VALUES
(1, 1, 'anchal gupta', '', '', 'dhbchjd', '', '', '', '', '', 'hsgsshhg', '398', 0, '', '', '', 1, '2019-12-28 00:00:00'),
(2, 2, 'anjali', '', '', 'hbhsdbhcs', '', '', '', '', '0.66971400 157416751668469159_111456566859511_4983000930602450944_n.jpg', 'jdnjvsdj', '12121', 1, '18%', '62', '407 ', 2, '2020-01-25 13:49:15');

-- --------------------------------------------------------

--
-- Table structure for table `pathology_test`
--

CREATE TABLE `pathology_test` (
  `id` int(11) NOT NULL,
  `test_name` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pathology_test`
--

INSERT INTO `pathology_test` (`id`, `test_name`, `category`, `description`, `created_date_time`) VALUES
(17, 'ABC', 'Heart', 'jfkj', '2020-02-07 17:19:19'),
(18, 'jhvhj', '', 'nbv', '2020-01-09 16:21:22'),
(19, 'yugui', '', 'hghu', '2020-01-09 17:16:50'),
(20, 'ajay new test data', '', 'this is my new test and we are calculate whole calculation', '2020-02-03 18:59:05'),
(21, 'kjdhjc', 'Heart', 'kdjnc kjd', '2020-02-07 17:01:40'),
(22, 'xd dc', 'Liver', 'fv df', '2020-02-07 17:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `pathology_test_price`
--

CREATE TABLE `pathology_test_price` (
  `id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `price` varchar(200) NOT NULL,
  `offer_price` varchar(200) NOT NULL,
  `off` varchar(200) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pathology_test_price`
--

INSERT INTO `pathology_test_price` (`id`, `test_id`, `lab_id`, `price`, `offer_price`, `off`, `created_date_time`) VALUES
(2, 10, 6, '2145', '32465', '25', '0000-00-00 00:00:00'),
(3, 11, 6, '64', '25', '11', '0000-00-00 00:00:00'),
(5, 12, 6, '211', '55', '11', '0000-00-00 00:00:00'),
(7, 13, 11, '2333', '222', '10', '0000-00-00 00:00:00'),
(8, 14, 6, '31', '20', '5', '0000-00-00 00:00:00'),
(9, 14, 11, '23', '62', '2', '0000-00-00 00:00:00'),
(10, 15, 11, '95', '20', '', '0000-00-00 00:00:00'),
(11, 16, 11, '566', '500', '5', '0000-00-00 00:00:00'),
(12, 17, 1, '55', '555', '5', '0000-00-00 00:00:00'),
(13, 17, 1, '44', '444', '444', '0000-00-00 00:00:00'),
(14, 17, 1, '500', '400', '5', '0000-00-00 00:00:00'),
(15, 18, 1, '400', '330', '5', '0000-00-00 00:00:00'),
(16, 18, 2, '500', '600', '5', '0000-00-00 00:00:00'),
(18, 19, 1, '500', '444 ', '11', '0000-00-00 00:00:00'),
(19, 19, 1, '600', '500', '', '0000-00-00 00:00:00'),
(20, 20, 1, '100', '50', '50', '0000-00-00 00:00:00'),
(21, 20, 3, '200', '100', '50', '0000-00-00 00:00:00'),
(22, 20, 2, '300', '200', '33', '0000-00-00 00:00:00'),
(23, 20, 1, '400', '300', '25', '0000-00-00 00:00:00'),
(24, 0, 0, '', '', '', '0000-00-00 00:00:00'),
(25, 0, 0, '', '', '', '0000-00-00 00:00:00'),
(26, 0, 2, '5000', '4000', '20', '0000-00-00 00:00:00'),
(27, 1, 2, '500', '400', '20', '0000-00-00 00:00:00'),
(28, 1, 2, '154', '212', '-38', '0000-00-00 00:00:00'),
(29, 1, 2, '300', '89', '70', '0000-00-00 00:00:00'),
(30, 1, 2, '500', '200', '60', '0000-00-00 00:00:00'),
(31, 2, 2, '300', '200', '33', '0000-00-00 00:00:00'),
(42, 20, 1, '500', '600', '-20', '0000-00-00 00:00:00'),
(43, 21, 1, '500', '400', '20', '2020-02-07 17:01:40'),
(44, 22, 1, '500', '400', '20', '2020-02-07 17:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `pharmacy`
--

CREATE TABLE `pharmacy` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `address` varchar(200) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `image` varchar(200) NOT NULL,
  `pricing` varchar(100) NOT NULL,
  `problem` varchar(255) NOT NULL,
  `gst_amount` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `payment_type` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pharmacy`
--

INSERT INTO `pharmacy` (`id`, `user_id`, `name`, `email`, `mobile_no`, `address`, `landmark`, `city`, `state`, `image`, `pricing`, `problem`, `gst_amount`, `total_amount`, `payment_type`, `status`, `created_date_time`) VALUES
(5, 1, 'jhvgh', 'ngfj', '21613', 'nbhh', 'ghfyt', 'nbgf', 'ghfkuy', 'aid5679943-v4-1200px-Write-a-Prescription-Step-15.jpg', '600', '', '108', '708', 0, 1, '2020-01-14 17:19:30'),
(6, 3, 'ankur bajaj', 'ankur@gmail.com', '5223362524', 'geeta bhawan ', 'kautilya acadmy', 'indore', 'mp', 'aid5679943-v4-1200px-Write-a-Prescription-Step-15.jpg', '5000', '', '', '', 1, 2, '2020-01-25 12:11:48');

-- --------------------------------------------------------

--
-- Table structure for table `pharmacy_medicine_data`
--

CREATE TABLE `pharmacy_medicine_data` (
  `id` int(11) NOT NULL,
  `prescription_id` int(11) NOT NULL,
  `medicine_name` varchar(255) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `medicine_gst_price` varchar(255) NOT NULL,
  `medicine_price` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `physiotherapy_labs`
--

CREATE TABLE `physiotherapy_labs` (
  `id` int(11) NOT NULL,
  `lab_name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address` varchar(255) NOT NULL,
  `p_city` varchar(200) NOT NULL,
  `p_state` varchar(200) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `landline_no` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `physiotherapy_labs`
--

INSERT INTO `physiotherapy_labs` (`id`, `lab_name`, `email`, `address`, `p_city`, `p_state`, `mobile_no`, `landline_no`, `created_date_time`) VALUES
(1, 'AAA', 'aaa@gmail.com', 'kautilya Acadmy, indore', 'indore', 'mp', '3211546512', '2111515', '2020-02-03 16:28:39');

-- --------------------------------------------------------

--
-- Table structure for table `physiotherapy_medicine_data`
--

CREATE TABLE `physiotherapy_medicine_data` (
  `id` int(11) NOT NULL,
  `prescription_id` int(11) NOT NULL,
  `medicine_name` varchar(255) NOT NULL,
  `medicine_price` varchar(255) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `physiotherapy_medicine_data`
--

INSERT INTO `physiotherapy_medicine_data` (`id`, `prescription_id`, `medicine_name`, `medicine_price`, `qty`, `created_date_time`) VALUES
(2, 1, 'ndkj', '400 ', '4 ', '2020-01-25 15:39:25');

-- --------------------------------------------------------

--
-- Table structure for table `physiotherapy_order`
--

CREATE TABLE `physiotherapy_order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `test_id` varchar(255) NOT NULL,
  `lab_id` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `gst_amount` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `txn_type` tinyint(4) NOT NULL,
  `txn_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `delivery_status` tinyint(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `p_mobile_no` varchar(200) NOT NULL,
  `alt_mobile_no` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `landmark` varchar(200) NOT NULL,
  `a_city` varchar(200) NOT NULL,
  `a_state` varchar(200) NOT NULL,
  `appointment_date` datetime NOT NULL,
  `appointment_time` varchar(100) NOT NULL,
  `clinical_history` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `upload_report` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `physiotherapy_order`
--

INSERT INTO `physiotherapy_order` (`id`, `user_id`, `test_id`, `lab_id`, `price`, `gst_amount`, `total_amount`, `txn_type`, `txn_id`, `status`, `delivery_status`, `name`, `p_mobile_no`, `alt_mobile_no`, `email`, `address`, `landmark`, `a_city`, `a_state`, `appointment_date`, `appointment_time`, `clinical_history`, `description`, `upload_report`, `created_date_time`) VALUES
(1, 1, '2', '3', '300', '', '', 1, 0, 1, 1, 'Harsh', '6585865462', '2536455656', 'harsh1234@gmail.com', 'geeta bhawan, indore', 'kautilya acadmy', 'indore', 'mp', '2019-12-28 00:00:00', '4PM', 'No Issue', 'hjbxasuhc', '', '2020-01-21 13:40:09'),
(2, 2, '2', '3', '300', '54', '354', 0, 0, 0, 0, 'fjvi', '32316545', '65744', 'rtjg', 'mrkjthgiu;', 'kjurhfr', 'jrfhie', 'mrtbkjg', '2020-01-04 00:00:00', '3 PM ', 'nscjhgs', 'kjfnjeri', '', '2020-01-13 16:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `physiotherapy_prescription`
--

CREATE TABLE `physiotherapy_prescription` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `prescription_image` varchar(255) NOT NULL,
  `problem` varchar(255) NOT NULL,
  `pricing` varchar(255) NOT NULL,
  `gst_percent` varchar(255) NOT NULL,
  `gst_amount` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `payment_type` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `physiotherapy_prescription`
--

INSERT INTO `physiotherapy_prescription` (`id`, `user_id`, `name`, `email`, `mobile_no`, `address`, `landmark`, `city`, `state`, `prescription_image`, `problem`, `pricing`, `gst_percent`, `gst_amount`, `total_amount`, `payment_type`, `status`, `created_date_time`) VALUES
(1, 2, 'Anchal Gupta', 'abc@gmail.com', '3245564154', 'hsvxvh', 'nshxvhg', '', '', '', 'jjksckj', '400', '18%', '94', '616', 2, 2, '2020-01-25 15:39:25');

-- --------------------------------------------------------

--
-- Table structure for table `physiotherapy_test`
--

CREATE TABLE `physiotherapy_test` (
  `id` int(11) NOT NULL,
  `test_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `physiotherapy_test`
--

INSERT INTO `physiotherapy_test` (`id`, `test_name`, `description`, `created_date_time`) VALUES
(1, 'ZZZ', 'hvhjvhd', '2020-02-04 11:58:35');

-- --------------------------------------------------------

--
-- Table structure for table `physiotherapy_test_price`
--

CREATE TABLE `physiotherapy_test_price` (
  `id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `offer_price` varchar(255) NOT NULL,
  `off` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `physiotherapy_test_price`
--

INSERT INTO `physiotherapy_test_price` (`id`, `test_id`, `lab_id`, `price`, `offer_price`, `off`, `created_date_time`) VALUES
(1, 1, 1, '300', '250', '10', '2019-12-28 18:28:47'),
(2, 2, 3, '245', '22', '20', '2019-12-30 17:55:21'),
(3, 2, 1, '200', '180', '10', '2019-12-30 17:55:21'),
(4, 1, 1, '600', '500', '17', '2020-02-04 12:00:11');

-- --------------------------------------------------------

--
-- Table structure for table `radiology_labs`
--

CREATE TABLE `radiology_labs` (
  `id` int(11) NOT NULL,
  `lab_name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `r_city` varchar(100) NOT NULL,
  `r_state` varchar(100) NOT NULL,
  `mobile_no` varchar(200) NOT NULL,
  `landline_no` varchar(100) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `radiology_labs`
--

INSERT INTO `radiology_labs` (`id`, `lab_name`, `email`, `address`, `r_city`, `r_state`, `mobile_no`, `landline_no`, `created_date_time`) VALUES
(1, 'Dr. Path', 'anchal@gmail.com', 'geeta bhawan,indore', 'indore', 'mp', '5545645645', '21545454545', '2020-02-03 15:55:08');

-- --------------------------------------------------------

--
-- Table structure for table `radiology_medicine_data`
--

CREATE TABLE `radiology_medicine_data` (
  `id` int(11) NOT NULL,
  `prescription_id` int(11) NOT NULL,
  `medicine_name` varchar(255) NOT NULL,
  `medicine_price` varchar(255) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `radiology_medicine_data`
--

INSERT INTO `radiology_medicine_data` (`id`, `prescription_id`, `medicine_name`, `medicine_price`, `qty`, `created_date_time`) VALUES
(1, 1, 'mnc kj', '600', '6', '2020-01-15 12:12:43'),
(2, 2, ' ksc js', '255', '5', '2020-01-25 15:12:35');

-- --------------------------------------------------------

--
-- Table structure for table `radiology_order`
--

CREATE TABLE `radiology_order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `gst_amount` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `txn_type` tinyint(4) NOT NULL,
  `txn_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `delivery_status` tinyint(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `p_mobile_no` varchar(50) NOT NULL,
  `alt_mobile_no` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `landmark` varchar(100) NOT NULL,
  `a_city` varchar(100) NOT NULL,
  `a_state` varchar(100) NOT NULL,
  `appointment_date` datetime NOT NULL,
  `a_appointment_time` varchar(100) NOT NULL,
  `clinical_history` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `upload_report` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `radiology_order`
--

INSERT INTO `radiology_order` (`id`, `user_id`, `test_id`, `lab_id`, `price`, `gst_amount`, `total_amount`, `txn_type`, `txn_id`, `status`, `delivery_status`, `name`, `p_mobile_no`, `alt_mobile_no`, `email`, `address`, `landmark`, `a_city`, `a_state`, `appointment_date`, `a_appointment_time`, `clinical_history`, `description`, `upload_report`, `created_date_time`) VALUES
(1, 1, 5, 1, '200', '36', '236', 1, 0, 1, 1, 'Anchal Gupta', '21541545454', '6564654665', 'abc@gmail.com', 'geeta bahwan indore', 'kautilya acadmy', 'indore', 'mp', '2019-12-28 00:00:00', '7 AM', 'no clinical history', 'i need a radiologist', '0.98070800 15784860927497loos......pdf', '2020-01-21 13:11:21'),
(3, 2, 5, 10, '600', '', '', 0, 0, 0, 0, 'nvjh', '2523222336', '4552255632', 'abc@gmail.com', 'indore', 'geeta bhawan', 'indore', 'mp', '2020-01-20 00:00:00', '7 PM', 'no', 'bsdbcjs', '', '2020-01-21 13:10:20');

-- --------------------------------------------------------

--
-- Table structure for table `radiology_prescription`
--

CREATE TABLE `radiology_prescription` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `prescription_image` varchar(255) NOT NULL,
  `problem` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `gst_percent` varchar(255) NOT NULL,
  `gst_amount` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `payment_type` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `radiology_prescription`
--

INSERT INTO `radiology_prescription` (`id`, `user_id`, `name`, `email`, `mobile_no`, `address`, `landmark`, `city`, `state`, `prescription_image`, `problem`, `price`, `gst_percent`, `gst_amount`, `total_amount`, `payment_type`, `status`, `created_date_time`) VALUES
(1, 1, 'kunal', '', '', 'indore', '', '', '', 'aid5679943-v4-1200px-Write-a-Prescription-Step-15.jpg', 'hbfhvhu', '600', '18%', '108', '708', 0, 4, '2020-01-20 15:22:49'),
(2, 2, 'divya', '', '', 'indore', '', '', '', '0.66971400 157416751668469159_111456566859511_4983000930602450944_n.jpg', 'jdj', '255', '', '', '', 0, 2, '2020-01-25 15:12:35');

-- --------------------------------------------------------

--
-- Table structure for table `radiology_test`
--

CREATE TABLE `radiology_test` (
  `id` int(11) NOT NULL,
  `test_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `radiology_test`
--

INSERT INTO `radiology_test` (`id`, `test_name`, `description`, `created_date_time`) VALUES
(2, 'ABC', 'csdvdsc ', '2020-02-04 11:42:56'),
(4, 'mnd csd', 'jdcjls', '2019-12-28 10:03:03'),
(5, 'FFF', 'nscbjhs', '2019-12-28 13:22:11'),
(7, 'SSSS', 'sdfghjkfghbjhg gfchv', '2020-01-07 12:59:01');

-- --------------------------------------------------------

--
-- Table structure for table `radiology_test_price`
--

CREATE TABLE `radiology_test_price` (
  `id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `lab_id` int(11) NOT NULL,
  `price` varchar(200) NOT NULL,
  `offer_price` varchar(200) NOT NULL,
  `off` varchar(200) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `radiology_test_price`
--

INSERT INTO `radiology_test_price` (`id`, `test_id`, `lab_id`, `price`, `offer_price`, `off`, `created_date_time`) VALUES
(1, 1, 1, '', '', '', '2019-12-28 09:59:09'),
(3, 3, 10, '25', '21', '2', '2019-12-28 10:00:33'),
(4, 4, 10, '251', '2221', '2', '2019-12-28 10:03:03'),
(5, 4, 1, '24', '65', '5', '2019-12-28 10:03:03'),
(6, 5, 1, '200', '160', '20', '2019-12-28 13:22:11'),
(8, 7, 10, '900', '850', '5', '2020-01-07 12:59:01'),
(9, 7, 1, '800', '750', '5', '2020-01-07 12:59:01'),
(10, 8, 1, '', '', '', '2020-01-08 13:44:17'),
(11, 2, 1, '900', '800', '11', '2020-02-04 11:42:47'),
(12, 1, 1, '800', '700', '13', '2020-02-04 11:59:26');

-- --------------------------------------------------------

--
-- Table structure for table `upload_radiology_report`
--

CREATE TABLE `upload_radiology_report` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `upload_report` varchar(255) NOT NULL,
  `download_report` varchar(255) NOT NULL,
  `upload_invoice` varchar(255) NOT NULL,
  `download_invoice` varchar(255) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `upload_radiology_report`
--

INSERT INTO `upload_radiology_report` (`id`, `order_id`, `upload_report`, `download_report`, `upload_invoice`, `download_invoice`, `created_date_time`) VALUES
(5, 0, '', '', '', '', '2020-01-03 16:12:36'),
(6, 0, '0.50817600 15781240371320 (5).pdf', '', '', '', '2020-01-04 13:17:17');

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--

CREATE TABLE `user_master` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `date_of_birth` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `address` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `pincode` varchar(100) NOT NULL,
  `refer_code` varchar(100) NOT NULL,
  `referer_code` varchar(100) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_master`
--

INSERT INTO `user_master` (`id`, `name`, `date_of_birth`, `email`, `mobile_no`, `address`, `city`, `state`, `pincode`, `refer_code`, `referer_code`, `created_date_time`) VALUES
(1, 'Anchal Gupta', '2020-01-06', 'bhxd v', '31654654', 'huvhgvjhvj', 'indore', 'Madhya Pradesh', '452001', 'gfghf32', 'hygcg512', '2020-01-06 00:00:00'),
(2, 'kajal', '2020-01-06', 'jhcdgs', '4645454', 'mdnkjd', 'indore', 'Madhya Pradesh', '214565', 'scnsc ', 'djcbidgi', '2020-01-06 00:00:00'),
(3, 'Rakesh', '2020-01-06', 'rakesh@gmail.com', '8319926540', 'geeta bhawan', 'indore', 'mp', '452001', 'QP0678', 'YMBD45', '2020-01-12 21:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `user_notification_list`
--

CREATE TABLE `user_notification_list` (
  `id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_notification_list`
--

INSERT INTO `user_notification_list` (`id`, `notification_id`, `user_id`, `created_date_time`) VALUES
(1, 1, 1, '2020-01-07 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_otp`
--

CREATE TABLE `user_otp` (
  `id` int(11) NOT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `otp` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_otp`
--

INSERT INTO `user_otp` (`id`, `mobile_no`, `otp`, `status`, `created_date_time`) VALUES
(1, '8319926540', '411654', 0, '2020-01-12 21:08:05'),
(2, '8223016122', '581294', 0, '2020-01-12 21:09:40'),
(3, NULL, '711671', 0, '2020-01-12 21:09:20'),
(4, NULL, '225166', 0, '2020-01-28 13:25:01'),
(5, '9893695111', '593491', 0, '2020-01-28 13:31:42');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `real_password` varchar(50) NOT NULL,
  `role` varchar(100) NOT NULL,
  `created_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `name`, `email`, `mobile_no`, `address`, `username`, `password`, `real_password`, `role`, `created_date_time`) VALUES
(1, 'Anchal Gupta ', 'ganchal22@gmail.com', '8223016122', 'indore', 'anchalgupta123', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'Dietician', '2019-12-24 12:04:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner_master`
--
ALTER TABLE `banner_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dietician_order_master`
--
ALTER TABLE `dietician_order_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `executive_health_package`
--
ALTER TABLE `executive_health_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `executive_health_package_price`
--
ALTER TABLE `executive_health_package_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generate_invoice_no`
--
ALTER TABLE `generate_invoice_no`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `health_package`
--
ALTER TABLE `health_package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `health_package_price`
--
ALTER TABLE `health_package_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_message_master`
--
ALTER TABLE `notification_message_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_onscreen_user`
--
ALTER TABLE `notification_onscreen_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_user`
--
ALTER TABLE `notification_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nursing_order`
--
ALTER TABLE `nursing_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nursing_requirement`
--
ALTER TABLE `nursing_requirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pathology_labs`
--
ALTER TABLE `pathology_labs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pathology_medicine_data`
--
ALTER TABLE `pathology_medicine_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pathology_order`
--
ALTER TABLE `pathology_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pathology_prescription`
--
ALTER TABLE `pathology_prescription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pathology_test`
--
ALTER TABLE `pathology_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pathology_test_price`
--
ALTER TABLE `pathology_test_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pharmacy`
--
ALTER TABLE `pharmacy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pharmacy_medicine_data`
--
ALTER TABLE `pharmacy_medicine_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `physiotherapy_labs`
--
ALTER TABLE `physiotherapy_labs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `physiotherapy_medicine_data`
--
ALTER TABLE `physiotherapy_medicine_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `physiotherapy_order`
--
ALTER TABLE `physiotherapy_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `physiotherapy_prescription`
--
ALTER TABLE `physiotherapy_prescription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `physiotherapy_test`
--
ALTER TABLE `physiotherapy_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `physiotherapy_test_price`
--
ALTER TABLE `physiotherapy_test_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radiology_labs`
--
ALTER TABLE `radiology_labs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radiology_medicine_data`
--
ALTER TABLE `radiology_medicine_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radiology_order`
--
ALTER TABLE `radiology_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radiology_prescription`
--
ALTER TABLE `radiology_prescription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radiology_test`
--
ALTER TABLE `radiology_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `radiology_test_price`
--
ALTER TABLE `radiology_test_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `upload_radiology_report`
--
ALTER TABLE `upload_radiology_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_master`
--
ALTER TABLE `user_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_notification_list`
--
ALTER TABLE `user_notification_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_otp`
--
ALTER TABLE `user_otp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner_master`
--
ALTER TABLE `banner_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `dietician_order_master`
--
ALTER TABLE `dietician_order_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `executive_health_package`
--
ALTER TABLE `executive_health_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `executive_health_package_price`
--
ALTER TABLE `executive_health_package_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `health_package`
--
ALTER TABLE `health_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `health_package_price`
--
ALTER TABLE `health_package_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `notification_message_master`
--
ALTER TABLE `notification_message_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `notification_onscreen_user`
--
ALTER TABLE `notification_onscreen_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `notification_user`
--
ALTER TABLE `notification_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `nursing_order`
--
ALTER TABLE `nursing_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nursing_requirement`
--
ALTER TABLE `nursing_requirement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pathology_labs`
--
ALTER TABLE `pathology_labs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pathology_medicine_data`
--
ALTER TABLE `pathology_medicine_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pathology_order`
--
ALTER TABLE `pathology_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pathology_prescription`
--
ALTER TABLE `pathology_prescription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pathology_test`
--
ALTER TABLE `pathology_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `pathology_test_price`
--
ALTER TABLE `pathology_test_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `pharmacy`
--
ALTER TABLE `pharmacy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pharmacy_medicine_data`
--
ALTER TABLE `pharmacy_medicine_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `physiotherapy_labs`
--
ALTER TABLE `physiotherapy_labs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `physiotherapy_medicine_data`
--
ALTER TABLE `physiotherapy_medicine_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `physiotherapy_order`
--
ALTER TABLE `physiotherapy_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `physiotherapy_prescription`
--
ALTER TABLE `physiotherapy_prescription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `physiotherapy_test`
--
ALTER TABLE `physiotherapy_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `physiotherapy_test_price`
--
ALTER TABLE `physiotherapy_test_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `radiology_labs`
--
ALTER TABLE `radiology_labs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `radiology_medicine_data`
--
ALTER TABLE `radiology_medicine_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `radiology_order`
--
ALTER TABLE `radiology_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `radiology_prescription`
--
ALTER TABLE `radiology_prescription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `radiology_test`
--
ALTER TABLE `radiology_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `radiology_test_price`
--
ALTER TABLE `radiology_test_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `upload_radiology_report`
--
ALTER TABLE `upload_radiology_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_master`
--
ALTER TABLE `user_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_notification_list`
--
ALTER TABLE `user_notification_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_otp`
--
ALTER TABLE `user_otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
